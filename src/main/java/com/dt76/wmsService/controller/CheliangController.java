package com.dt76.wmsService.controller;


import com.dt76.wmsService.pojo.Cheliang;
import com.dt76.wmsService.service.CheliangService;
import com.dt76.wmsService.utils.Page;
import com.dt76.wmsService.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/che")
public class CheliangController {
    @Autowired
    private CheliangService cheliangService;

    @RequestMapping("/qryAll")
    public Page qryChePageAll(@RequestParam("pageIndex") int pageIndex,
                              @RequestParam(value = "cheNo", required = false) String cheNo,
                              @RequestParam(value = "sj", required = false) String sj,
                              @RequestParam(value = "stu", required = false) String stu){
        System.out.println("成功调用服务");
        return cheliangService.getAll(pageIndex,cheNo,sj,stu);
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Cheliang cheliang){
        System.out.println("成功调用服务");
        try {
            if(cheliang.getcId()==0){
                cheliangService.addChe(cheliang);
                return  new Result(true,"新增成功");
            }else {
                cheliangService.updChe(cheliang);
                return  new Result(true,"修改成功");
            }
        } catch (Exception e) {
           return new Result(false,"新增和修改异常");
        }
    }

    @RequestMapping("/delete")
    public Result delete(@RequestParam("id") int id){
        System.out.println("成功调用服务");
        try {
            cheliangService.delChe(id);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            return new Result(false,"删除异常");
        }
    }
}
