package com.dt76.wmsService.controller;

/*
 *Created by 王超 on 2019/5/10
 */

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.service.ChukuTongzhiService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController//接口统一用json返回
@RequestMapping("/chuku")
public class ChukuTongzhiController {
    @Autowired
    private ChukuTongzhiService chukuTongzhiService;

    @RequestMapping("/get1")

    public Page getAll(@RequestParam("pageIndex") int pageIndex,
                       @RequestParam(value = "sjr",required = false) String sjr,
                  @RequestParam(value = "ddh",required = false) String ddh){
        System.out.println("成功调用远程服务>>>>>chukuTongzhiService>>getAll");
        Page page = chukuTongzhiService.getAllChuku(pageIndex,sjr,ddh);
        return page;
    }

    @RequestMapping("/zhunbei")
    public void chuku(@RequestParam("spId") int id){
        chukuTongzhiService.xiuAdd(id);
    }


    @RequestMapping("/findOne")

    public ChukuTongzhi findOne(@RequestParam("spId") int spId){
         ChukuTongzhi chukuTongzhi =  chukuTongzhiService.get(spId);
         return chukuTongzhi;
    }
}
