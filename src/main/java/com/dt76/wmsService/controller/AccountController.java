package com.dt76.wmsService.controller;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 11:14
 */

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.SysEmployee;
import com.dt76.wmsService.service.AccountService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 提供后台用户增加，登录等方法
 */

@RestController
@RequestMapping("/account2")
public class AccountController {


    @Autowired
    private AccountService accountService;

    /**
     * 首次进入时 空跳转 进入登录
     */
    @RequestMapping("/login")
    public SysEmployee login(@RequestParam("username") String username,
                             @RequestParam("password")String password){
        System.out.println("进入消费端调用");
        SysEmployee loginUser = accountService.login(username, password);
        return loginUser;
    }
    @RequestMapping("/loginShiro")
    public CommonReturnType loginShiro(@RequestParam("username") String username,
                                       @RequestParam("password")String password) {
        // 判断用户名密码是否为空
        if (username != null && !"".equals(username) && password != null && !"".equals(password)) {
            // 进行登录验证
            Subject subject = SecurityUtils.getSubject();
            // 创建验证用的令牌对象
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            try {
                subject.login(token);
                boolean flag = subject.isAuthenticated();
                if(flag){
                    System.out.println("登录成功！");
                    //获取当前用对象，放入到session中
                    SysEmployee user = (SysEmployee)subject.getPrincipal();

                    return CommonReturnType.create(user);
                }else{
                    return CommonReturnType.create("登录认证失败","fail");
                }

            } catch (Exception e) {
                return CommonReturnType.create("登录认证失败","fail");
            }
        }
        return CommonReturnType.create("账户或者密码不正确","fail");
    }
}
