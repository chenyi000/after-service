package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.service.IsShouService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/isShou")
public class IsShouController {

    @Autowired
    private IsShouService isShouService;


    @RequestMapping("/updataCost")
    public int udpDayAll(@RequestBody WmsDayCost wmsDayCost) {
        return isShouService.updataCost(wmsDayCost);
    }

    @RequestMapping("/findIsShou")
    public List<DayCostPageUtil> getPageAndCondition(@RequestBody DayCostSearchUtil dayCostSearchUtil) {
        System.out.println("进入后台调用");
        //测试用
        List<DayCostPageUtil> dayCostPageUtils = isShouService.findDayCost(dayCostSearchUtil);
        return dayCostPageUtils;
    }

    @RequestMapping("/addDayCost")
    public CommonReturnType addDayCost(@RequestBody WmsDayCost wmsDayCost) {
        Integer uId = wmsDayCost.getId();
        int aLong = 0;
        if (uId == null || uId == 0) {
            //增加
            aLong = isShouService.addDayCost(wmsDayCost);
            return CommonReturnType.create("录入成功", "success");
        } else {
            //修改
            aLong = isShouService.updataCost(wmsDayCost);
        }
        if (aLong > 0) {
            return CommonReturnType.create("编辑员工成功", "success");
        }
        return CommonReturnType.create("编辑员工失败", "success");

    }

    @RequestMapping("/delDayCost")
    public CommonReturnType delDayCost(@RequestParam("id") int id) {

        if (isShouService.delDayCost(id) > 0) {
            return CommonReturnType.create("删除成功");
        }
        return CommonReturnType.create("删除失败", "fial");
    }

    @RequestMapping("/findName")
    public List<SysCustomer> findName(@RequestParam("name") String name) {
        return isShouService.findName(name);
    }

    @RequestMapping("/findById")
    public DayCostPageUtil findById(@RequestParam("id") int id) {
        return isShouService.findById(id);
    }
}

