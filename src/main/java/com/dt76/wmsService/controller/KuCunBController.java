package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.KuCunB;
import com.dt76.wmsService.service.KuCunBService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController //接口统一用json返回
@RequestMapping("/kucunb")
public class KuCunBController {
    @Autowired
    private KuCunBService kuCunBService;
    @RequestMapping("/findPageConditionAll")
    public Page findPageConditionAll(@RequestParam("pageIndex") int pageIndex,

                                     @RequestParam(value="goodsName",required = false)String goodsName,
                                     @RequestParam(value="chuweiName",required = false)String chuweiName
    ){
        System.out.println("成功调用远程服务:kuCunAService>findPageConditionAll>>>");
        return kuCunBService.findAllKuCunB(pageIndex,goodsName,chuweiName);
    }
    @RequestMapping("/findAll")
    @ResponseBody
    public List<KuCunB> findAll(){
        System.out.println("成功调用远程服务:kuCunAService>findAll>>>");
        return kuCunBService.findALlKuCunB()


                 ;
    }

}
