package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.Bin;
import com.dt76.wmsService.service.BinService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController //接口统一用json返回
@RequestMapping("/bin")
public class BinController {
    @Autowired
    private BinService binService;


  /*  @RequestMapping("/findPageConditionAll")
    public Page findPageConditionAll( Integer pageIndex,
                                   String storeCode,
                                  Integer storeId,
                                   Integer pId){
        System.out.println("成功调用远程服务:binService>findPageConditionAll>>>");

       return binService.getAllBin(pageIndex,storeCode,storeId,pId);
    }*/
    @RequestMapping("/findAll")
    public List<Bin> findAll(){
        System.out.println("成功调用远程服务:binService>findAll>>>");
        return binService.findAllBin();
    }
    @RequestMapping("/findPageAll")
    public Page findPageAll(@RequestParam("pageIndex") int pageIndex,

                                     @RequestParam(value="storeName",required = false)String storeName,
                                     @RequestParam(value="chuweiName",required = false)String chuweiName,
                                     @RequestParam(value="tuopanName",required = false)String tuopanName
    ){
        System.out.println("成功调用远程服务:lihuoService>findPageConditionAll>>>");
        return binService.getAllBin2(pageIndex,storeName,chuweiName,tuopanName);
    }
}
