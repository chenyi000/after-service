package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.IntegratedInventory;
import com.dt76.wmsService.service.IntegratedInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/IIMapper")
public class IntegratedInventoryController {

    @Autowired
    private IntegratedInventoryService integratedInventoryService;

    @RequestMapping("/findAll3")
    @ResponseBody
    public List<IntegratedInventory> qryAll(){
        System.out.println("成功调用远程服务");

        return integratedInventoryService.qryAll();
    }


}
