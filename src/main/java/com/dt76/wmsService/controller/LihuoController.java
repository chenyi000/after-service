package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.KuCunA;
import com.dt76.wmsService.pojo.Lihuo;
import com.dt76.wmsService.service.LihuoService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController //接口统一用json返回
@RequestMapping("/lihuo")
public class LihuoController {
    @Autowired
    private LihuoService lihuoService;
    @RequestMapping("/findPageConditionAll")
    public Page findPageConditionAll(@RequestParam("pageIndex") int pageIndex,

                                     @RequestParam(value="goodsName",required = false)String goodsName,
                                     @RequestParam(value="chuweiName",required = false)String chuweiName,
                                     @RequestParam(value="tuopanName",required = false)String tuopanName
    ){
        System.out.println("成功调用远程服务:lihuoService>findPageConditionAll>>>");
        return lihuoService.findAllLihuo(pageIndex,goodsName,chuweiName,tuopanName);
    }
    @RequestMapping("/findAll")
    @ResponseBody
    public List<Lihuo> findAll(){
        System.out.println("成功调用远程服务:lihuoService>findAll>>>");
        return lihuoService.findALlLihuo();

    }
}
