package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.Inventory;
import com.dt76.wmsService.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.List;

@Controller
@RequestMapping("/InventoryMapper")
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @RequestMapping("/findAll1")
    @ResponseBody
    public List<Inventory> qryAll(){
        System.out.println("成功调用远程服务");
        List<Inventory> list = inventoryService.qryAll();
        System.out.println(list);
        return inventoryService.qryAll();
    }

}
