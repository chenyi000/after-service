package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.service.DayCostService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dayCost")
public class DayCostController {

    @Autowired
    private DayCostService dayCostService;

    /*@RequestMapping("/getAll")
    public List<WmsDayCost> getAllDayCost() {
        return dayCostService.getAllDayCost();
    }*/

    @RequestMapping("/updataCost")
    public int udpDayAll(@RequestBody WmsDayCost wmsDayCost) {
        return dayCostService.updataCost(wmsDayCost);
    }

    @RequestMapping("/findDayCost")
    public List<DayCostPageUtil> getPageAndCondition(@RequestBody DayCostSearchUtil dayCostSearchUtil) {
        System.out.println("进入后台调用");
        //测试用
        List<DayCostPageUtil> dayCostPageUtils = dayCostService.findDayCost(dayCostSearchUtil);
        return dayCostPageUtils;
    }

    @RequestMapping("/addDayCost")
    public CommonReturnType addDayCost(@RequestBody WmsDayCost wmsDayCost) {
        Integer uId = wmsDayCost.getId();
        int aLong = 0;
        if (uId == null || uId == 0) {
            //增加
            aLong = dayCostService.addDayCost(wmsDayCost);
            return CommonReturnType.create("录入成功", "success");
        } else {
            //修改
            aLong = dayCostService.updataCost(wmsDayCost);
        }
        if (aLong > 0) {
            return CommonReturnType.create("编辑成功", "success");
        }
        return CommonReturnType.create("编辑失败", "success");

    }

    @RequestMapping("/delDayCost")
    public CommonReturnType delDayCost(@RequestParam("id") int id) {

        if (dayCostService.delDayCost(id) > 0) {
            return CommonReturnType.create("删除成功");
        }
        return CommonReturnType.create("删除失败", "fial");
    }

    @RequestMapping("/findName")
    public List<SysCustomer> findName(@RequestParam("name") String name) {
        return dayCostService.findName(name);
    }

    @RequestMapping("/findById")
    public DayCostPageUtil findById(@RequestParam("id") int id) {
        return dayCostService.findById(id);
    }

    @RequestMapping("/findShop")
    public DayCostPageUtil findShop(String name) {
        return dayCostService.findShop(name);
    }
}

