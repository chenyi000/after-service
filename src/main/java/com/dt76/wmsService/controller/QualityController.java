package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.Quality;
import com.dt76.wmsService.service.QualityService;
import com.dt76.wmsService.utils.Result;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
    @RequestMapping("/QualityMapper")
    public class QualityController {
    //注入QualityStatus服务层接口
    @Autowired
    private QualityService qualityStatusService;

    /* 不带分页*/
    @RequestMapping("/qryAll")
    public List<Quality> qryQuality() {
        return qualityStatusService.getAllQuality();
    }

    /*带条件查询*/
    @RequestMapping("/toqryAll")
    public Quality toqryQuality(@RequestParam("ID") String ID) {
        return qualityStatusService.toqryQuality(Integer.parseInt(ID));
    }

    @RequestMapping("/add")
    public Result addQuality(@RequestParam("quality") String quality) {
        System.out.println("成功调用远程服务:orderService>add>>>");

      Gson gson =new Gson();
      Quality quality2 =gson.fromJson(quality,Quality.class);

        qualityStatusService.addQuality(quality2);

        return new Result(true, "新增成功");  //只需要知道返回结果成功没有
    }

    @RequestMapping("/update")
    public Result updateQuality(@RequestParam("quality") String quality) {


        Gson gson=new Gson();
        Quality quality1 = gson.fromJson(quality, Quality.class);


        System.out.println("成功调用远程服务：qualityService");

        qualityStatusService.updateQuality(quality1);

        return new Result(true, "修改成功");
    }

    @RequestMapping("/delQuality")
    public Result delQuality(@RequestParam("ID") String ID) {
        System.out.println("成功调用远程服务：qualityService");
        Integer result = qualityStatusService.delQuality(Integer.parseInt(ID));
       return new Result(true, "修改成功");

    }
}
