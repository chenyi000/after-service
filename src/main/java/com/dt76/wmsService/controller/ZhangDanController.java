package com.dt76.wmsService.controller;

import com.dt76.wmsService.service.ZhangDaoService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.ZhangDanPageUtil;
import com.dt76.wmsService.utils.ZhangDanSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("zDan")
public class ZhangDanController {

    @Autowired
    private ZhangDaoService zhangDaoService;

    @RequestMapping("findZdan")
    public List<ZhangDanPageUtil> findzDan(@RequestBody ZhangDanSearchUtil zhangDanSearchUtil) {
        System.out.println("进入后台调用");
        //测试用
        List<ZhangDanPageUtil> zhangDanPageUtils = zhangDaoService.findZd(zhangDanSearchUtil);
        return zhangDanPageUtils;
    }

    @RequestMapping("/exportBill")
    public List<DayCostPageUtil> exportBill(@RequestBody ZhangDanSearchUtil zhangDanSearchUtil) {
        return zhangDaoService.exportBill(zhangDanSearchUtil);
    }

}
