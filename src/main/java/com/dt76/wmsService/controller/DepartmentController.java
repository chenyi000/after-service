package com.dt76.wmsService.controller;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 14:06
 */

import com.dt76.wmsService.service.DepartmentService;
import com.dt76.wmsService.utils.DepartmentPageUtil;
import com.dt76.wmsService.utils.DepartmentSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 部门信息的controller
 */

@Controller
@Repository("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    /**
     * 按条件查询
     * @param departmentSearchUtil ：查询条件
     * @return
     */
    @RequestMapping("/getDepartment")
    public List<DepartmentPageUtil> getDepartment(DepartmentSearchUtil departmentSearchUtil){
        return departmentService.finDepartment(departmentSearchUtil);
    }

}
