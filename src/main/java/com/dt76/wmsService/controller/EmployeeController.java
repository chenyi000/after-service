package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.SysEmployee;
import com.dt76.wmsService.service.EmployeeService;
import com.dt76.wmsService.utils.EmployeePageUtil;
import com.dt76.wmsService.utils.EmployeeSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 14:59
 */
@RestController
@RequestMapping("/employee2")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 动态分页，+ 查询
     * @param employeeSearchUtil
     * @return
     */
    @RequestMapping(value = "/finEmployee")
    public List<EmployeePageUtil> finEmployee(@RequestBody EmployeeSearchUtil employeeSearchUtil){
        System.out.println("进入后台调用");
        List<EmployeePageUtil> employeePageUtils = employeeService.finEmployee(employeeSearchUtil);
        return employeePageUtils;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("delEmployese")
    public CommonReturnType delEmployee(@RequestParam("id") Long id){
        System.out.println("进入后台：employee2 . delEmployese ");
        Integer result = employeeService.delEmployee(id);
        if(result > 0){
            return CommonReturnType.create("删除成功");
        }
        return CommonReturnType.create("删除失败","fial");
    }

    /**
     * 增加或者修改
     * @param sysEmployee
     * @return
     */
    @RequestMapping("editEmployee")
    public CommonReturnType editEmployee(@RequestBody SysEmployee sysEmployee){
        Long uId = sysEmployee.getEId();
        Integer aLong =null;
        if(uId == null || uId == 0L){
            //增加
            aLong=employeeService.addEmployee(sysEmployee);
        }else{
            //修改
            aLong = employeeService.updEmployee(sysEmployee);
        }
        if(aLong > 0){
            return CommonReturnType.create("编辑员工成功","success");
        }
        return CommonReturnType.create("编辑员工失败","success");

    }


    @RequestMapping("getEmployeeById")
    public SysEmployee getEmployeeById(@RequestParam("id") Long id){
         return employeeService.getEmployeeById(id);
    }

}
