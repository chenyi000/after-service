package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.SendOrderUtil;
import com.dt76.wmsService.commen.Result;
import com.dt76.wmsService.pojo.WmsOrder;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.OrderService;
import com.dt76.wmsService.service.ShopService;
import com.dt76.wmsService.utils.OrderPageUtil;
import com.dt76.wmsService.utils.OrderSearchUtil;
import com.dt76.wmsService.utils.OrderSh;
import com.dt76.wmsService.utils.WmsUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-10 09:46
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;



    /*@RequestMapping("/order/add")
    public Result addOrder(@RequestBody SendOrderUtil orderShop){
        System.out.println("成功调用远程服务:orderService>add>>>");
        //1生成订单号
        String orderId = "DT"+ WmsUUID.getUUID();
        String shopId = "SH"+ WmsUUID.getUUID();
        //2插入商品表
        WmsShop wmsShop = new WmsShop();
        wmsShop.setCreateName("wx");
        wmsShop.setShpName(orderShop.getShpName());
        wmsShop.setShpType(orderShop.getShpType());
        wmsShop.setShpCode(shopId);
        wmsShop.setShpNum(orderShop.getShpNum());
        wmsShop.setShpWeight(orderShop.getShpWeight());
        wmsShop.setShpTiji(orderShop.getShpTiji());
        wmsShop.setInType(orderShop.getInType());
        wmsShop.setProDate(orderShop.getProDate());
        wmsShop.setBaozhiqi(orderShop.getBaozhiqi());
        wmsShop.setUnit(orderShop.getUnit());
        wmsShop.setInDay(orderShop.getInDay());
        wmsShop.setOrderCode(orderId);
        wmsShop.setStatusId(1);

        shopService.addShop(wmsShop);
        System.out.println("增加成功");


        //3插入订单表
        //获取商品主键
        int reciveId = wmsShop.getId();
        //插入订单表
        WmsOrder wmsOrder = new WmsOrder();
        wmsOrder.setCreateName("Wx");
        wmsOrder.setCusCode(orderId);
        wmsOrder.setCusName(orderShop.getCusName());
        wmsOrder.setMasName(orderShop.getMasName());
        wmsOrder.setCusAddress(orderShop.getCusAddress());
        wmsOrder.setMasAddress(orderShop.getMasAddress());
        wmsOrder.setCusPhone(orderShop.getCusPhone());
        wmsOrder.setMasPhone(orderShop.getMasPhone());
        wmsOrder.setShopId(reciveId);
        wmsOrder.setTypeId(orderShop.getTypeId());

             orderService.addOrder(wmsOrder);
            System.out.println("增加成功2");
        return new Result(true,"新增成功");
    }*/

    @RequestMapping("/order/qryAll1")
    public List<WmsOrder> qryOrder(){
        System.out.println("成功调用远程服务:orderService>qryAll>>>");
        return orderService.getAll();
    }

    //查询全部订单加商品
    @RequestMapping("/order/qryAll2")
    public List<OrderPageUtil> qryOrder2(@RequestBody OrderSearchUtil orderSearchUtil){
        System.out.println("成功调用远程服务:orderService>qryAll2>>>");
        List<OrderPageUtil> employeePageUtils = orderService.getAll2(orderSearchUtil);
        for (OrderPageUtil orderPageUtil:employeePageUtils) {
            if (orderPageUtil.getStatusId()==1){
                orderPageUtil.setStatus("待接单");
            }else {
                orderPageUtil.setStatus("已接单");
            }
        }
        System.out.println(employeePageUtils);
        return employeePageUtils;
    }
    /*@RequestMapping("/qryAll2")
    public PageInfo qryOrder2(@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);
        System.out.println("成功调用远程服务:orderService>qryAll2>>>");
        List<WmsOrder> list = orderService.getAll2();
        PageInfo pageInfo = new PageInfo(list,5);
        return pageInfo;
    }*/

    //查询全单条订单
    @RequestMapping("/order/qryById")
    public OrderPageUtil qryOrder3(@RequestParam("id") int id){
        System.out.println("成功调用远程服务:orderService>qryById>>>");
        return orderService.getById(id);
    }

    /*//修改订单
    @RequestMapping("/order/upd")
    public Result updOrder1(@RequestBody OrderSh orderSh){
        System.out.println("成功调用远程服务:orderService>updOrder>>>");
        WmsOrder wmsOrder = new WmsOrder();
        wmsOrder.setId(orderSh.getOId());
        wmsOrder.setCusName(orderSh.getCusName());
        orderService.updOrder(wmsOrder);
        return new Result(true,"修改成功");
    }*/
    @RequestMapping("/order/change")
    public void change(@RequestBody OrderSh orderSh){
        System.out.println("成功调用远程服务:orderService>updOrder>>>");
        System.out.println(orderSh);
        WmsOrder wmsOrder = new WmsOrder();
        wmsOrder.setId(Integer.parseInt(orderSh.getCus()));
        wmsOrder.setCusName(orderSh.getCusName());
        wmsOrder.setCusPhone(orderSh.getCusPhone());
        wmsOrder.setCusAddress(orderSh.getCusAdd());
        wmsOrder.setMasName(orderSh.getMas());
        wmsOrder.setMasPhone(orderSh.getMasPhone());
        wmsOrder.setMasAddress(orderSh.getMasAdd());
        orderService.updOrder(wmsOrder);
    }

}
