package com.dt76.wmsService.controller;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 8:16
 */

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.service.CustomerService;
import com.dt76.wmsService.utils.CustomerInfoUtil;
import com.dt76.wmsService.utils.CustomerPageUtil;
import com.dt76.wmsService.utils.CustomerSearcherUtil;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户的controller 层
 */

@RestController
@RequestMapping("/sysCustomer2")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 动态查询分页
     *
     * @param searcherUtil
     * @return
     */
    @RequestMapping("/findCustomer")
    public List<CustomerPageUtil> findCustomer(@RequestParam("searcherUtil") String searcherUtil) {
        Gson gson=new Gson();
        CustomerSearcherUtil customerSearcherUtil = gson.fromJson(searcherUtil, CustomerSearcherUtil.class);
        List<CustomerPageUtil> customers = customerService.findCustomer(customerSearcherUtil);
        return customers;
    }

    /**
     * 增加用户
     *
     * @param sysCustomer
     * @return
     */
    @RequestMapping("/addCustomer")
    public CommonReturnType addCustomer(@RequestBody SysCustomer sysCustomer) {
        Integer result = customerService.addCustomer(sysCustomer);
        if(result > 0){
            return CommonReturnType.create("增加成功");
        }
        return CommonReturnType.create("删除失败","fial");
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delCustomer")
    public CommonReturnType delCustomer(@RequestParam("id") Long id) {
        Integer result = customerService.delCustomerById(id);
        if (result > 0) {
            return CommonReturnType.create("删除成功");
        }
        return CommonReturnType.create("删除失败", "fial");
    }


    /**
     * 修改
     *
     * @param sysCustomer
     * @return
     */
    @RequestMapping("/updCustomer")
    public CommonReturnType updCustomer(@RequestBody SysCustomer sysCustomer) {
        Integer result = customerService.updCustomer(sysCustomer);
        if (result > 0) {
            return CommonReturnType.create("增加成功");
        }
        return CommonReturnType.create("删除失败", "fial");
    }

    /**
     * 单个查找
     *
     * @param id
     * @return
     */
    @RequestMapping("/findCustomerById")
    public CustomerInfoUtil findCustomerById(@RequestParam("id") Long id) {
        return customerService.findCustomerById(id);
    }

    /**
     * 修改合作状态
     *
     * @param id
     * @return
     */
    @RequestMapping("/updCustomerStatusById")
    public CommonReturnType updCustomerStatusById(@RequestParam("status") Integer status,
                                                  @RequestParam("id") Long id) {
        Integer result = customerService.updCustomerStatusById(status, id);
        if (result > 0) {
            return CommonReturnType.create("增加成功");
        }
        return CommonReturnType.create("删除失败", "fial");
    }


}
