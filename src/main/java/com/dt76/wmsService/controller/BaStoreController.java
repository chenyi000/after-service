package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.BaStore;
import com.dt76.wmsService.service.BaStoreService;
import com.dt76.wmsService.utils.Page;
import com.dt76.wmsService.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController //接口统一用json返回
@RequestMapping("/bastore")
public class BaStoreController {
    @Autowired
    private BaStoreService baStoreService;

    @RequestMapping("/findPageConditionAll")
    public Page findPageConditionAll(@RequestParam("pageIndex") int pageIndex,
                                    @RequestParam(value = "storeCode",required = false)String storeCode,
                                    @RequestParam(value = "storeName",required = false)String  storeName){
        System.out.println("成功调用远程服务:BaStoreService>findPageConditionAll>>>");
        return baStoreService.findAllbaStore(pageIndex,storeCode,storeName);
    }
    @RequestMapping("/findAll")
    public List<BaStore> findAll(){
        System.out.println("成功调用远程服务:BaStoreService>findAll>>>");
        return baStoreService.findALlbaStore();
    }
    @RequestMapping("/findOne")
    public BaStore findOne(@RequestParam("storeId") int storeId){
        System.out.println("成功调用远程服务:userservice>qryAll>>>");
        return baStoreService.findbaStoreById(storeId);
    }
    @RequestMapping("/add")
    public Result add(@RequestBody BaStore baStore){
        System.out.println("成功调用远程服务:BaStoreService>add>>>");
        //调用新增接口
        try {
            if(baStore.getStoreId()==0){
                baStoreService.addbaStore(baStore);
                return new Result(true,"新增成功>>>");
            }else{//走修改接口
                baStoreService.udpbaStore(baStore);
                return new Result(true,"修改成功>>>");
            }
        } catch (Exception e) {
            return new Result(false,"修改或新增异常");
        }
    }
    @RequestMapping("/delete")
    public Result delete(@RequestParam("storeId") int storeId){
        System.out.println("成功调用远程服务:BaStoreService>delete>>>");
        try {
            baStoreService.delbaStore(storeId);
            return new Result(true,"删除成功>>>");
        } catch (Exception e) {
            return new Result(false,"删除失败>>>");
        }

    }

}
