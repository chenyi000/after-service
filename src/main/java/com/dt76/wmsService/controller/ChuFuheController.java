package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.service.ChuFuheService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
 *Created by 王超 on 2019/5/17
 */
@RestController//接口统一用json返回
@RequestMapping("/fuhe")
public class ChuFuheController {
    @Autowired
    private ChuFuheService chuFuheService;

    @RequestMapping("/get")
    @ResponseBody
    public Page getAll(@RequestParam("pageIndex") int pageIndex, @RequestParam(value = "ddh", required = false) String ddh) {
        System.out.println("成功调用远程服务》》》》》》》》》chuFuheService》》》getAll");
        Page page = chuFuheService.getAllFuhe(pageIndex, ddh);
        return page;
    }

    @RequestMapping("/modify")
    public void modify(@RequestParam("spId") int spId) {
        chuFuheService.modify(spId);
    }

}
