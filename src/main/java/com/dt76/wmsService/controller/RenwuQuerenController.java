package com.dt76.wmsService.controller;

import com.dt76.wmsService.service.ReneuQuerenService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 *Created by 王超 on 2019/5/12
 */
@RestController
@RequestMapping("/renwu")
public class RenwuQuerenController {
    @Autowired
    private ReneuQuerenService reneuQuerenService;

    @RequestMapping("/get")
    public Page getAll(@RequestParam("pageIndex") int pageIndex,@RequestParam(value = "sp",required = false) String sp){
        System.out.println("成功调用远程服务》》》》》reneuQuerenService》》》》分页");
        Page page = reneuQuerenService.getAllRenwu(pageIndex,sp);
        return page;
    }

   @RequestMapping("/gai")
    public void modify(@RequestParam("spId") int id){
        reneuQuerenService.modiffy(id);
   }

    @RequestMapping("/insert1")
    public void  tong(@RequestParam("spId") int id){
        reneuQuerenService.GaiInsert(id);
    }
}
