package com.dt76.wmsService.controller;

import com.dt76.wmsService.service.ChuweiService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController //接口统一用json返回
@RequestMapping("/chuwei")
public class ChuweiController {
    @Autowired
    private ChuweiService chuweiService;
    @RequestMapping("/findPageConditionAll")
    public Page findPageConditionAll(@RequestParam("pageIndex") int pageIndex,

                                     @RequestParam(value = "storeName",required = false)String storeName){
        System.out.println("成功调用远程服务:BaStoreService>findPageConditionAll>>>");
        return chuweiService.findAllcuWei(pageIndex,storeName);
    }
}
