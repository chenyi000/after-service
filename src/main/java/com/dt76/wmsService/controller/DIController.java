package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.DifferencesInventory;
import com.dt76.wmsService.service.DIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/DIMapper")
public class DIController {

    @Autowired
    private DIService diService;

    @RequestMapping("/findAll2")
    @ResponseBody
    public List<DifferencesInventory> qryAll(){
        System.out.println("成功调用远程服务");
        return diService.qryAll();
    }
}
