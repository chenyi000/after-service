package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.Wms_his_stock_ku;
import com.dt76.wmsService.service.Wms_his_stock_kuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/wms_his_stock_ku")
public class Wms_his_stock_kuController {

    @Autowired
    private Wms_his_stock_kuService wmsHisStockKuService;

    @RequestMapping("/getAll")
    @ResponseBody
    public List<Wms_his_stock_ku> getAll(){
        System.out.println("成功调用远程服务！");
        return   wmsHisStockKuService.findAllWms_his_stock_ku();

    };
}
