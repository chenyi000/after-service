package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.CommonReturnType;
import com.dt76.wmsService.pojo.BShop;
import com.dt76.wmsService.service.BShopService;
import com.dt76.wmsService.utils.BShopPageUtil;
import com.dt76.wmsService.utils.BShopSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bshop")
public class BShopController {
    @Autowired
    private BShopService bShopService;
    @RequestMapping("/qryAll")
    public List<BShop> qryAll(){
        System.out.println("成功调用远程服务:bShopService>>qryAll");
        System.out.println(bShopService.findALlBshop());
        return bShopService.findALlBshop();
    }
    /**
     * 动态分页，+ 查询
     * @param
     * @return
     */
    @RequestMapping("/finBshop")
    public List<BShopPageUtil> finBshop(@RequestBody BShopSearchUtil bShopSearchUtil){
        System.out.println("进入后台调用");
        //测试用
        List<BShopPageUtil> bshopPageUtils = bShopService.finBshop(bShopSearchUtil);
        return bshopPageUtils;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("delBshop")
    public CommonReturnType delBshop(@RequestParam("storeId") int storeId){
        System.out.println("进入后台：employee2 . delEmployese ");
        Integer result = bShopService.delBshop(storeId);
        if(result > 0){
            return CommonReturnType.create("删除成功");
        }
        return CommonReturnType.create("删除失败","fial");
    }
    /**
     * 增加或者修改
     * @param sysEmployee
     * @return
     */
    @RequestMapping("editBshop")
    public CommonReturnType editBshop(@RequestBody BShop bShop){
        int  uId = bShop.getStoreId();
        Integer aLong =null;
        if( uId == 0){
            //增加
            aLong=bShopService.addBshop(bShop);
        }else{
            //修改
            aLong = bShopService.updBshop(bShop);
        }
        if(aLong > 0){
            return CommonReturnType.create("编辑仓库成功","success");
        }
        return CommonReturnType.create("编辑仓库失败","success");

    }
    @RequestMapping("getBshopById")
    public BShop getBshopById(@RequestParam("storeId") int storeId){
        return bShopService.getBshopById(storeId);
    }

}
