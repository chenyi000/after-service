package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.Result;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.ShopService;
import com.dt76.wmsService.utils.OrderSh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-10 09:45
 */
@RestController
@RequestMapping("/shop")
public class ShopConroller {

    @Autowired
    private ShopService shopService;

    @RequestMapping("/getOne")
    public WmsShop getOne(@RequestParam(value = "id",required = false) int id){
        return shopService.getOne(id);
    }

    @RequestMapping("/change")
    public Result change(@RequestBody WmsShop wmsShop){
        shopService.upd(wmsShop);
        return new Result(true,"修改成功");
    }

    @RequestMapping("/change2")
    public void change2(@RequestBody OrderSh orderSh){
        WmsShop wmsShop = new WmsShop();
        wmsShop.setShpNum(orderSh.getShpNum());
        wmsShop.setShpName(orderSh.getShpName());
        wmsShop.setInDay(Integer.parseInt(orderSh.getInDay()));
        wmsShop.setInType(orderSh.getInType());
    }

    @RequestMapping("/qryCount1")
    public int qryCount1(){
        int count1 = shopService.qryCount1();
        //System.out.println(count1);
        return count1;
    }
    @RequestMapping("/qryCount2")
    public int qryCount2(){
        int count2 = shopService.qryCount2();
        //System.out.println(count2);
        return count2;
    }
    @RequestMapping("/qryCount3")
    public int qryCount3(){
        int count3 = shopService.qryCount3();
        //System.out.println(count3);
        return count3;
    }
    @RequestMapping("/qryCount4")
    public int qryCount4(){
        int count4 = shopService.qryCount4();
        //System.out.println(count4);
        return count4;
    }
}
