package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.Platform;
import com.dt76.wmsService.service.PlatformService;
import com.dt76.wmsService.utils.Page;
import com.dt76.wmsService.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plat")
public class PlatformController {
    //注入服务接口
    @Autowired
    private PlatformService platformService;

    @RequestMapping("/qryAll")
    public List<Platform> qryAll(){
        System.out.println("成功调用远程服务:platformService>>qryAll");
        System.out.println(platformService.getPlatformAll());
        return platformService.getPlatformAll();
    }

    @RequestMapping("/qryPlatPageAll")
    public Page qryPlatPageAll(@RequestParam("pageIndex") int pageIndex,
                               @RequestParam(value = "qryCode", required = false) String qryCode,
                               @RequestParam(value = "qryName", required = false) String qryName){
        System.out.println("成功调用：platformService》》qryPlatPageAll");
        return  platformService.getAll(pageIndex,qryCode,qryName);
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Platform platform){
        System.out.println("成功调用远程服务》》》");
        try {
            if(platform.getPfId()==0){
                platformService.addPlatform(platform);
                return new Result(true,"新增成功");
            }else {
                platformService.updPlatform(platform);
                return new Result(true,"修改成功");
            }
        } catch (Exception e) {
            return new Result(false,"新增或修改异常");
        }
    }

    @RequestMapping("/delete")
    public Result delete(@RequestParam("id") int id){
        System.out.println("成功调用远程服务》》》");
        try {
            platformService.delPlatform(id);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            return new Result(false,"删除失败");
        }
    }

    @RequestMapping("/getformById")
    public Platform getformById(@RequestParam("id") Long id){
        return platformService.getPlatformById(id);
    }
}
