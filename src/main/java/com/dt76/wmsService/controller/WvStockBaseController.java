package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.WvStockBase;
import com.dt76.wmsService.service.WvStockBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/WvStockBaseMapper")
public class WvStockBaseController {

    //注入服务层接口
    @Autowired
    private WvStockBaseService wvStockBaseService;

//    //json格式
//    @ResponseBody

    @RequestMapping("/getAll")
    @ResponseBody
        public List<WvStockBase> qryAll(){
//        List<WvStockBase> wvStockBases = new wvStockBaseService<>();
        System.out.println("成功调用远程服务");
        List<WvStockBase> list = wvStockBaseService.qryAll();
        System.out.println(list);
        return wvStockBaseService.qryAll();
    }



}
