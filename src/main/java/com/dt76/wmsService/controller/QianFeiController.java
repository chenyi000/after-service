package com.dt76.wmsService.controller;

import com.dt76.wmsService.service.QianFeiService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/qianFei")
public class QianFeiController {

    @Autowired
    private QianFeiService qianFeiService;

    @RequestMapping("/findQianFei")
    public List<DayCostPageUtil> getPageAndCondition(@RequestBody DayCostSearchUtil dayCostSearchUtil) {
        System.out.println("进入后台调用");
        //测试用
        List<DayCostPageUtil> dayCostPageUtils = qianFeiService.findQianFei(dayCostSearchUtil);
        return dayCostPageUtils;
    }

    @RequestMapping("/findById")
    public DayCostPageUtil findById(@RequestParam("id") int id) {
        return qianFeiService.findById(id);
    }

}



