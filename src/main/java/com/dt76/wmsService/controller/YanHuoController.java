package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.Result;
import com.dt76.wmsService.pojo.Yanhuo;
import com.dt76.wmsService.service.YanHuoService;
import com.dt76.wmsService.utils.OrderPageUtil;
import com.dt76.wmsService.utils.YanHuoPageUtil;
import com.dt76.wmsService.utils.YanHuoSearchUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: u3
 * @description: 验货
 * @author: wx
 * @create: 2019-05-12 21:33
 */
@RestController
@RequestMapping("/yanhuo")
public class YanHuoController {

    @Autowired
    private YanHuoService yanHuoService;

    @RequestMapping("/findAll")
    public List<YanHuoPageUtil> findAll(@RequestBody YanHuoSearchUtil yanHuoSearchUtil){
        System.out.println("成功调用远程服务:YanHuoService>findAll>>>");
        List<YanHuoPageUtil> employeePageUtils = yanHuoService.findAll(yanHuoSearchUtil);
        for (YanHuoPageUtil yanHuoPageUtil:employeePageUtils) {
            if (yanHuoPageUtil.getStatusId()==3){
                yanHuoPageUtil.setStatus1("待验货");
            }else {
                yanHuoPageUtil.setStatus1("已验货");
            }
        }
        System.out.println(employeePageUtils);
        return employeePageUtils;
    }

    @RequestMapping("/findAll2")
    public List<YanHuoPageUtil> findAll2(@RequestBody YanHuoSearchUtil yanHuoSearchUtil){
        System.out.println("成功调用远程服务:YanHuoService>findAll>>>");
        List<YanHuoPageUtil> employeePageUtils = yanHuoService.findAll2(yanHuoSearchUtil);
        for (YanHuoPageUtil yanHuoPageUtil:employeePageUtils) {
            if (yanHuoPageUtil.getStatusId()==4){
                yanHuoPageUtil.setStatus2("待上架");
            }else {
                yanHuoPageUtil.setStatus2("已上架");
            }
        }
        System.out.println(employeePageUtils);
        return employeePageUtils;
    }

    @RequestMapping("/findAll3")
    public List<YanHuoPageUtil> findAll3(@RequestBody YanHuoSearchUtil yanHuoSearchUtil){
        System.out.println("成功调用远程服务:YanHuoService>findAll3>>>");
        List<YanHuoPageUtil> employeePageUtils = yanHuoService.findAll3(yanHuoSearchUtil);
        System.out.println(employeePageUtils);
        return employeePageUtils;
    }

   /* @RequestMapping("/findAll")
    public PageInfo findAll(@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);
        System.out.println("成功调用远程服务:orderService>qryAll>>>");
        List<Yanhuo> list = yanHuoService.findAll();
        PageInfo pageInfo = new PageInfo(list,5);
        return pageInfo;
    }*/


    /*@RequestMapping("/findAll2")
    public List<Yanhuo> findAll2(){
        System.out.println("成功调用远程服务:YanHuoService>findAll2>>>");
        return yanHuoService.findAll2();
    }*/

  /*  @RequestMapping("/findAll2")
    public PageInfo findAll2(@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);
        System.out.println("成功调用远程服务:orderService>qryAll2>>>");
        List<Yanhuo> list = yanHuoService.findAll2();
        PageInfo pageInfo = new PageInfo(list,5);
        return pageInfo;
    }*/



    @RequestMapping("/getOne")
    public YanHuoPageUtil getOne(@RequestParam("id") int id){
        System.out.println("成功调用远程服务:YanHuoService>getOne>>>");
        return yanHuoService.getOne(id);
    }

    @RequestMapping("/add")
    public Result add(@RequestParam("spId") int spId){
        System.out.println("成功调用远程服务:YanHuoService>add>>>");
        yanHuoService.add(spId);
        return new Result(true,"新增成功");
    }

    @RequestMapping("/add2")
    public Result add2(@RequestParam("spId") int spId){
        System.out.println("成功调用远程服务:YanHuoService>add2>>>");
        yanHuoService.upd(spId);
        return new Result(true,"新增成功");
    }
}
