package com.dt76.wmsService.controller;

import com.dt76.wmsService.commen.Result;
import com.dt76.wmsService.service.ShangjiaService;
import com.dt76.wmsService.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-15 15:52
 */
@RestController
public class ShangjiaController {

    @Autowired
    private ShangjiaService shangjiaService;

    @RequestMapping("/sh/add")
    public Result add(@RequestBody ShangjiaSearchUtil shangjiaSearchUtil){
        shangjiaService.add(shangjiaSearchUtil);
        return new Result(true,"插入成功");
    }

    @RequestMapping("/sh/findAll")
    public List<ShangjiaPageUtil> findAll(@RequestBody ShangjiaSearchUtil2 shangjiaSearchUtil2){
        System.out.println("成功调用远程服务:shangjiaService>findAll>>>");
        List<ShangjiaPageUtil> employeePageUtils = shangjiaService.findAll(shangjiaSearchUtil2);
        System.out.println(employeePageUtils);
        return employeePageUtils;
    }

    @RequestMapping("/sh/getCount")
    public List<kuCount> getCount(@RequestParam("id") int id){

        return shangjiaService.getCount(id);
    }

}
