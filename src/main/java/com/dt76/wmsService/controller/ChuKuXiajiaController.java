package com.dt76.wmsService.controller;

import com.dt76.wmsService.service.ChuKuXiajiaService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*
 *Created by 王超 on 2019/5/14
 */
@RestController//接口统一用json返回
@RequestMapping("/xiajia")
public class ChuKuXiajiaController {
    @Autowired
    private ChuKuXiajiaService chuKuXiajiaService;
    @RequestMapping("/get")
    @ResponseBody
    public Page getAll(@RequestParam("pageIndex") int pageIndex,@RequestParam(value = "jjr",required = false) String jjr){
        System.out.println("成功调用远程服务》》》》》》》》》chuKuXiajiaService》》》getAll");
        Page page = chuKuXiajiaService.getAllXiajia(pageIndex,jjr);
        return page;
    }


    @RequestMapping("/gai")
    public void modify(@RequestParam("spId") int spId){
        chuKuXiajiaService.gaiXia(spId);
    }

}
