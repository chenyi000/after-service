package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsHuiDan;
import com.dt76.wmsService.service.ChukuHuidanService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
 *Created by 王超 on 2019/5/17
 */
@RestController
@RequestMapping("/huidan")
public class ChukuHuidanController {
    @Autowired
    private ChukuHuidanService chukuHuidanService;

    @RequestMapping("/luru")
    public void add(@RequestBody WmsHuiDan wmsHuiDan){
        chukuHuidanService.luru(wmsHuiDan);
    }

    @RequestMapping("/get")
    public List<Plat> getAll(){
        List<Plat> platList = chukuHuidanService.getAll();
        return platList;
    }


//查询所有已回单的商品1.0
//    @RequestMapping("/getAll")
//    public List<ChukuTongzhi> getAll1(){
//        List<ChukuTongzhi> chukuTongzhiList = chukuHuidanService.getSuoyou();
//        return chukuTongzhiList;
//    }

    //查询所有已回单的商品2.0
    @RequestMapping("/getAll")
    public Page getAll(@RequestParam("pageIndex") int pageIndex, @RequestParam(value = "jjr",required = false) String jjr){
        System.out.println("成功调用远程服务>>>>>chukuHuidanService>>getAll");
        Page page = chukuHuidanService.getSuoyou(pageIndex,jjr);
        return page;
    }


    @RequestMapping("/modify")
    public String modify(@RequestParam("chuDocId") String chuDocId){
        chukuHuidanService.modify(chuDocId);
        return "商品状态已设置为已回单！";
    }
    //删除回单记录
    @RequestMapping("/del")
    public String delete(@RequestParam("docId") int docId){
        chukuHuidanService.delete(docId);
        return "删除成功！";
    }
}



