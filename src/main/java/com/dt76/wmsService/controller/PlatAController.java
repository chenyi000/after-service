package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.PlatAService;
import com.dt76.wmsService.utils.Page;
import com.dt76.wmsService.utils.Result;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/platA")
public class PlatAController {
    @Autowired
    private PlatAService platAService;

    @RequestMapping("/qryAll")
    public Page qryPageAll(@RequestParam("pageIndex") int pageIndex,
                           @RequestParam(value = "doc", required = false) String doc,
                           @RequestParam(value = "plat", required = false) String plat) {
        System.out.println("成功调用服务");
        return platAService.getAll(pageIndex, doc, plat);
    }

    @RequestMapping("/udp")
    public Result udp(@RequestParam("plat") String plat) {
        Gson gson = new Gson();
        Plat json = gson.fromJson(plat, Plat.class);

        System.out.println("成功调用服务");
        try {
            platAService.updPlat(json);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            return new Result(false, "修改异常");
        }
    }


    @RequestMapping("/del")
    public Result delete(@RequestParam("id") int id) {
        System.out.println("成功调用远程服务》》》");
        try {
            platAService.delPlat(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            return new Result(false, "删除失败");
        }
    }

    @RequestMapping("/modifyId")
    public void modify(@RequestParam("spId") int spId,
                       @RequestParam("statusId") int statusId) {
        try {
            if (statusId == 2) {
                statusId = 3;
            }else if (statusId == 7) {
                statusId = 8;
            }else if (statusId == 13) {
                statusId = 14;
            } else {
                System.out.println("状态不正确");
            }
            platAService.udpShopStuIdById(spId, statusId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @RequestMapping("/qryPlatById")
    public Plat qryPlatById(@RequestParam("id") Long id) {
        return platAService.getById(id);
    }

    @RequestMapping("/qrySpById")
    public WmsShop qrySpById(@RequestParam("id") int id) {
        return platAService.getShopById(id);
    }

    @RequestMapping("/modifyInDate")
    public void modifyInDate(@RequestParam("pId") int pId){
        platAService.udpPlatInDate(pId);
    }

    @RequestMapping("/modifyOutDate")
    public void modifyOutDate(@RequestParam("pId") int pId){
        platAService.udpPlatOutDate(pId);
    }
}
