package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.pojo.Cuweijihua;
import com.dt76.wmsService.service.CuweiJihuaService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController //接口统一用json返回
@RequestMapping("/cuweijihua")
public class CuweiJihuaController {
    @Autowired
    private CuweiJihuaService cuweiJihuaService;
    @RequestMapping("/findPageConditionAll")
    public Page findPageConditionAll(@RequestParam("pageIndex") int pageIndex,

                                     @RequestParam(value="goodsName",required = false)String goodsName
                                    ){
        System.out.println("成功调用远程服务:cuweiJihuaService>findPageConditionAll>>>");
        return cuweiJihuaService.findAllCuweiJihua(pageIndex,goodsName);
    }
    @RequestMapping("/findAll")
    @ResponseBody
    public List<Cuweijihua> findAll(){
        System.out.println("成功调用远程服务:cuweiJihuaService>findAll>>>");
        return cuweiJihuaService.findALlCuweiJihua();
    }


    @RequestMapping("/findOne")
    public Cuweijihua findOne(@RequestParam("odId") int odId){
        return cuweiJihuaService.get(odId);
    }
}
