package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.KuCunA;
import com.dt76.wmsService.utils.Page;

import java.util.List;

public interface KuCunAService {
    Page findAllKuCunA(int pageIndex, String goodsName,String chuweiName);

    List<KuCunA> findALlKuCunA();
}
