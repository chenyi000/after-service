package com.dt76.wmsService.service;

        import com.dt76.wmsService.pojo.ChukuTongzhi;
        import com.dt76.wmsService.pojo.Plat;
        import com.dt76.wmsService.utils.ChukuTongzhiPageUtil;
        import com.dt76.wmsService.utils.ChukuTongzhiSearchUtil;
        import com.dt76.wmsService.utils.Page;

        import java.util.List;

/*
 *Created by 王超 on 2019/5/10
 */
public interface ChukuTongzhiService {
    //    List<ChukuTongzhi> getChuku();
//分页查询
    Page getAllChuku(int pageIndex,String sjr,String ddh);



    //一个开始出库按钮同时执行2种操作
    void xiuAdd(int id);

    //单条商品信息查询
    ChukuTongzhi get(int spId);

}
