package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;

import java.util.List;

public interface DayCostService {


    //获得所有费用明细
    List<WmsDayCost> getAllDayCost();


    //动态分页查询费用明细
    List<DayCostPageUtil> findDayCost(DayCostSearchUtil dayCostSearchUtil);

    //添加
    int addDayCost(WmsDayCost wmsDayCost);

    //删除
    int delDayCost(int id);

    List<SysCustomer> findName(String name);

    int updataCost(WmsDayCost wmsDayCost);

    DayCostPageUtil findById(int id);

    DayCostPageUtil findShop(String name);

}
