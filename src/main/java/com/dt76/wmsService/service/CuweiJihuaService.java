package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Cuweijihua;
import com.dt76.wmsService.utils.Page;

import java.util.List;

public interface CuweiJihuaService {
    Page findAllCuweiJihua(int pageIndex,String goodsName
                           );
    List<Cuweijihua> findALlCuweiJihua();
    //单条商品信息查询
    Cuweijihua get(int odId);

}