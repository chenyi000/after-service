package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.WmsShop;

import java.util.List;

public interface ShopService {
    //增加商品
    boolean addShop(WmsShop wmsShop);
    //查询
    List<WmsShop> getAll();
    //查询单条
    WmsShop getOne(int id);
    //修改状态
    boolean upd(WmsShop wmsShop);

    boolean updShop(WmsShop wmsShop);

    int qryCount1();
    int qryCount2();
    int qryCount3();
    int qryCount4();
}
