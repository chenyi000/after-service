package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Yanhuo;
import com.dt76.wmsService.utils.YanHuoPageUtil;
import com.dt76.wmsService.utils.YanHuoSearchUtil;

import java.util.List;

/**
 * @program: u3
 * @description: 验货
 * @author: wx
 * @create: 2019-05-12 21:30
 */
public interface YanHuoService {

    List<YanHuoPageUtil> findAll(YanHuoSearchUtil yanHuoSearchUtil);
    List<YanHuoPageUtil> findAll2(YanHuoSearchUtil yanHuoSearchUtil);
    List<YanHuoPageUtil> findAll3(YanHuoSearchUtil yanHuoSearchUtil);

    YanHuoPageUtil getOne(int id);

    boolean add(int spId);

    boolean upd(int spId);
}
