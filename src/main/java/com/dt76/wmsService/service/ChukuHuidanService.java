package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsHuiDan;
import com.dt76.wmsService.utils.Page;

import java.util.List;

/*
 *Created by 王超 on 2019/5/17
 */
public interface ChukuHuidanService {

    //录入一笔回单
    void  luru(WmsHuiDan wmsHuiDan);
    //查询所有可以录入的出库单号
    List<Plat> getAll();

    //查询所有已回单的商品
    //1.0查询
//    List<ChukuTongzhi> getSuoyou();
  //2.0查询
 Page getSuoyou(int pageIndex,String jjr);


    //修改商品的状态(已回单)
    void modify(String chuDocId);


    //根据出库单号删除回单记录
    void delete(int docId);
}
