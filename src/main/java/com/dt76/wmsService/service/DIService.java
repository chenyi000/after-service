package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.DifferencesInventory;

import java.util.List;

public interface DIService {

    List<DifferencesInventory> qryAll();
}
