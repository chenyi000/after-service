package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.KuCunB;
import com.dt76.wmsService.utils.Page;

import java.util.List;

public interface KuCunBService {
    Page findAllKuCunB(int pageIndex, String goodsName, String chuweiName);

    List<KuCunB> findALlKuCunB();
}
