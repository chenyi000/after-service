package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Quality;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface QualityService {
    //查询所有
    List<Quality> getAllQuality();

    //增加状态
    boolean addQuality(Quality quality);

    //修改品检
    boolean updateQuality(Quality quality);

    //带条件查询
    Quality toqryQuality(int ID);

    //带条件删除
    int delQuality(@Param("ID") int ID);
}
