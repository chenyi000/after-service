package com.dt76.wmsService.service;

import com.dt76.wmsService.utils.Page;

/*
 *Created by 王超 on 2019/5/17
 */
public interface ChuFuheService {
    //分页查询所有待复核的商品
    Page getAllFuhe(int pageIndex,String ddh);

    //点击复核按钮修改状态
    void modify(int spId);
}
