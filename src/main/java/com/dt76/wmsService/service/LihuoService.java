package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Lihuo;
import com.dt76.wmsService.utils.Page;

import java.util.List;

public interface LihuoService {
    Page findAllLihuo(int pageIndex, String goodsName, String chuweiName,String tuopanName);

    List<Lihuo> findALlLihuo();
}
