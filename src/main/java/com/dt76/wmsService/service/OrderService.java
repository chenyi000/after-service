package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.WmsOrder;
import com.dt76.wmsService.utils.OrderPageUtil;
import com.dt76.wmsService.utils.OrderSearchUtil;

import java.util.List;

public interface OrderService {
    //增加订单
    boolean addOrder(WmsOrder wmsOrder);
    //查询
    List<WmsOrder> getAll();
    //查询订单详情
    List<OrderPageUtil> getAll2(OrderSearchUtil orderSearchUtil);
    //查询单条订单
    OrderPageUtil getById(int id);
    //修改
    boolean updOrder(WmsOrder wmsOrder);
}
