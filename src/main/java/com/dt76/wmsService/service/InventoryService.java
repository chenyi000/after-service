package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Inventory;

import java.util.List;

public interface InventoryService {

    List<Inventory> qryAll();
}
