package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.BShop;
import com.dt76.wmsService.utils.BShopPageUtil;
import com.dt76.wmsService.utils.BShopSearchUtil;

import java.util.List;

public  interface BShopService {
    //新增
    Integer addBshop(BShop bShop);
    //修改
    Integer updBshop(BShop bShop);
    //删除
    Integer delBshop(int storeId);
    //分页+条件查询
    List<BShopPageUtil> finBshop(BShopSearchUtil bShopSearchUtil);
    //查询单个
   BShop getBshopById(int storeId);
   //查询所有
    List<BShop> findALlBshop();

}
