package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Platform;
import com.dt76.wmsService.utils.Page;

import java.util.List;

public interface PlatformService {
        List<Platform>  getPlatformAll();

        Page getAll(int pageIndex,String qryCode,String qryName);

        void addPlatform(Platform platform);

        void updPlatform(Platform platform);

        Platform getPlatformById(Long id);
        void delPlatform(int id);
}
