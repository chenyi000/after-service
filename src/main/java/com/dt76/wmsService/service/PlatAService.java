package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.utils.Page;


public interface PlatAService {

    Page getAll(int pageIndex,String doc,String plat);

    void updPlat(Plat plat);

    void delPlat(int id);

    void udpShopStuIdById(int spId,int statusId);

     Plat getById(Long id);

    WmsShop getShopById(int id);

    void udpPlatInDate(int pId);

    void udpPlatOutDate(int pId);
}
