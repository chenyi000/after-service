package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.SysEmployee;
import com.dt76.wmsService.pojo.TbFunctions;

import java.util.List;


/**
 * @Description
 * @auther jun
 * @create 2019-05-09 12:31
 */

public interface AccountService {

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    SysEmployee login(String username, String password);


    public SysEmployee findByUsername(String username);

    public List<SysEmployee> findAll();

    public List<TbFunctions> findFuncByUserId(Long userId);
}
