package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.BaStore;
import com.dt76.wmsService.utils.Page;

import java.util.List;
/*
   增加：add
   删除：del
   修改：upd
   统计个数：get
   查询：find
 */
public interface BaStoreService {
    Page findAllbaStore(int pageIndex, String storeCode, String storeName);
    List<BaStore> findALlbaStore();
    void addbaStore(BaStore baStore);

    BaStore findbaStoreById(int storeId);

    void udpbaStore(BaStore baStore);

    void delbaStore(int storeId);
}
