package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.WvStockBase;

import java.util.List;

public interface WvStockBaseService {

    List<WvStockBase> qryAll();
}
