package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;

import java.util.List;

public interface QianFeiService {

    //获得所有费用明细
    //  List<WmsDayCost> getAllDayCost();

    //修改费用明细
    // void udpDayCost(WmsDayCost wmsDayCost);

    //动态分页查询费用明细
    List<DayCostPageUtil> findQianFei(DayCostSearchUtil dayCostSearchUtil/*int pageIndex, String cusName,
                                            String startCostData,
                                            String endCostData,
                                            String costName,
                                            String costJs*/);

    //添加
    // int addDayCost(WmsDayCost wmsDayCost);

    // int delDayCost(int id);

    DayCostPageUtil findById(int id);

}
