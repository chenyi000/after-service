package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Cheliang;
import com.dt76.wmsService.utils.Page;

public interface CheliangService {

    Page getAll(int pageIndex, String cheNo, String sj,String stu);

    void addChe(Cheliang cheliang);

    void updChe(Cheliang cheliang);

    void delChe(int id);
}
