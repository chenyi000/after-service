package com.dt76.wmsService.service;

import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.ZhangDanPageUtil;
import com.dt76.wmsService.utils.ZhangDanSearchUtil;

import java.util.List;

public interface ZhangDaoService {

    List<ZhangDanPageUtil> findZd(ZhangDanSearchUtil zhangDanSearchUtil);

    List<DayCostPageUtil> exportBill(ZhangDanSearchUtil zhangDanSearchUtil);
}
