package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.SysEmployee;
import com.dt76.wmsService.utils.EmployeePageUtil;
import com.dt76.wmsService.utils.EmployeeSearchUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 15:13
 */
public interface EmployeeService {

    /**
     * 员工增加
     * @param sysEmployee
     * @return
     */
    Integer addEmployee(SysEmployee sysEmployee);

    /**
     * 员工修改
     * @param sysEmployee
     * @return
     */
    Integer updEmployee(SysEmployee sysEmployee);

    /**
     * 根据搜索条件进行分页
     * @param employeeSearchUtil
     * @return
     */
    List<EmployeePageUtil> finEmployee(EmployeeSearchUtil employeeSearchUtil);


    /**
     * 删除
     * @param id
     * @return
     */
    Integer delEmployee(Long id);

    /**
     * 单个查询
     * @param id
     * @return
     */
    SysEmployee getEmployeeById(Long id);
}
