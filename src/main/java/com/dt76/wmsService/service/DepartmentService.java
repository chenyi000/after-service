package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.SysDepartment;
import com.dt76.wmsService.utils.DepartmentPageUtil;
import com.dt76.wmsService.utils.DepartmentSearchUtil;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 13:51
 */
public interface DepartmentService {


    /**
     * 按条件查询部门
     * @param departmentSearchUtil
     * @return
     */
    List<DepartmentPageUtil> finDepartment(DepartmentSearchUtil departmentSearchUtil);

    /**
     * 增加
     * @param sysEpartment
     * @return
     */
    Integer addDepartment(SysDepartment sysEpartment);

    /**
     * 删除
     * @param id
     * @return
     */
    Integer delDepartment(Long id);

    /**
     * 修改
     * @param sysEpartment
     * @return
     */
    Integer updDepartment(SysDepartment sysEpartment);




}
