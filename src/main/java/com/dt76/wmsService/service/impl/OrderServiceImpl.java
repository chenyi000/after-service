package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.OrderMapper;
import com.dt76.wmsService.pojo.WmsOrder;
import com.dt76.wmsService.service.OrderService;
import com.dt76.wmsService.service.WebSocket;
import com.dt76.wmsService.utils.OrderPageUtil;
import com.dt76.wmsService.utils.OrderSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public boolean addOrder(WmsOrder wmsOrder) {
        orderMapper.insert(wmsOrder);
        return true;
    }

    @Override
    public List<WmsOrder> getAll() {
        return orderMapper.qryAll();
    }


    @Override
    public List<OrderPageUtil> getAll2(OrderSearchUtil orderSearchUtil) {

        Integer count = orderMapper.getCount(orderSearchUtil);
        orderSearchUtil.setPageSumCount(count);

        Integer pageNum=count%orderSearchUtil.getPageSize() > 0
                ? count/orderSearchUtil.getPageSize()+1
                : count/orderSearchUtil.getPageSize();

        orderSearchUtil.setStart(orderSearchUtil.getPageSize()*(orderSearchUtil.getPageIndex()-1));

        orderSearchUtil.setPageNum(pageNum);
        List<OrderPageUtil> employeePage = orderMapper.qryAll2(orderSearchUtil);

        for(int i =0; i< employeePage.size(); i++){
            employeePage.get(i).setPageIndex(orderSearchUtil.getPageIndex());
            employeePage.get(i).setPageNum(orderSearchUtil.getPageNum());
            employeePage.get(i).setPageSize(orderSearchUtil.getPageSize());
            employeePage.get(i).setPageSumCount(orderSearchUtil.getPageSumCount());
        }
        return employeePage;

    }

    @Override
    public OrderPageUtil getById(int id) {
        return orderMapper.qryById(id);
    }

    @Override
    public boolean updOrder(WmsOrder wmsOrder) {
        int row = orderMapper.upd(wmsOrder);
        if (row==1){
            System.out.println("修改成功");
        }
        return true;
    }
}
