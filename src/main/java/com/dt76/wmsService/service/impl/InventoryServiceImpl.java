package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.InventoryMapper;
import com.dt76.wmsService.pojo.Inventory;
import com.dt76.wmsService.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryMapper inventoryMapper;

    @Override
    public List<Inventory> qryAll(){
        return inventoryMapper.qryAll();
    }

}
