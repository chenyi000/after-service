package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.KuCunAMapper;
import com.dt76.wmsService.pojo.KuCunA;
import com.dt76.wmsService.service.KuCunAService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KuCunAServiceImpl implements KuCunAService {
    @Autowired
    private KuCunAMapper kuCunAMapper;
    @Override
    public Page findAllKuCunA(int pageIndex, String goodsName, String chuweiName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(kuCunAMapper.getConditionCount(goodsName,chuweiName));
        List<KuCunA> kuCunAList = kuCunAMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),goodsName,chuweiName
        );
        page.setRows(kuCunAList);

        return page;
    }

    @Override
    public List<KuCunA> findALlKuCunA() {
        return kuCunAMapper.findAll();
    }
}
