package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.CuweiJihuaMapper;
import com.dt76.wmsService.pojo.Cuweijihua;
import com.dt76.wmsService.service.CuweiJihuaService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CuweiJihuaServiceImpl implements CuweiJihuaService {
    @Autowired
    private CuweiJihuaMapper cuweiJihuaMapper;
    @Override
    public Page findAllCuweiJihua(int pageIndex,String goodsName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(cuweiJihuaMapper.getConditionCount(goodsName));
        List<Cuweijihua> cuweijihuaList = cuweiJihuaMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),goodsName
        );
        page.setRows(cuweijihuaList);

        return page;
    }

    @Override
    public List<Cuweijihua> findALlCuweiJihua() {
        return cuweiJihuaMapper.findAll();
    }

    @Override
    public Cuweijihua get(int odId) {
        return cuweiJihuaMapper.findOne(odId);
    }
}
