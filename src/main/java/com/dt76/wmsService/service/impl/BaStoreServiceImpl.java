package com.dt76.wmsService.service.impl;



import com.dt76.wmsService.mapper.BaStoreMapper;
import com.dt76.wmsService.pojo.BaStore;
import com.dt76.wmsService.service.BaStoreService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BaStoreServiceImpl implements BaStoreService {
     @Autowired
     private BaStoreMapper baStoreMapper;
    @Override
    public Page findAllbaStore(int pageIndex, String storeCode, String storeName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(baStoreMapper.getConditionCount(storeCode,storeName));
        List<BaStore> baStoreList = baStoreMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),storeCode,storeName
        );
        page.setRows(baStoreList);

        return page;
    }

    @Override
    public List<BaStore> findALlbaStore() {
        return baStoreMapper.findAll();
    }

    @Override
    public void addbaStore(BaStore baStore) {
        baStoreMapper.add(baStore);
    }

    @Override
    public BaStore findbaStoreById(int storeId) {
        return baStoreMapper.findOne(storeId);
    }

    @Override
    public void udpbaStore(BaStore baStore) {
         baStoreMapper.update(baStore);
    }

    @Override
    public void delbaStore(int storeId) {
         baStoreMapper.delete(storeId);
    }
}
