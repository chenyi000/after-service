package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.ChuweiMapper;
import com.dt76.wmsService.pojo.Chuwei;
import com.dt76.wmsService.service.ChuweiService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChuweiServiceImpl implements ChuweiService {
    @Autowired
    private ChuweiMapper chuweiMapper;
    @Override
    public Page findAllcuWei(int pageIndex, String storeName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(chuweiMapper.getConditionPageAll(storeName));
        List<Chuwei> chuweiList = chuweiMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),storeName
        );
        System.out.println(chuweiList);
        page.setRows(chuweiList);

        return page;
    }
}
