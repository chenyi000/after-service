package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.KuCunBMapper;
import com.dt76.wmsService.pojo.KuCunB;
import com.dt76.wmsService.service.KuCunBService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KuCunBServiceImpl implements KuCunBService {
    @Autowired
    private KuCunBMapper kuCunBMapper;
    @Override
    public Page findAllKuCunB(int pageIndex, String goodsName, String chuweiName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(kuCunBMapper.getConditionCount(goodsName,chuweiName));
        List<KuCunB> kuCunBList = kuCunBMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),goodsName,chuweiName
        );
        page.setRows(kuCunBList);

        return page;
    }

    @Override
    public List<KuCunB> findALlKuCunB() {
        return kuCunBMapper.findAll();
    }
}
