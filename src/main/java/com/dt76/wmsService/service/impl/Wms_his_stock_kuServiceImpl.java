package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.Wms_his_stock_kuMapper;
import com.dt76.wmsService.pojo.Wms_his_stock_ku;
import com.dt76.wmsService.service.Wms_his_stock_kuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class Wms_his_stock_kuServiceImpl implements Wms_his_stock_kuService {

    @Autowired
    private Wms_his_stock_kuMapper wms_his_stock_kuMapper;
    @Override
    public List<Wms_his_stock_ku> findAllWms_his_stock_ku() {
        return wms_his_stock_kuMapper.findAllWms_his_stock_ku();
    }
}
