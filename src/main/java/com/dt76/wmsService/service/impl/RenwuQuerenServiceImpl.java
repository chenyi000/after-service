package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.RenwuQuerenMapper;
import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.service.ReneuQuerenService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 *Created by 王超 on 2019/5/12
 */
@Service
public class RenwuQuerenServiceImpl implements ReneuQuerenService {
    @Autowired
    private RenwuQuerenMapper renwuQuerenMapper;
//    @Override
//    public List<ChukuTongzhi> getAll() {
//        return renwuQuerenMapper.get();
//    }

    @Override
    public Page getAllRenwu(int pageIndex
            ,String sp
    ) {
        Page page = new Page();
        page.setPageIndex(pageIndex);
        //更新总条数
        page.setTotalCount(renwuQuerenMapper.qryCount(
                sp
        ));
        List<ChukuTongzhi> chukuTongzhiList = renwuQuerenMapper.get((page.getPageIndex()-1)*page.getPageSize(),page.getPageSize()
                ,sp
        );
        page.setRows(chukuTongzhiList);
        return page;
    }

    @Override
    public void modiffy(int id) {
        renwuQuerenMapper.udp(id);
    }

    @Override
    public void GaiInsert(int id) {
        renwuQuerenMapper.gai(id);
        renwuQuerenMapper.insert(id);

    }

//    @Override
//    public void Insert(int id) {
//        renwuQuerenMapper.insert(id);
//    }
//
//    @Override
//    public void Gai(int id) {
//      renwuQuerenMapper.gai(id);
//    }


}
