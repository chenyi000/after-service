package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.DayCostMapper;
import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.service.DayCostService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import com.dt76.wmsService.utils.EmployeePageUtil;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DayCostServiceImpl implements DayCostService {

    @Autowired
    private DayCostMapper dayCostMapper;

    @Override
    public DayCostPageUtil findShop(String name) {
        return dayCostMapper.findShop(name);
    }

    @Override
    public List<DayCostPageUtil> findDayCost(DayCostSearchUtil dayCostSearchUtil) {

        Integer count = dayCostMapper.getConditionCount(dayCostSearchUtil);
        dayCostSearchUtil.setPageSumCount(count);

        Integer pageNum = count % dayCostSearchUtil.getPageSize() > 0
                ? count / dayCostSearchUtil.getPageSize() + 1
                : count / dayCostSearchUtil.getPageSize();

        dayCostSearchUtil.setStart(dayCostSearchUtil.getPageSize() * (dayCostSearchUtil.getPageIndex() - 1));

        dayCostSearchUtil.setPageNum(pageNum);
        List<DayCostPageUtil> dayCostPage = dayCostMapper.findDayCost(dayCostSearchUtil);

        for (int i = 0; i < dayCostPage.size(); i++) {
            dayCostPage.get(i).setPageIndex(dayCostSearchUtil.getPageIndex());
            dayCostPage.get(i).setPageNum(dayCostSearchUtil.getPageNum());
            dayCostPage.get(i).setPageSize(dayCostSearchUtil.getPageSize());
            dayCostPage.get(i).setPageSumCount(dayCostSearchUtil.getPageSumCount());
        }
        return dayCostPage;
    }

    @Override
    public List<WmsDayCost> getAllDayCost() {
        return null;
    }


    @Override
    public int addDayCost(WmsDayCost wmsDayCost) {
        if (wmsDayCost.getId() == 0) {
            return dayCostMapper.addDayCost(wmsDayCost);

        } else {
            return dayCostMapper.updataDayCost(wmsDayCost);
        }
    }

    @Override
    public int delDayCost(int id) {
        return dayCostMapper.delDayCost(id);
    }

    @Override
    public int updataCost(WmsDayCost wmsDayCost) {
        return dayCostMapper.updataDayCost(wmsDayCost);
    }

    @Override
    public List<SysCustomer> findName(String name) {
        return dayCostMapper.getName(name);
    }

    @Override
    public DayCostPageUtil findById(int id) {
        return dayCostMapper.findById(id);
    }
}
