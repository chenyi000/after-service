package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.KongMapper;
import com.dt76.wmsService.pojo.Kongchuwei;
import com.dt76.wmsService.service.KongService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KongServiceImpl implements KongService {
    @Autowired
    private KongMapper kongMapper;
    @Override
    public Page findAllkongchuwei(int pageIndex, String storeName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(kongMapper.getConditionCount(storeName));
        List<Kongchuwei> kongList = kongMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),storeName
        );
        page.setRows(kongList);

        return page;
    }
}
