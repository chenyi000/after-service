package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.EmployeeMapper;
import com.dt76.wmsService.pojo.SysEmployee;
import com.dt76.wmsService.service.EmployeeService;
import com.dt76.wmsService.utils.EmployeePageUtil;
import com.dt76.wmsService.utils.EmployeeSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 15:14
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {


    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工增加
     *
     * @param sysEmployee
     * @return
     */
    @Override
    public Integer addEmployee(SysEmployee sysEmployee) {
        return employeeMapper.addEmployee(sysEmployee);
    }

    /**
     * 员工修改
     *
     * @param sysEmployee
     * @return
     */
    @Override
    public Integer updEmployee(SysEmployee sysEmployee) {
        return employeeMapper.updEmployee(sysEmployee);
    }

    /**
     * 根据搜索条件进行分页
     *
     * @param employeeSearchUtil
     * @return
     */
    @Override
    public List<EmployeePageUtil> finEmployee(EmployeeSearchUtil employeeSearchUtil) {
        Integer count = employeeMapper.getEmployeeCount(employeeSearchUtil);
        employeeSearchUtil.setPageSumCount(count);

        Integer pageNum = count % employeeSearchUtil.getPageSize() > 0
                ? count / employeeSearchUtil.getPageSize() + 1
                : count / employeeSearchUtil.getPageSize();

        employeeSearchUtil.setStart(employeeSearchUtil.getPageSize() * (employeeSearchUtil.getPageIndex() - 1));

        employeeSearchUtil.setPageNum(pageNum);
        List<EmployeePageUtil> employeePage = employeeMapper.finEmployee(employeeSearchUtil);

        for (int i = 0; i < employeePage.size(); i++) {
            employeePage.get(i).setPageIndex(employeeSearchUtil.getPageIndex());
            employeePage.get(i).setPageNum(employeeSearchUtil.getPageNum());
            employeePage.get(i).setPageSize(employeeSearchUtil.getPageSize());
            employeePage.get(i).setPageSumCount(employeeSearchUtil.getPageSumCount());
        }
        return employeePage;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public Integer delEmployee(Long id) {
        return employeeMapper.delEmployee(id);
    }

    /**
     * 单个查询
     *
     * @param id
     * @return
     */
    @Override
    public SysEmployee getEmployeeById(Long id) {
        return employeeMapper.getEmployeeById(id);
    }


}
