package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.BShopMapper;
import com.dt76.wmsService.pojo.BShop;
import com.dt76.wmsService.service.BShopService;
import com.dt76.wmsService.utils.BShopPageUtil;
import com.dt76.wmsService.utils.BShopSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BShopServiceImpl implements BShopService {
    @Autowired
    private BShopMapper bShopMapper;
    @Override
    public Integer addBshop(BShop bShop) {
        return bShopMapper.add(bShop);
    }

    @Override
    public Integer updBshop(BShop bShop) {
        return bShopMapper.update(bShop);
    }

    @Override
    public Integer delBshop(int storeId) {
        return bShopMapper.delete(storeId);
    }

    @Override
    public List<BShopPageUtil> finBshop(BShopSearchUtil bShopSearchUtil) {
        Integer count =bShopMapper.getConditionCount(bShopSearchUtil);
        bShopSearchUtil.setPageSumCount(count);


        Integer pageNum=count%bShopSearchUtil.getPageSize() > 0
                ? count/bShopSearchUtil.getPageSize()+1
                : count/bShopSearchUtil.getPageSize();

        bShopSearchUtil.setStart(bShopSearchUtil.getPageSize()*(bShopSearchUtil.getPageIndex()-1));

        bShopSearchUtil.setPageNum(pageNum);
        List<BShopPageUtil> bshopPage = bShopMapper.findConditionPageAll(bShopSearchUtil);

        for(int i =0; i< bshopPage.size(); i++){
            bshopPage.get(i).setPageIndex(bShopSearchUtil.getPageIndex());
            bshopPage.get(i).setPageNum(bShopSearchUtil.getPageNum());
            bshopPage.get(i).setPageSize(bShopSearchUtil.getPageSize());
            bshopPage.get(i).setPageSumCount(bShopSearchUtil.getPageSumCount());
        }
        return bshopPage;
    }

    @Override
    public BShop getBshopById(int storeId) {
        return bShopMapper.findOne(storeId);
    }

    @Override
    public List<BShop> findALlBshop() {
        return bShopMapper.findAll();
    }
}
