package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.LihuoMapper;
import com.dt76.wmsService.pojo.Lihuo;
import com.dt76.wmsService.service.LihuoService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LihuoServiceImpl implements LihuoService {
    @Autowired
    private LihuoMapper lihuoMapper;
    @Override
    public Page findAllLihuo(int pageIndex, String goodsName, String chuweiName, String tuopanName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(lihuoMapper.getConditionCount(goodsName,chuweiName,tuopanName));
        List<Lihuo> lihuoList = lihuoMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),goodsName,chuweiName,tuopanName
        );
        page.setRows(lihuoList);

        return page;
    }

    @Override
    public List<Lihuo> findALlLihuo() {
        return lihuoMapper.findAll();
    }
}
