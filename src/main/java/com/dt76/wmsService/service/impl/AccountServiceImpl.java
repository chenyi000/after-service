package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.AccountMapper;
import com.dt76.wmsService.mapper.TbFunctionsMapper;
import com.dt76.wmsService.pojo.SysEmployee;
import com.dt76.wmsService.pojo.TbFunctions;
import com.dt76.wmsService.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 12:32
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private TbFunctionsMapper tbFunctionsMapper;

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @Override
    public SysEmployee login(String username, String password) {
        return accountMapper.login(username, password);
    }

    @Override
    public SysEmployee findByUsername(String username) {
        return accountMapper.loginShiro(username);
    }
    @Override
    public List<SysEmployee> findAll() {

        return accountMapper.findAll();
    }

    @Override
    public List<TbFunctions> findFuncByUserId(Long userId) {
        return null;
    }




}
