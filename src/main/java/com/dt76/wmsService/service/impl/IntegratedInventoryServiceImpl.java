package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.IntegratedInventoryMapper;
import com.dt76.wmsService.pojo.IntegratedInventory;
import com.dt76.wmsService.service.IntegratedInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IntegratedInventoryServiceImpl implements IntegratedInventoryService {

    @Autowired
    private IntegratedInventoryMapper integratedInventoryMapper;

    @Override
    public List<IntegratedInventory> qryAll() {
        return integratedInventoryMapper.qryAll()   ;
    }
}
