package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.ShopMapper;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopMapper shopMapper;

    @Override
    public boolean addShop(WmsShop wmsShop) {
        shopMapper.insertShop(wmsShop);
        return true;
    }

    @Override
    public List<WmsShop> getAll() {
        return shopMapper.qryAll();
    }

    @Override
    public WmsShop getOne(int id) {
        return shopMapper.getOne(id);
    }

    @Override
    public boolean upd(WmsShop wmsShop) {
        shopMapper.upd(wmsShop);
        return true;
    }

    @Override
    public boolean updShop(WmsShop wmsShop) {
        shopMapper.updShop(wmsShop);
        return true;
    }

    @Override
    public int qryCount1() {
        return shopMapper.qryCount1();
    }

    @Override
    public int qryCount2() {
        return shopMapper.qryCount2();
    }

    @Override
    public int qryCount3() {
        return shopMapper.qryCount3();
    }

    @Override
    public int qryCount4() {
        return shopMapper.qryCount4();
    }

}
