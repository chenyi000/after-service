package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.ChukuHuidanMapper;
import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsHuiDan;
import com.dt76.wmsService.service.ChukuHuidanService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 *Created by 王超 on 2019/5/17
 */
@Service
public class ChukuHuidanServiceImpl implements ChukuHuidanService {
    @Autowired
    private ChukuHuidanMapper chukuHuidanMapper;
    @Override
    public void luru(WmsHuiDan wmsHuiDan) {
        chukuHuidanMapper.add(wmsHuiDan);
    }

    @Override
    public List<Plat> getAll() {
        return chukuHuidanMapper.huidan();
    }

    //查询所有已回单的商品2.0
    @Override
    public Page getSuoyou(int pageIndex, String jjr) {
        Page page = new Page();
        page.setPageIndex(pageIndex);
        //更新总条数
        page.setTotalCount(chukuHuidanMapper.qryCountHui(jjr));
        List<ChukuTongzhi> chukuTongzhiList = chukuHuidanMapper.getAll((page.getPageIndex() - 1) * page.getPageSize(),
                page.getPageSize(),jjr);
        page.setRows(chukuTongzhiList);
        return page;
    }


    //查询所有已回单的商品1.0
//    @Override
//    public List<ChukuTongzhi> getSuoyou() {
//        return chukuHuidanMapper.getAll();
//    }

    //查询所有已回单的条数2.0


    @Override
    public void modify(String chuDocId) {
        chukuHuidanMapper.udp(chuDocId);
    }


    //根据出库单号删除回单记录
    @Override
    public void delete(int docId) {
        chukuHuidanMapper.del(docId);
    }

}
