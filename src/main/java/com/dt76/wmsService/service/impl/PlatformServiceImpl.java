package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.PlatformMapper;
import com.dt76.wmsService.pojo.Platform;
import com.dt76.wmsService.service.PlatformService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PlatformServiceImpl implements PlatformService {

    @Autowired
    private PlatformMapper platformMapper;
    @Override
    public List<Platform> getPlatformAll() {
        return platformMapper.qryPlatformAll();
    }

    @Override
    public Page getAll(int pageIndex, String qryCode, String qryName) {
        Page page = new Page();
        page.setPageIndex(pageIndex); //每次跟新当前页
        //更新总条数
        page.setTotalCount(platformMapper.qryCount());
        List<Platform> plList =platformMapper.qryPageAll((page.getPageIndex()-1)*page.getPageSize(),page.getPageSize(),qryCode,qryName);
        page.setRows(plList);
        return page;
    }

    @Override
    public void addPlatform(Platform platform) {
        platformMapper.insert(platform);
    }

    @Override
    public void updPlatform(Platform platform) {
        platformMapper.update(platform);
    }

    @Override
    public Platform getPlatformById(Long id) {
        return platformMapper.getPlatformById(id);
    }

    @Override
    public void delPlatform(int id) {
        platformMapper.delete(id);
    }
}
