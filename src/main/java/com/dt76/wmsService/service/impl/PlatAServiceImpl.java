package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.PlatAMapper;
import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.PlatAService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlatAServiceImpl implements PlatAService {
    @Autowired
    private PlatAMapper platAMapper;
    @Override
    public Page getAll(int pageIndex, String doc, String plat) {
        Page page = new Page();
        page.setPageIndex(pageIndex);
        page.setTotalCount(platAMapper.qryCount(doc,plat));
        List<Plat> pList = platAMapper.qryPageAll((page.getPageIndex()-1)*page.getPageSize(),page.getPageSize(),doc,plat);
        page.setRows(pList);
        return page;
    }

    @Override
    public void updPlat(Plat plat) {
            platAMapper.update(plat);
    }

    @Override
    public void delPlat(int id) {
            platAMapper.delete(id);
    }

    @Override
    public void udpShopStuIdById(int spId, int statusId) {
        platAMapper.udpStuIdById(spId,statusId);
    }


    @Override
    public Plat getById(Long id) {
        return platAMapper.qryById(id);
    }

    @Override
    public WmsShop getShopById(int id) {
        return platAMapper.qryShopById(id);
    }

    @Override
    public void udpPlatInDate(int pId) {
        platAMapper.updateInDate(pId);
    }

    @Override
    public void udpPlatOutDate(int pId) {
        platAMapper.updateOutDate(pId);
    }
}
