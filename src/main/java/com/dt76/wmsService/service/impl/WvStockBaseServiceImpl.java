package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.WvStockBaseMapper;
import com.dt76.wmsService.pojo.WvStockBase;
import com.dt76.wmsService.service.WvStockBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service//证明是service层
public class WvStockBaseServiceImpl implements WvStockBaseService {

    //注入dao mapper
    @Autowired
private WvStockBaseMapper wvStockBaseMapper;
    @Override
    public List<WvStockBase> qryAll() {
        return wvStockBaseMapper.qryAll();
    }
}
