package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.DepartmentMapper;
import com.dt76.wmsService.pojo.SysDepartment;
import com.dt76.wmsService.service.DepartmentService;
import com.dt76.wmsService.utils.DepartmentPageUtil;
import com.dt76.wmsService.utils.DepartmentSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 14:03
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    /**
     * 按条件查询部门
     *
     * @param departmentSearchUtil
     * @return
     */
    @Override
    public List<DepartmentPageUtil> finDepartment(DepartmentSearchUtil departmentSearchUtil) {


        return departmentMapper.finDepartment(departmentSearchUtil);
    }

    /**
     * 增加
     *
     * @param sysEpartment
     * @return
     */
    @Override
    public Integer addDepartment(SysDepartment sysEpartment) {
        return departmentMapper.addDepartment(sysEpartment);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public Integer delDepartment(Long id) {
        return departmentMapper.delDepartment(id);
    }

    /**
     * 修改
     *
     * @param sysEpartment
     * @return
     */
    @Override
    public Integer updDepartment(SysDepartment sysEpartment) {
        return departmentMapper.updDepartment(sysEpartment);
    }
}
