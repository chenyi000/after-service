package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.DayCostMapper;
import com.dt76.wmsService.mapper.ZhangDanMapper;
import com.dt76.wmsService.service.ZhangDaoService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.ZhangDanPageUtil;
import com.dt76.wmsService.utils.ZhangDanSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZhangDanServiceImpl implements ZhangDaoService {

    @Autowired
    private ZhangDanMapper zhangDanMapper;


    @Override
    public List<ZhangDanPageUtil> findZd(ZhangDanSearchUtil zhangDanSearchUtil) {
        Integer count = zhangDanMapper.getCount(zhangDanSearchUtil);
        zhangDanSearchUtil.setPageSumCount(count);

        Integer pageNum = count % zhangDanSearchUtil.getPageSize() > 0
                ? count / zhangDanSearchUtil.getPageSize() + 1
                : count / zhangDanSearchUtil.getPageSize();

        zhangDanSearchUtil.setStart(zhangDanSearchUtil.getPageSize() * (zhangDanSearchUtil.getPageIndex() - 1));

        zhangDanSearchUtil.setPageNum(pageNum);
        List<ZhangDanPageUtil> dayCostPage = zhangDanMapper.findZd(zhangDanSearchUtil);

        for (int i = 0; i < dayCostPage.size(); i++) {
            dayCostPage.get(i).setPageIndex(zhangDanSearchUtil.getPageIndex());
            dayCostPage.get(i).setPageNum(zhangDanSearchUtil.getPageNum());
            dayCostPage.get(i).setPageSize(zhangDanSearchUtil.getPageSize());
            dayCostPage.get(i).setPageSumCount(zhangDanSearchUtil.getPageSumCount());
        }
        return dayCostPage;
    }

    @Override
    public List<DayCostPageUtil> exportBill(ZhangDanSearchUtil zhangDanSearchUtil) {
        return zhangDanMapper.exportBill(zhangDanSearchUtil);
    }
}