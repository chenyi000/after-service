package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.BinMapper;
import com.dt76.wmsService.pojo.Bin;
import com.dt76.wmsService.service.BinService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BinSerivceImpl implements BinService {
    @Autowired
    private BinMapper binMapper;
   /* @Override
    public Page getAllBin(int pageIndex, String storeCode, int storeId, int pId) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(binMapper.getConditionCount(storeCode,storeId,pId));
        List<Bin> binList = binMapper.findConditionPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),storeCode,storeId,pId
        );
        System.out.println(binList);
        page.setRows(binList);

        return page;
    }*/

    @Override
    public List<Bin> findAllBin() {
        return binMapper.findAll();
    }

    @Override
    public Page getAllBin2(int pageIndex, String storeName, String chuweiName, String tuopanName) {
        Page page = new Page();
        page.setPageIndex(pageIndex);//每次更新当前页
        //更新总条数
        page.setTotalCount(binMapper.getCount(storeName,chuweiName,tuopanName));
        List<Bin> bin2List = binMapper.findPageAll((page.getPageIndex()-1)*page.getPageSize(),
                page.getPageSize(),storeName,chuweiName,tuopanName
        );
        System.out.println(bin2List);
        page.setRows(bin2List);

        return page;
    }
}
