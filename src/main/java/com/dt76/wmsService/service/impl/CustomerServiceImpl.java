package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.CustomerMapper;
import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.service.CustomerService;
import com.dt76.wmsService.utils.CustomerInfoUtil;
import com.dt76.wmsService.utils.CustomerPageUtil;
import com.dt76.wmsService.utils.CustomerSearcherUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 11:25
 */

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerMapper customerMapper;

    /**
     * 动态查询分页
     *
     * @param searcherUtil
     * @return
     */
    @Override
    public List<CustomerPageUtil> findCustomer(CustomerSearcherUtil searcherUtil) {
        Integer pageSumCount= customerMapper.getCustomerCount(searcherUtil);
        searcherUtil.setPageSumCount(pageSumCount);
        Integer pageNum=pageSumCount%searcherUtil.getPageSize() > 0
                ? pageSumCount/searcherUtil.getPageSize() +1
                : pageSumCount/searcherUtil.getPageSize();
        searcherUtil.setPageNum(pageNum);
        searcherUtil.setStart(searcherUtil.getPageSize()*(searcherUtil.getPageIndex()-1));

        List<CustomerPageUtil> customers = customerMapper.findCustomer(searcherUtil);

        for(int i =0; i< customers.size(); i++){
            CustomerPageUtil customerPageUtil = customers.get(i);
            if(customerPageUtil.getCCustomerType() == 1){
                customerPageUtil.setCustomerType("个人用户");
            }else{
                customerPageUtil.setCustomerType("企业用户");
            }

            if(customerPageUtil.getStatus() ==1){
                customerPageUtil.setStatusName("合作中");
            }else{
                customerPageUtil.setStatusName("终止合作");
            }

            customerPageUtil.setPageIndex(searcherUtil.getPageIndex());
            customerPageUtil.setPageNum(searcherUtil.getPageNum());
            customerPageUtil.setPageSumCount(searcherUtil.getPageSumCount());
            customerPageUtil.setPageSize(searcherUtil.getPageSize());
        }
        return customers;
    }

    /**
     * 增加用户
     *
     * @param sysCustomer
     * @return
     */
    @Override
    public Integer addCustomer(SysCustomer sysCustomer) {
        return customerMapper.addCustomer(sysCustomer);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
 /*   @Override
    public Integer delCustomerById(Long id) {
        return customerMapper.delCustomerById(id);
    }*/

    /**
     * 修改
     *
     * @param sysCustomer
     * @return
     */
    @Override
    public Integer updCustomer(SysCustomer sysCustomer) {
        return customerMapper.updCustomer(sysCustomer);
    }

    /**
     * 单个查找
     *
     * @param id
     * @return
     */
  /*  @Override
    public CustomerInfoUtil findCustomerById(Long id) {
        CustomerInfoUtil customerById = customerMapper.findCustomerById(id);
        if(customerById.getCCustomerType() == 1){
            customerById.setCustomerType("个人用户");
        }else{
            customerById.setCustomerType("企业用户");
        }

        if(customerById.getStatus() ==1){
            customerById.setStatusName("合作中");
        }else{
            customerById.setStatusName("终止合作");
        }
        return customerById;
    }*/

    /**
     * 修改合作状态
     *
     * @param status
     * @param id
     * @return
     */
   /* @Override
    public Integer updCustomerStatusById(Integer status, Long id) {
        return customerMapper.updCustomerStatusById(status,id);
    }*/

    @Override
    public Integer delCustomerById(Long id) {
        return null;
    }

    @Override
    public CustomerInfoUtil findCustomerById(Long id) {
        return null;
    }

    @Override
    public Integer updCustomerStatusById(Integer status, Long id) {
        return null;
    }
}
