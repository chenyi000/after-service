package com.dt76.wmsService.service.impl;


import com.dt76.wmsService.mapper.ChukuXiajiaMapper;
import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.service.ChuKuXiajiaService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/*
 *Created by 王超 on 2019/5/14
 */
@Service
public class ChuKuXiajiaServiceImpl implements ChuKuXiajiaService {
  @Autowired
  private ChukuXiajiaMapper chukuXiajiaMapper;

  @Override
  public Page getAllXiajia(int pageIndex,String jjr) {
    Page page = new Page();
    page.setPageIndex(pageIndex);
    //更新总条数
    page.setTotalCount(chukuXiajiaMapper.qryCount(jjr));
    List<ChukuTongzhi> chukuTongzhiList = chukuXiajiaMapper.getAllXiaJia((page.getPageIndex()-1)*page.getPageSize(),page.getPageSize(),jjr);
    page.setRows(chukuTongzhiList);
    return page;
  }




  @Override
    public void gaiXia(int id) {
        chukuXiajiaMapper.udp(id);
    }
}
