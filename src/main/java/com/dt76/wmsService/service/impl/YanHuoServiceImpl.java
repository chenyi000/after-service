package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.YanHuoMapper;
import com.dt76.wmsService.pojo.Yanhuo;
import com.dt76.wmsService.service.YanHuoService;
import com.dt76.wmsService.utils.OrderPageUtil;
import com.dt76.wmsService.utils.YanHuoPageUtil;
import com.dt76.wmsService.utils.YanHuoSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: u3
 * @description: 验货
 * @author: wx
 * @create: 2019-05-12 21:31
 */
@Service
public class YanHuoServiceImpl implements YanHuoService{

    @Autowired
    private YanHuoMapper yanHuoMapper;


    //查询所有验货单
    @Override
    public List<YanHuoPageUtil> findAll(YanHuoSearchUtil yanHuoSearchUtil) {

        Integer count = yanHuoMapper.getCount(yanHuoSearchUtil);
        yanHuoSearchUtil.setPageSumCount(count);

        Integer pageNum=count%yanHuoSearchUtil.getPageSize() > 0
                ? count/yanHuoSearchUtil.getPageSize()+1
                : count/yanHuoSearchUtil.getPageSize();

        yanHuoSearchUtil.setStart(yanHuoSearchUtil.getPageSize()*(yanHuoSearchUtil.getPageIndex()-1));

        yanHuoSearchUtil.setPageNum(pageNum);
        List<YanHuoPageUtil> employeePage = yanHuoMapper.qryAll(yanHuoSearchUtil);

        for(int i =0; i< employeePage.size(); i++){
            employeePage.get(i).setPageIndex(yanHuoSearchUtil.getPageIndex());
            employeePage.get(i).setPageNum(yanHuoSearchUtil.getPageNum());
            employeePage.get(i).setPageSize(yanHuoSearchUtil.getPageSize());
            employeePage.get(i).setPageSumCount(yanHuoSearchUtil.getPageSumCount());
        }
        return employeePage;
    }

    @Override
    public List<YanHuoPageUtil> findAll2(YanHuoSearchUtil yanHuoSearchUtil) {
        Integer count = yanHuoMapper.getCount2(yanHuoSearchUtil);
        yanHuoSearchUtil.setPageSumCount(count);

        Integer pageNum=count%yanHuoSearchUtil.getPageSize() > 0
                ? count/yanHuoSearchUtil.getPageSize()+1
                : count/yanHuoSearchUtil.getPageSize();

        yanHuoSearchUtil.setStart(yanHuoSearchUtil.getPageSize()*(yanHuoSearchUtil.getPageIndex()-1));

        yanHuoSearchUtil.setPageNum(pageNum);
        List<YanHuoPageUtil> employeePage = yanHuoMapper.qryAll2(yanHuoSearchUtil);

        for(int i =0; i< employeePage.size(); i++){
            employeePage.get(i).setPageIndex(yanHuoSearchUtil.getPageIndex());
            employeePage.get(i).setPageNum(yanHuoSearchUtil.getPageNum());
            employeePage.get(i).setPageSize(yanHuoSearchUtil.getPageSize());
            employeePage.get(i).setPageSumCount(yanHuoSearchUtil.getPageSumCount());
        }
        return employeePage;
    }

    @Override
    public List<YanHuoPageUtil> findAll3(YanHuoSearchUtil yanHuoSearchUtil) {
        Integer count = yanHuoMapper.getCount2(yanHuoSearchUtil);
        yanHuoSearchUtil.setPageSumCount(count);

        Integer pageNum=count%yanHuoSearchUtil.getPageSize() > 0
                ? count/yanHuoSearchUtil.getPageSize()+1
                : count/yanHuoSearchUtil.getPageSize();

        yanHuoSearchUtil.setStart(yanHuoSearchUtil.getPageSize()*(yanHuoSearchUtil.getPageIndex()-1));

        yanHuoSearchUtil.setPageNum(pageNum);
        List<YanHuoPageUtil> employeePage = yanHuoMapper.qryAll3(yanHuoSearchUtil);

        for(int i =0; i< employeePage.size(); i++){
            employeePage.get(i).setPageIndex(yanHuoSearchUtil.getPageIndex());
            employeePage.get(i).setPageNum(yanHuoSearchUtil.getPageNum());
            employeePage.get(i).setPageSize(yanHuoSearchUtil.getPageSize());
            employeePage.get(i).setPageSumCount(yanHuoSearchUtil.getPageSumCount());
        }
        return employeePage;
    }

    @Override
    public YanHuoPageUtil getOne(int id) {
        return yanHuoMapper.getOne(id);
    }

    @Override
    public boolean add(int spId) {
        yanHuoMapper.insert(spId);
        return true;
    }

    @Override
    public boolean upd(int spId) {
        yanHuoMapper.upd(spId);
        return true;
    }

}
