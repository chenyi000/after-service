package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.QualityMapper;
import com.dt76.wmsService.pojo.Quality;
import com.dt76.wmsService.service.QualityService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public class QualityServiceImpl implements QualityService {

    @Autowired
    private QualityMapper qualityMapper;

    @Override
    public List<Quality> getAllQuality(){
        return qualityMapper.qryAll();

    }

    @Override
    public boolean addQuality(Quality quality) {
        qualityMapper.insert(quality);
        return true;
    }

    @Override
    public boolean updateQuality(Quality quality) {
        qualityMapper.update(quality);
        return false;
    }

    @Override
    public Quality toqryQuality(int ID) {
        Quality quality = qualityMapper.toqryAll(ID);
       return quality;

    }



    @Override
    public int delQuality(int ID) {
        return qualityMapper.delet(ID);
    }
}
