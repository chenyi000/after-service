package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.ChuFuheMapper;
import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.service.ChuFuheService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 *Created by 王超 on 2019/5/17
 */
@Service
public class ChuFuheServiceImpl implements ChuFuheService {
    @Autowired
    private ChuFuheMapper chuFuheMapper;

    @Override
    public Page getAllFuhe(int pageIndex, String ddh) {
        Page page = new Page();
        page.setPageIndex(pageIndex);
        //更新总条数
        page.setTotalCount(chuFuheMapper.qryCount(ddh));
        List<ChukuTongzhi> chukuTongzhiList =chuFuheMapper.getAllFuhe((page.getPageIndex()-1)*page.getPageSize(),page.getPageSize(),ddh);
        page.setRows(chukuTongzhiList);
        return page;
    }

    @Override
    public void modify(int spId) {
        chuFuheMapper.udp(spId);
    }
}

