package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.DayCostMapper;
import com.dt76.wmsService.mapper.QianFeiMapper;
import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.service.DayCostService;
import com.dt76.wmsService.service.QianFeiService;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QianFeiServiceImpl implements QianFeiService {

    @Autowired
    private QianFeiMapper qianFeiMapper;

   /* @Override
    public List<WmsDayCost> getAllDayCost() {
        List<WmsDayCost> wmsDayCostList = null;
        wmsDayCostList = dayCostMapper.qryAllDayCost();
        return wmsDayCostList;
    }*/


    @Override
    public List<DayCostPageUtil> findQianFei(DayCostSearchUtil dayCostSearchUtil) {
        Integer count = qianFeiMapper.getCount(dayCostSearchUtil);
        dayCostSearchUtil.setPageSumCount(count);

        Integer pageNum = count % dayCostSearchUtil.getPageSize() > 0
                ? count / dayCostSearchUtil.getPageSize() + 1
                : count / dayCostSearchUtil.getPageSize();

        dayCostSearchUtil.setStart(dayCostSearchUtil.getPageSize() * (dayCostSearchUtil.getPageIndex() - 1));

        dayCostSearchUtil.setPageNum(pageNum);
        List<DayCostPageUtil> dayCostPage = qianFeiMapper.findQianFei(dayCostSearchUtil);

        for (int i = 0; i < dayCostPage.size(); i++) {
            dayCostPage.get(i).setPageIndex(dayCostSearchUtil.getPageIndex());
            dayCostPage.get(i).setPageNum(dayCostSearchUtil.getPageNum());
            dayCostPage.get(i).setPageSize(dayCostSearchUtil.getPageSize());
            dayCostPage.get(i).setPageSumCount(dayCostSearchUtil.getPageSumCount());
        }
        return dayCostPage;
    }

    @Override
    public DayCostPageUtil findById(int id) {
        return qianFeiMapper.findById(id);
    }
}
