package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.CheliangMapper;
import com.dt76.wmsService.pojo.Cheliang;
import com.dt76.wmsService.service.CheliangService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CheliangServiceImpl implements CheliangService {

    @Autowired
    private CheliangMapper cheliangMapper;

    @Override
    public Page getAll(int pageIndex, String cheNo, String sj, String stu) {
       Page page = new Page();
       page.setPageIndex(pageIndex);
       page.setTotalCount(cheliangMapper.qryCount(cheNo,sj,stu));
       List<Cheliang> clList = cheliangMapper.qryPageAll((page.getPageIndex()-1)*page.getPageSize(),page.getPageSize(),cheNo,sj,stu);
       page.setRows(clList);
        return page;
    }

    @Override
    public void addChe(Cheliang cheliang) {
        cheliangMapper.insert(cheliang);
    }

    @Override
    public void updChe(Cheliang cheliang) {
        cheliangMapper.update(cheliang);
    }

    @Override
    public void delChe(int id) {
        cheliangMapper.delete(id);
    }
}
