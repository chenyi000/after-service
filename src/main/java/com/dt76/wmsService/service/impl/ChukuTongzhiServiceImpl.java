package com.dt76.wmsService.service.impl;


import com.dt76.wmsService.mapper.ChukuTongzhiMapper;
import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.service.ChukuTongzhiService;
import com.dt76.wmsService.service.MailService;
import com.dt76.wmsService.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/*
 *Created by 王超 on 2019/5/10
 */
@Service
public class ChukuTongzhiServiceImpl implements ChukuTongzhiService {

    @Autowired
    private ChukuTongzhiMapper chukuTongzhiMapper;

    @Override
    public Page getAllChuku(int pageIndex, String sjr,String ddh) {
        Page page = new Page();
        page.setPageIndex(pageIndex);
        //更新总条数
        page.setTotalCount(chukuTongzhiMapper.qryCount(sjr,ddh));
        List<ChukuTongzhi> chukuTongzhiList = chukuTongzhiMapper.getAllChuKu((page.getPageIndex() - 1) * page.getPageSize(),
                page.getPageSize(),sjr,ddh);
        page.setRows(chukuTongzhiList);
        return page;
    }


    //同时执行2种操作
    @Override
    public void xiuAdd(int id) {
        chukuTongzhiMapper.xiu(id);
        chukuTongzhiMapper.udp(id);
    }


    @Override
    public ChukuTongzhi get(int spId) {
        return chukuTongzhiMapper.findOne(spId);
    }
}
