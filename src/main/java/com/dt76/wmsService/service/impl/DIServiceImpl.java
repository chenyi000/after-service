package com.dt76.wmsService.service.impl;


import com.dt76.wmsService.mapper.DIMapper;
import com.dt76.wmsService.pojo.DifferencesInventory;
import com.dt76.wmsService.service.DIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DIServiceImpl implements DIService {

    @Autowired
    private DIMapper diMapper;

    @Override
    public List<DifferencesInventory> qryAll(){
        return diMapper.qryAll();
    }
}
