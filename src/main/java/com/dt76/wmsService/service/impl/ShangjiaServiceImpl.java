package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.ShangjiaMapper;
import com.dt76.wmsService.service.ShangjiaService;
import com.dt76.wmsService.utils.ShangjiaPageUtil;
import com.dt76.wmsService.utils.ShangjiaSearchUtil;
import com.dt76.wmsService.utils.ShangjiaSearchUtil2;
import com.dt76.wmsService.utils.kuCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-15 15:50
 */
@Service
public class ShangjiaServiceImpl implements ShangjiaService{

    @Autowired
    private ShangjiaMapper shangjiaMapper;


    @Override
    public boolean add(ShangjiaSearchUtil shangjiaSearchUtil) {
        shangjiaMapper.insert(shangjiaSearchUtil);
        return true;
    }

    @Override
    public List<ShangjiaPageUtil> findAll(ShangjiaSearchUtil2 shangjiaSearchUtil2) {
        Integer count = shangjiaMapper.getCount(shangjiaSearchUtil2);
        shangjiaSearchUtil2.setPageSumCount(count);

        Integer pageNum=count%shangjiaSearchUtil2.getPageSize() > 0
                ? count/shangjiaSearchUtil2.getPageSize()+1
                : count/shangjiaSearchUtil2.getPageSize();

        shangjiaSearchUtil2.setStart(shangjiaSearchUtil2.getPageSize()*(shangjiaSearchUtil2.getPageIndex()-1));

        shangjiaSearchUtil2.setPageNum(pageNum);
        List<ShangjiaPageUtil> employeePage = shangjiaMapper.qryAll(shangjiaSearchUtil2);

        for(int i =0; i< employeePage.size(); i++){
            employeePage.get(i).setPageIndex(shangjiaSearchUtil2.getPageIndex());
            employeePage.get(i).setPageNum(shangjiaSearchUtil2.getPageNum());
            employeePage.get(i).setPageSize(shangjiaSearchUtil2.getPageSize());
            employeePage.get(i).setPageSumCount(shangjiaSearchUtil2.getPageSumCount());
        }
        return employeePage;
    }

    @Override
    public List<kuCount> getCount(int id) {
        return shangjiaMapper.getCount2(id);
    }
}
