package com.dt76.wmsService.service;

import com.dt76.wmsService.utils.Page;
import org.apache.ibatis.annotations.Param;

/*
 *Created by 王超 on 2019/5/14
 */
public interface ChuKuXiajiaService {

    //分页查询+模糊查询所有待下架的商品
    Page getAllXiajia(int pageIndex, @Param("jjr") String jjr);


    //点击下架按钮修改商品状态
    void gaiXia(int id);
}
