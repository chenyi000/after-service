package com.dt76.wmsService.service;

/**
 * @Description
 * @auther jun
 * @create 2019-05-16 10:39
 */
public interface MailService {

    void sendEmail();
}
