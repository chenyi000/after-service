package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.utils.CustomerInfoUtil;
import com.dt76.wmsService.utils.CustomerPageUtil;
import com.dt76.wmsService.utils.CustomerSearcherUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 11:23
 */
public interface CustomerService {

    /**
     * 动态查询分页
     * @param searcherUtil
     * @return
     */
    List<CustomerPageUtil> findCustomer(CustomerSearcherUtil searcherUtil);

    /**
     * 增加用户
     * @param sysCustomer
     * @return
     */
    Integer addCustomer(SysCustomer sysCustomer);

    /**
     * 删除
     * @param id
     * @return
     */
    Integer delCustomerById(Long id);


    /**
     * 修改
     * @param sysCustomer
     * @return
     */
    Integer updCustomer(SysCustomer sysCustomer);

    /**
     * 单个查找
     * @param id
     * @return
     */
    CustomerInfoUtil findCustomerById(Long id);

    /**
     * 修改合作状态
     * @param id
     * @return
     */
    Integer updCustomerStatusById(Integer status,Long id);


}
