package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.Bin;
import com.dt76.wmsService.utils.Page;

import java.util.List;

public interface BinService {
   /* Page getAllBin(int pageIndex, String storeCode, int storeId,int pId);*/
    List<Bin> findAllBin();
    Page getAllBin2(int pageIndex, String storeName, String chuweiName,String tuopanName);
}
