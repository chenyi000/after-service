package com.dt76.wmsService.service;

import com.dt76.wmsService.utils.ShangjiaPageUtil;
import com.dt76.wmsService.utils.ShangjiaSearchUtil;
import com.dt76.wmsService.utils.ShangjiaSearchUtil2;
import com.dt76.wmsService.utils.kuCount;

import java.util.List;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-15 15:50
 */
public interface ShangjiaService {

    boolean add(ShangjiaSearchUtil shangjiaSearchUtil);

    List<ShangjiaPageUtil> findAll(ShangjiaSearchUtil2 shangjiaSearchUtil2);

    List<kuCount> getCount(int id);
}
