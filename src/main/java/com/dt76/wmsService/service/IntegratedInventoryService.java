package com.dt76.wmsService.service;

import com.dt76.wmsService.pojo.IntegratedInventory;

import java.util.List;


public interface IntegratedInventoryService {

    List<IntegratedInventory> qryAll();
}
