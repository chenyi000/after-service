package com.dt76.wmsService;

import com.github.pagehelper.PageHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.util.Properties;

@SpringBootApplication
//启用服务的被注册中心发现--自己主动注册到注册中心去
@EnableDiscoveryClient
@MapperScan(basePackages = "com.dt76.wmsService.mapper")
public class BackService {

    public static void main(String[] args) {
        SpringApplication.run(BackService.class, args);
    }

}
