package com.dt76.wmsService.pojo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class SysDepartment implements Serializable {

  private long eId;
  private String eCode;
  private String eName;
  private long eParentId;
  private String eCreatename;
  private Date eCreateTime;
  private String eUpdatename;
  private Date eUpadateTime;

}
