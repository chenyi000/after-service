package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Data
@Component
/**
 * 商品表
 */
public class WmsShop implements Serializable {
    private int id;
    private String createName;
    private Date createDate;
    private String updateName;
    private Date updateDate;
    private String shpName;//商品名称
    private String shpType;//商品类型
    private String shpCode;//商品编码
    private String shpNum;//商品数量
    private String shpWeight;//商品重量
    private String shpTiji;//商品体积
    private String inType;//存放类型
    private String proDate;//生产日期
    private String baozhiqi;//保质期
    private String unit;//商品单位
    private int inDay;//存放时间
    private String orderCode;//订单编号
    private int statusId;//业务状态
    private String chuhuoTime;//出货时间

}
