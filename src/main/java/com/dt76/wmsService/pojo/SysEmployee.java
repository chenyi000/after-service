package com.dt76.wmsService.pojo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysEmployee implements Serializable {

  private long eId;
  private String eCode;
  private String eLoadname;
  private String ePassword;
  private String eRealName;
  private String eTelphone;
  private String eEmial;
  private String eAddress;
  private long eEmpTypeId;
  private long eDepartmentId;
  private Integer eStatus;
  private String eCreateName;
  private Date eCreateTime;
  private String eUpdateName;
  private Date eUpdateTime;

  //关联表
  private SysEmployeeType sysEmployeeType;
  private SysDepartment sysEpartment;

}
