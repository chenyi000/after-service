package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class KuCunA implements Serializable {
    private int cId;
    private String chuweiName;//储位名称
    private String goodsName;//商品名称

    private BShop bShop;
    private Tuopan tuopan;
    private Chuwei chuwei;
    private WmsShop wmsShop;
}
