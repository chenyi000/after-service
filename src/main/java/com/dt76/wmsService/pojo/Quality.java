package com.dt76.wmsService.pojo;

import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class Quality implements Serializable {
     private int id;         //品检状态编码
     private String inName;  //品检状态名称

    public Quality() {
    }

    public Quality(int id, String inName) {
        this.id = id;
        this.inName = inName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInName() {
        return inName;
    }

    public void setInName(String inName) {
        this.inName = inName;
    }
}
