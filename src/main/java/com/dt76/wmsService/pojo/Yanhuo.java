package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 验货表
 */
@Component
@Data
public class Yanhuo implements Serializable {
    private String daoId;
 private WmsOrder wmsOrder;
 private WmsShop wmsShop;
 private Plat plat;
 private Platform platform;

}
