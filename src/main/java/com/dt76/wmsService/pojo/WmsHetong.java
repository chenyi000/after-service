package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class WmsHetong {

    private int hetongId;
    private double dingjin;
    private double yue;
    private int costId;
 

}
