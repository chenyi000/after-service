package com.dt76.wmsService.pojo;

import java.io.Serializable;

public class Platform implements Serializable {

    private long pfId;
    private String platformCode;
    private String platformName;



    public long getPfId() {
        return pfId;
    }

    public void setPfId(long pfId) {
        this.pfId = pfId;
    }

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }
}
