package com.dt76.wmsService.pojo;

import java.util.Date;

/*
   退货查询历史用
 */
public class Wms_his_stock_ku {
    private String id;
    private String cus_code; //客户订单号
    private String createName;//
    private Date createDate; //创建时间
    private String orderno;  //商品编码
    private Date predict_time; //预计到达时间
    private String carno; //车号
    private String driver_name; //司机姓名
    private String driver_phone; //司机电话
    private String platform; //月台
    private String remark ;// 备注
    private String status; //状态
    private String shop_name;//商品名
    private String shop_num;//商品数量
    private String shop_weight;//商品单位
    private String order_type; //订单类型

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public Wms_his_stock_ku(String id, String cus_code, String createName, Date createDate, String orderno, Date predict_time, String carno, String driver_name, String driver_phone, String platform, String remark, String status, String shop_name, String shop_num, String shop_weight, String order_type) {
        this.id = id;
        this.cus_code = cus_code;
        this.createName = createName;
        this.createDate = createDate;
        this.orderno = orderno;
        this.predict_time = predict_time;
        this.carno = carno;
        this.driver_name = driver_name;
        this.driver_phone = driver_phone;
        this.platform = platform;
        this.remark = remark;
        this.status = status;
        this.shop_name = shop_name;
        this.shop_num = shop_num;
        this.shop_weight = shop_weight;
        this.order_type = order_type;
    }

    public Wms_his_stock_ku() {
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_num() {
        return shop_num;
    }

    public void setShop_num(String shop_num) {
        this.shop_num = shop_num;
    }

    public String getShop_weight() {
        return shop_weight;
    }

    public void setShop_weight(String shop_weight) {
        this.shop_weight = shop_weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCus_code() {
        return cus_code;
    }

    public void setCus_code(String cus_code) {
        this.cus_code = cus_code;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public Date getPredict_time() {
        return predict_time;
    }

    public void setPredict_time(Date predict_time) {
        this.predict_time = predict_time;
    }

    public String getCarno() {
        return carno;
    }

    public void setCarno(String carno) {
        this.carno = carno;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
