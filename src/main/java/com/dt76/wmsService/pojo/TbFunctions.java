package com.dt76.wmsService.pojo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TbFunctions implements Serializable {

  private long funcId;
  private String funcName;
  private String funcUrl;
  private String funcCode;
  private long parentId;
  private long funcType;
  private long status;
  private long sortNum;
  private Date createTime;
  private Date updateTime;

}
