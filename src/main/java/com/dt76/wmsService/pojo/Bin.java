package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class Bin implements Serializable {
    private int bId;
    private String storeCode;
    private String storeName;
    private String storeText;

    private String ccName;
    private String cMianji;
    private String Cixu;
    private String zhongliang;
    private String zuidatuopan;
    private String tingyong;
    private String beizhu;
    private BaStore baStore;
    private Wendu wendu;
    private Tuopan tuopan;

}
