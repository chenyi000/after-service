package com.dt76.wmsService.pojo;


public class WmsChargeMode {

    private long chargeModeId;//计费方式编号啊
    private String chargeUnit;//计费方式
    private double unitPrice;//价格
    private long shopId;//商品编号

    public long getChargeModeId() {
        return chargeModeId;
    }

    public void setChargeModeId(long chargeModeId) {
        this.chargeModeId = chargeModeId;
    }

    public String getChargeUnit() {
        return chargeUnit;
    }

    public void setChargeUnit(String chargeUnit) {
        this.chargeUnit = chargeUnit;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }
}
