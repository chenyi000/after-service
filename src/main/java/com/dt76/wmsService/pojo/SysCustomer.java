package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysCustomer implements Serializable {

  private long cId;
  private String cCode;
  private String cUsername;
  private String cPassword;
  private String cNickname;
  private String cRealname;
  private long cCompanyId;
  private String cTelphone;
  private String cAddress;
  private Long cCustomerType;
  private Long status;
  private java.util.Date cCreateTime;
  private String cCreatName;
  private java.util.Date cUpdateTime;
  private String cUpdateName;

  //公司
  private SysCompany sysCompany;

}
