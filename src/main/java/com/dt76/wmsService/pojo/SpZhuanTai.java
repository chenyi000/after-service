package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

/*
 *Created by 王超 on 2019/5/18
 */
@Component
@Data
public class SpZhuanTai {
    private  int id;//主键
    private String zhuangtainame;//状态

}
