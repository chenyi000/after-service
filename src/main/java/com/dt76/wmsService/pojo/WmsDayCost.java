package com.dt76.wmsService.pojo;


import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class WmsDayCost implements Serializable {

    private int id;
    private long cusId;
    private String dayCostYj;
    private String costJs;
    private long shopId;


}
