package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Data
@Component
/**
 * 订单表
 */
public class WmsOrder implements Serializable {
    private int id;
    private String createName;
    private String createDate;
    private String updateName;
    private Date updateDate;
    private String cusCode;//订单编号
    private String cusName;//客户姓名
    private String masName;//收件人姓名
    private String cusAddress;//客户地址
    private String masAddress;//收件人地址
    private String cusPhone;//客户电话
    private String masPhone;//收件人电话
    private int shopId;//商品id
    private String typeId;//订单类型

    private WmsShop wmsShop;

}
