package com.dt76.wmsService.pojo;


import java.io.Serializable;

/*
 *Created by 王超 on 2019/5/10
 */
public class ChukuTongzhi implements Serializable {
 private WmsOrder wmsOrder;
 private WmsShop wmsShop;
 private Plat plat;
 private Platform platform;
 private ChukuBoci chukuBoci;
private WmsHuiDan wmsHuiDan;
private SpZhuanTai spZhuanTai;

    public WmsOrder getWmsOrder() {
        return wmsOrder;
    }

    public void setWmsOrder(WmsOrder wmsOrder) {
        this.wmsOrder = wmsOrder;
    }

    public WmsShop getWmsShop() {
        return wmsShop;
    }

    public void setWmsShop(WmsShop wmsShop) {
        this.wmsShop = wmsShop;
    }

    public Plat getPlat() { return plat; }

    public void setPlat(Plat plat) { this.plat = plat; }

    public Platform getPlatform() { return platform; }

    public void setPlatform(Platform platform) { this.platform = platform; }

    public ChukuBoci getChukuBoci() { return chukuBoci; }

    public void setChukuBoci(ChukuBoci chukuBoci) { this.chukuBoci = chukuBoci; }

    public WmsHuiDan getWmsHuiDan() {
        return wmsHuiDan;
    }

    public void setWmsHuiDan(WmsHuiDan wmsHuiDan) {
        this.wmsHuiDan = wmsHuiDan;
    }

    public SpZhuanTai getSpZhuanTai() {
        return spZhuanTai;
    }

    public void setSpZhuanTai(SpZhuanTai spZhuanTai) {
        this.spZhuanTai = spZhuanTai;
    }
}
