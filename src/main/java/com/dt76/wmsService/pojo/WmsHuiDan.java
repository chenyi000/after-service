package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

/*
 *Created by 王超 on 2019/5/17
 */
@Data
@Component
public class WmsHuiDan {
    private int id;//主键ID
    private String docId;//出库单号
    private String huidantime;//回单时间
}
