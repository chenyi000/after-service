package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class Chuwei implements Serializable {
    private int cId;
    private String createName;//创建人名称
    private String updateName;//修改人名称
    private String storeName;//仓库名称
    private String pName;//库位种类
    private String ccName;//储位名称
    private String cMianji;//最大面积、
    private String cCixu;//上架次序；
    private String chuweiName;//储位名称

    private BShop bShop;
    private Wendu wendu;
}
