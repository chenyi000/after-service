package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysCompany implements Serializable {

  private long pId;
  private String pCode;
  private String pName;
  private String pPerson;
  private String pContacts;
  private String pTelphone;
  private String pEmail;
  private String pAddress;
  private String pCreatename;
  private Date pCreateTime;
  private String pUpdatename;
  private Date pUdateTIme;

}
