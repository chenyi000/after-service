package com.dt76.wmsService.pojo;

//历史库存查询 视图
public class RpWmHisStockKu {
    private String id;//主键
    private String hisDate;//结余日期
    private String cusCode;//货主
    private String zhongWenQch;//中文全称
    private String kuWeiBianMa;//储位
    private String binId;//托盘
    private String goodsId;//商品
    private String shpMingCheng;//商品名称
    private String count;//数量
    private String baseUnit;//单位
    private int zhlKg;//净重
    private String chlShl;//拆零数量

    public RpWmHisStockKu() {
    }
    public RpWmHisStockKu(String id, String hisDate, String cusCode, String zhongWenQch, String kuWeiBianMa, String binId, String goodsId, String shpMingCheng, String count, String baseUnit, int zhlKg, String chlShl) {
        this.id = id;
        this.hisDate = hisDate;
        this.cusCode = cusCode;
        this.zhongWenQch = zhongWenQch;
        this.kuWeiBianMa = kuWeiBianMa;
        this.binId = binId;
        this.goodsId = goodsId;
        this.shpMingCheng = shpMingCheng;
        this.count = count;
        this.baseUnit = baseUnit;
        this.zhlKg = zhlKg;
        this.chlShl = chlShl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHisDate() {
        return hisDate;
    }

    public void setHisDate(String hisDate) {
        this.hisDate = hisDate;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getZhongWenQch() {
        return zhongWenQch;
    }

    public void setZhongWenQch(String zhongWenQch) {
        this.zhongWenQch = zhongWenQch;
    }

    public String getKuWeiBianMa() {
        return kuWeiBianMa;
    }

    public void setKuWeiBianMa(String kuWeiBianMa) {
        this.kuWeiBianMa = kuWeiBianMa;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getShpMingCheng() {
        return shpMingCheng;
    }

    public void setShpMingCheng(String shpMingCheng) {
        this.shpMingCheng = shpMingCheng;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public int getZhlKg() {
        return zhlKg;
    }

    public void setZhlKg(int zhlKg) {
        this.zhlKg = zhlKg;
    }

    public String getChlShl() {
        return chlShl;
    }

    public void setChlShl(String chlShl) {
        this.chlShl = chlShl;
    }
}
