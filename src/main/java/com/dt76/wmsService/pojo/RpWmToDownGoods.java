package com.dt76.wmsService.pojo;

import java.util.Date;

public class RpWmToDownGoods {

    private String id;
    private String createName;//创建人名称
    private String createBy;//创建人登录名称
    private Date createDate;//创建日期
    private String updateName;//更新人名称
    private String updateBy;//更新人登录名称
    private Date updateDate;//更新日期
    private String orderId;//原始单据编码
    private String kuWeiBianMa;//库位编码
    private String binId;//托盘码
    private String binIdTo;//托盘
    private String cusCode;//货主
    private String zhongWenQch;//中文全称
    private String goodsCode;//商品统一码
    private String goodsName;//商品名称
    private String goodsProData;//生产日期
    private String bzhiQi;//保质期
    private String baseUnit;//基本单位
    private String baseGoodscount;//基本单位数量
    private String zhlKg;//净重

}
