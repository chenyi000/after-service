package com.dt76.wmsService.pojo;

import java.io.Serializable;
import java.util.Date;

//盘点表
public class WmSttInGoods implements Serializable {
    private String id;
    private String createName;//创建人名称
    private String createBy;//创建人登录名称
    private String createDate;//创建日期
    private String updateName;//更新人名称
    private String updateBy;//更新人登录名称
    private Date updateDate;//更新日期
    private String sysOrgCode;//所属部门
    private String sysCompanyCode;//所属公司
    private String kuWeiBianMa;//储位编码
    private String binId;//托盘编码
    private String goodsId;//商品编码
    private String goodsName;//商品名称
    private String goodsQua;//数量
    private String goodsUnit;//单位
    private String goodsProData;//生产日期
    private String goodsBatch;//批次
    private String sttQua;//盘点数量
    private String cusCode;//客户编码
    private String cusName;//客户名称 zhongWenQch
    private String sttSta;//盘点状态
    private String baseUnit;//基本单位
    private String baseGoodscount;//基本单位数量
    private String sttId;
    private String goodsCode;//商品统一编码
    private String sttType;//盘点类型
    private String dongXian;//动线

    public WmSttInGoods() {
    }
    public WmSttInGoods(String id, String createName, String createBy, String createDate, String updateName, String updateBy, Date updateDate, String sysOrgCode, String sysCompanyCode, String kuWeiBianMa, String binId, String goodsId, String goodsName, String goodsQua, String goodsUnit, String goodsProData, String goodsBatch, String sttQua, String cusCode, String cusName, String sttSta, String baseUnit, String baseGoodscount, String sttId, String goodsCode, String sttType, String dongXian) {
        this.id = id;
        this.createName = createName;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateName = updateName;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.sysOrgCode = sysOrgCode;
        this.sysCompanyCode = sysCompanyCode;
        this.kuWeiBianMa = kuWeiBianMa;
        this.binId = binId;
        this.goodsId = goodsId;
        this.goodsName = goodsName;
        this.goodsQua = goodsQua;
        this.goodsUnit = goodsUnit;
        this.goodsProData = goodsProData;
        this.goodsBatch = goodsBatch;
        this.sttQua = sttQua;
        this.cusCode = cusCode;
        this.cusName = cusName;
        this.sttSta = sttSta;
        this.baseUnit = baseUnit;
        this.baseGoodscount = baseGoodscount;
        this.sttId = sttId;
        this.goodsCode = goodsCode;
        this.sttType = sttType;
        this.dongXian = dongXian;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getSysOrgCode() {
        return sysOrgCode;
    }

    public void setSysOrgCode(String sysOrgCode) {
        this.sysOrgCode = sysOrgCode;
    }

    public String getSysCompanyCode() {
        return sysCompanyCode;
    }

    public void setSysCompanyCode(String sysCompanyCode) {
        this.sysCompanyCode = sysCompanyCode;
    }

    public String getKuWeiBianMa() {
        return kuWeiBianMa;
    }

    public void setKuWeiBianMa(String kuWeiBianMa) {
        this.kuWeiBianMa = kuWeiBianMa;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsQua() {
        return goodsQua;
    }

    public void setGoodsQua(String goodsQua) {
        this.goodsQua = goodsQua;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public String getGoodsProData() {
        return goodsProData;
    }

    public void setGoodsProData(String goodsProData) {
        this.goodsProData = goodsProData;
    }

    public String getGoodsBatch() {
        return goodsBatch;
    }

    public void setGoodsBatch(String goodsBatch) {
        this.goodsBatch = goodsBatch;
    }

    public String getSttQua() {
        return sttQua;
    }

    public void setSttQua(String sttQua) {
        this.sttQua = sttQua;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSttSta() {
        return sttSta;
    }

    public void setSttSta(String sttSta) {
        this.sttSta = sttSta;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public String getBaseGoodscount() {
        return baseGoodscount;
    }

    public void setBaseGoodscount(String baseGoodscount) {
        this.baseGoodscount = baseGoodscount;
    }

    public String getSttId() {
        return sttId;
    }

    public void setSttId(String sttId) {
        this.sttId = sttId;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getSttType() {
        return sttType;
    }

    public void setSttType(String sttType) {
        this.sttType = sttType;
    }

    public String getDongXian() {
        return dongXian;
    }

    public void setDongXian(String dongXian) {
        this.dongXian = dongXian;
    }
}
