package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class BaStore implements Serializable {
 private int  storeId;
 private String storeCode;
 private String storeName;
 private String storeText;

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreText() {
        return storeText;
    }

    public void setStoreText(String storeText) {
        this.storeText = storeText;
    }

    public BaStore() {
    }

    public BaStore(int storeId, String storeCode, String storeName, String storeText) {
        this.storeId = storeId;
        this.storeCode = storeCode;
        this.storeName = storeName;
        this.storeText = storeText;
    }
}
