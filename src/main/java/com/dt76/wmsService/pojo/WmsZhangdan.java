package com.dt76.wmsService.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class WmsZhangdan {

    private long zdId;
    private long cusId;
    private long costId;

}
