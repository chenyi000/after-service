package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class BShop implements Serializable {
    private int  storeId;
    private String storeCode;
    private String storeName;
    private String storeText;
}
