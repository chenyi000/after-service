package com.dt76.wmsService.pojo;

import java.io.Serializable;
import java.util.Date;

//存储明细 视图
public class WvStockBase implements Serializable {

    private Date createDate; //创建日期
    private String createName; //创建人名称
    private String createBy; //创建人登录名称
    private String id;
    private String kucType;//库存类型
    private String kuWeiBianMa;//库位编码
    private String binId;//托盘码
    private String cusCode;//货主
    private String zhongWenQch;//中文全称
    private String goodsCode;//商品统一码
    private String goodsId;//商品编码
    private String goodsQua;//数量
    private String shpMingCheng;//商品名称
    private String goodsProData;//生产日期
    private String bzhiQi;//保质期
    private String yushoutianshu;//允收天数
    private String goodsUnit;//商品单位
    private String baseUnit;//基本单位
    private String baseGoodscount;//基本单位数量
    private String shlDanWei;//单位
    private String hiti;
    private String shpBianMakh;//商品客户编码
    private String chlShl;//拆零数量
    private String orderId;//原始单据编码
    private String zhlKg;//净重
    private String shpGuiGe;//商品规格

    public WvStockBase() {
    }

    public WvStockBase(Date createDate, String createName, String createBy, String id, String kucType, String kuWeiBianMa, String binId, String cusCode, String zhongWenQch, String goodsCode, String goodsId, String goodsQua, String shpMingCheng, String goodsProData, String bzhiQi, String yushoutianshu, String goodsUnit, String baseUnit, String baseGoodscount, String shlDanWei, String hiti, String shpBianMakh, String chlShl, String orderId, String zhlKg, String shpGuiGe) {
        this.createDate = createDate;
        this.createName = createName;
        this.createBy = createBy;
        this.id = id;
        this.kucType = kucType;
        this.kuWeiBianMa = kuWeiBianMa;
        this.binId = binId;
        this.cusCode = cusCode;
        this.zhongWenQch = zhongWenQch;
        this.goodsCode = goodsCode;
        this.goodsId = goodsId;
        this.goodsQua = goodsQua;
        this.shpMingCheng = shpMingCheng;
        this.goodsProData = goodsProData;
        this.bzhiQi = bzhiQi;
        this.yushoutianshu = yushoutianshu;
        this.goodsUnit = goodsUnit;
        this.baseUnit = baseUnit;
        this.baseGoodscount = baseGoodscount;
        this.shlDanWei = shlDanWei;
        this.hiti = hiti;
        this.shpBianMakh = shpBianMakh;
        this.chlShl = chlShl;
        this.orderId = orderId;
        this.zhlKg = zhlKg;
        this.shpGuiGe = shpGuiGe;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKucType() {
        return kucType;
    }

    public void setKucType(String kucType) {
        this.kucType = kucType;
    }

    public String getKuWeiBianMa() {
        return kuWeiBianMa;
    }

    public void setKuWeiBianMa(String kuWeiBianMa) {
        this.kuWeiBianMa = kuWeiBianMa;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getZhongWenQch() {
        return zhongWenQch;
    }

    public void setZhongWenQch(String zhongWenQch) {
        this.zhongWenQch = zhongWenQch;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsQua() {
        return goodsQua;
    }

    public void setGoodsQua(String goodsQua) {
        this.goodsQua = goodsQua;
    }

    public String getShpMingCheng() {
        return shpMingCheng;
    }

    public void setShpMingCheng(String shpMingCheng) {
        this.shpMingCheng = shpMingCheng;
    }

    public String getGoodsProData() {
        return goodsProData;
    }

    public void setGoodsProData(String goodsProData) {
        this.goodsProData = goodsProData;
    }

    public String getBzhiQi() {
        return bzhiQi;
    }

    public void setBzhiQi(String bzhiQi) {
        this.bzhiQi = bzhiQi;
    }

    public String getYushoutianshu() {
        return yushoutianshu;
    }

    public void setYushoutianshu(String yushoutianshu) {
        this.yushoutianshu = yushoutianshu;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public String getBaseGoodscount() {
        return baseGoodscount;
    }

    public void setBaseGoodscount(String baseGoodscount) {
        this.baseGoodscount = baseGoodscount;
    }

    public String getShlDanWei() {
        return shlDanWei;
    }

    public void setShlDanWei(String shlDanWei) {
        this.shlDanWei = shlDanWei;
    }

    public String getHiti() {
        return hiti;
    }

    public void setHiti(String hiti) {
        this.hiti = hiti;
    }

    public String getShpBianMakh() {
        return shpBianMakh;
    }

    public void setShpBianMakh(String shpBianMakh) {
        this.shpBianMakh = shpBianMakh;
    }

    public String getChlShl() {
        return chlShl;
    }

    public void setChlShl(String chlShl) {
        this.chlShl = chlShl;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getZhlKg() {
        return zhlKg;
    }

    public void setZhlKg(String zhlKg) {
        this.zhlKg = zhlKg;
    }

    public String getShpGuiGe() {
        return shpGuiGe;
    }

    public void setShpGuiGe(String shpGuiGe) {
        this.shpGuiGe = shpGuiGe;
    }
}
