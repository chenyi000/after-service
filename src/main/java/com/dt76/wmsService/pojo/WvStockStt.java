package com.dt76.wmsService.pojo;

import java.io.Serializable;
import java.util.Date;

//托盘盘点视图 同wv_stock_stt
public class WvStockStt implements Serializable {

    private Date createDate;//创建日期
    private String createName;//创建人名称
    private String createBy;//创建人登录名称
    private String id;
    private String kuctype;//库存类型
    private String kuWeiBianMa;//库位编码
    private String binId;
    private String cusCode;//货主
    private String zhongWenQch;//中文全称
    private String goodsCode;//商品统一码
    private String goodsId;//商品编码
    private Double goodsQua;//数量
    private String shpMingCheng;//商品名称
    private String goodsProData;//生产日期
    private String bzhiQi;//保质期
    private String yushoutianshu;//允收天数
    private String goodsUnit;//商品单位
    private String sttSta;//盘点状态
    private String moveSta;//状态
    private String lastMove;//最后移动

    public WvStockStt() {
    }
    public WvStockStt(Date createDate, String createName, String createBy, String id, String kuctype, String kuWeiBianMa, String binId, String cusCode, String zhongWenQch, String goodsCode, String goodsId, Double goodsQua, String shpMingCheng, String goodsProData, String bzhiQi, String yushoutianshu, String goodsUnit, String sttSta, String moveSta, String lastMove) {
        this.createDate = createDate;
        this.createName = createName;
        this.createBy = createBy;
        this.id = id;
        this.kuctype = kuctype;
        this.kuWeiBianMa = kuWeiBianMa;
        this.binId = binId;
        this.cusCode = cusCode;
        this.zhongWenQch = zhongWenQch;
        this.goodsCode = goodsCode;
        this.goodsId = goodsId;
        this.goodsQua = goodsQua;
        this.shpMingCheng = shpMingCheng;
        this.goodsProData = goodsProData;
        this.bzhiQi = bzhiQi;
        this.yushoutianshu = yushoutianshu;
        this.goodsUnit = goodsUnit;
        this.sttSta = sttSta;
        this.moveSta = moveSta;
        this.lastMove = lastMove;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKuctype() {
        return kuctype;
    }

    public void setKuctype(String kuctype) {
        this.kuctype = kuctype;
    }

    public String getKuWeiBianMa() {
        return kuWeiBianMa;
    }

    public void setKuWeiBianMa(String kuWeiBianMa) {
        this.kuWeiBianMa = kuWeiBianMa;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getZhongWenQch() {
        return zhongWenQch;
    }

    public void setZhongWenQch(String zhongWenQch) {
        this.zhongWenQch = zhongWenQch;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public Double getGoodsQua() {
        return goodsQua;
    }

    public void setGoodsQua(Double goodsQua) {
        this.goodsQua = goodsQua;
    }

    public String getShpMingCheng() {
        return shpMingCheng;
    }

    public void setShpMingCheng(String shpMingCheng) {
        this.shpMingCheng = shpMingCheng;
    }

    public String getGoodsProData() {
        return goodsProData;
    }

    public void setGoodsProData(String goodsProData) {
        this.goodsProData = goodsProData;
    }

    public String getBzhiQi() {
        return bzhiQi;
    }

    public void setBzhiQi(String bzhiQi) {
        this.bzhiQi = bzhiQi;
    }

    public String getYushoutianshu() {
        return yushoutianshu;
    }

    public void setYushoutianshu(String yushoutianshu) {
        this.yushoutianshu = yushoutianshu;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public String getSttSta() {
        return sttSta;
    }

    public void setSttSta(String sttSta) {
        this.sttSta = sttSta;
    }

    public String getMoveSta() {
        return moveSta;
    }

    public void setMoveSta(String moveSta) {
        this.moveSta = moveSta;
    }

    public String getLastMove() {
        return lastMove;
    }

    public void setLastMove(String lastMove) {
        this.lastMove = lastMove;
    }
}
