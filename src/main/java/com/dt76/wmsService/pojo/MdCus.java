package com.dt76.wmsService.pojo;

import java.util.Date;

//客户视图
public class MdCus {

    private String id;
    private String createName;//创建人名称
    private String createBy;//创建了登录名称
    private Date createDate;//创建日期
    private String updateName;//更新人名称
    private String updateBy;//更新人登录名称
    private Date updateDate;//更新日期
    private String sysOrgCode;//所属部门
    private String sysCompanyCode;//所属公司
    private String zhongWenQch;//中文全称
    private String zhuJiMa;//助记码
    private String keHuJianCheng;//客户简称
    private String keHuBianMa;//客户编码
    private String keHuYingWen;//客户英文名称
    private String keHuZhuangTai;//客户状态
    private String xingYeFenLei;//企业属性
    private String suoShuXingYe;//所属行业
    private Date zhongZhiHeShiJian;//终止合作时间
    private Date shenQingShiJian;//申请时间
    private String keHuShuXing;//客户属性
    private String guiSuZuZh;//归属组织代码
    private String guiShuSheng;//归属省份代码
    private String guiShuShiDai;//归属市代码
    private String guiShu;//归属区县代码
    private String diZhi;//地址
    private String zhuLianXiRen;//主联系人
    private String dianHua;//电话
    private String shouJi;//手机
    private String chuanZhen;//传真
    private String EmaildiZhi;//email地址
    private String zhuYngYeWu;//主营业务
    private String beiZhu;//备注
    private String mdCusId;

    public MdCus() {
    }
    public MdCus(String id, String createName, String createBy, Date createDate, String updateName, String updateBy, Date updateDate, String sysOrgCode, String sysCompanyCode, String zhongWenQch, String zhuJiMa, String keHuJianCheng, String keHuBianMa, String keHuYingWen, String keHuZhuangTai, String xingYeFenLei, String suoShuXingYe, Date zhongZhiHeShiJian, Date shenQingShiJian, String keHuShuXing, String guiSuZuZh, String guiShuSheng, String guiShuShiDai, String guiShu, String diZhi, String zhuLianXiRen, String dianHua, String shouJi, String chuanZhen, String emaildiZhi, String zhuYngYeWu, String beiZhu, String mdCusId) {
        this.id = id;
        this.createName = createName;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateName = updateName;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.sysOrgCode = sysOrgCode;
        this.sysCompanyCode = sysCompanyCode;
        this.zhongWenQch = zhongWenQch;
        this.zhuJiMa = zhuJiMa;
        this.keHuJianCheng = keHuJianCheng;
        this.keHuBianMa = keHuBianMa;
        this.keHuYingWen = keHuYingWen;
        this.keHuZhuangTai = keHuZhuangTai;
        this.xingYeFenLei = xingYeFenLei;
        this.suoShuXingYe = suoShuXingYe;
        this.zhongZhiHeShiJian = zhongZhiHeShiJian;
        this.shenQingShiJian = shenQingShiJian;
        this.keHuShuXing = keHuShuXing;
        this.guiSuZuZh = guiSuZuZh;
        this.guiShuSheng = guiShuSheng;
        this.guiShuShiDai = guiShuShiDai;
        this.guiShu = guiShu;
        this.diZhi = diZhi;
        this.zhuLianXiRen = zhuLianXiRen;
        this.dianHua = dianHua;
        this.shouJi = shouJi;
        this.chuanZhen = chuanZhen;
        EmaildiZhi = emaildiZhi;
        this.zhuYngYeWu = zhuYngYeWu;
        this.beiZhu = beiZhu;
        this.mdCusId = mdCusId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getSysOrgCode() {
        return sysOrgCode;
    }

    public void setSysOrgCode(String sysOrgCode) {
        this.sysOrgCode = sysOrgCode;
    }

    public String getSysCompanyCode() {
        return sysCompanyCode;
    }

    public void setSysCompanyCode(String sysCompanyCode) {
        this.sysCompanyCode = sysCompanyCode;
    }

    public String getZhongWenQch() {
        return zhongWenQch;
    }

    public void setZhongWenQch(String zhongWenQch) {
        this.zhongWenQch = zhongWenQch;
    }

    public String getZhuJiMa() {
        return zhuJiMa;
    }

    public void setZhuJiMa(String zhuJiMa) {
        this.zhuJiMa = zhuJiMa;
    }

    public String getKeHuJianCheng() {
        return keHuJianCheng;
    }

    public void setKeHuJianCheng(String keHuJianCheng) {
        this.keHuJianCheng = keHuJianCheng;
    }

    public String getKeHuBianMa() {
        return keHuBianMa;
    }

    public void setKeHuBianMa(String keHuBianMa) {
        this.keHuBianMa = keHuBianMa;
    }

    public String getKeHuYingWen() {
        return keHuYingWen;
    }

    public void setKeHuYingWen(String keHuYingWen) {
        this.keHuYingWen = keHuYingWen;
    }

    public String getKeHuZhuangTai() {
        return keHuZhuangTai;
    }

    public void setKeHuZhuangTai(String keHuZhuangTai) {
        this.keHuZhuangTai = keHuZhuangTai;
    }

    public String getXingYeFenLei() {
        return xingYeFenLei;
    }

    public void setXingYeFenLei(String xingYeFenLei) {
        this.xingYeFenLei = xingYeFenLei;
    }

    public String getSuoShuXingYe() {
        return suoShuXingYe;
    }

    public void setSuoShuXingYe(String suoShuXingYe) {
        this.suoShuXingYe = suoShuXingYe;
    }

    public Date getZhongZhiHeShiJian() {
        return zhongZhiHeShiJian;
    }

    public void setZhongZhiHeShiJian(Date zhongZhiHeShiJian) {
        this.zhongZhiHeShiJian = zhongZhiHeShiJian;
    }

    public Date getShenQingShiJian() {
        return shenQingShiJian;
    }

    public void setShenQingShiJian(Date shenQingShiJian) {
        this.shenQingShiJian = shenQingShiJian;
    }

    public String getKeHuShuXing() {
        return keHuShuXing;
    }

    public void setKeHuShuXing(String keHuShuXing) {
        this.keHuShuXing = keHuShuXing;
    }

    public String getGuiSuZuZh() {
        return guiSuZuZh;
    }

    public void setGuiSuZuZh(String guiSuZuZh) {
        this.guiSuZuZh = guiSuZuZh;
    }

    public String getGuiShuSheng() {
        return guiShuSheng;
    }

    public void setGuiShuSheng(String guiShuSheng) {
        this.guiShuSheng = guiShuSheng;
    }

    public String getGuiShuShiDai() {
        return guiShuShiDai;
    }

    public void setGuiShuShiDai(String guiShuShiDai) {
        this.guiShuShiDai = guiShuShiDai;
    }

    public String getGuiShu() {
        return guiShu;
    }

    public void setGuiShu(String guiShu) {
        this.guiShu = guiShu;
    }

    public String getDiZhi() {
        return diZhi;
    }

    public void setDiZhi(String diZhi) {
        this.diZhi = diZhi;
    }

    public String getZhuLianXiRen() {
        return zhuLianXiRen;
    }

    public void setZhuLianXiRen(String zhuLianXiRen) {
        this.zhuLianXiRen = zhuLianXiRen;
    }

    public String getDianHua() {
        return dianHua;
    }

    public void setDianHua(String dianHua) {
        this.dianHua = dianHua;
    }

    public String getShouJi() {
        return shouJi;
    }

    public void setShouJi(String shouJi) {
        this.shouJi = shouJi;
    }

    public String getChuanZhen() {
        return chuanZhen;
    }

    public void setChuanZhen(String chuanZhen) {
        this.chuanZhen = chuanZhen;
    }

    public String getEmaildiZhi() {
        return EmaildiZhi;
    }

    public void setEmaildiZhi(String emaildiZhi) {
        EmaildiZhi = emaildiZhi;
    }

    public String getZhuYngYeWu() {
        return zhuYngYeWu;
    }

    public void setZhuYngYeWu(String zhuYngYeWu) {
        this.zhuYngYeWu = zhuYngYeWu;
    }

    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }

    public String getMdCusId() {
        return mdCusId;
    }

    public void setMdCusId(String mdCusId) {
        this.mdCusId = mdCusId;
    }
}
