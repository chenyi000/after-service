package com.dt76.wmsService.pojo;


import java.util.Date;

public class SysEpartment {

  private long eId;
  private String eCode;
  private String eName;
  private long eParentId;
  private String eCreatename;
  private Date eCreateTime;
  private String eUpdatename;
  private Date eUpadateTime;

  public long geteId() {
    return eId;
  }

  public void seteId(long eId) {
    this.eId = eId;
  }

  public String geteCode() {
    return eCode;
  }

  public void seteCode(String eCode) {
    this.eCode = eCode;
  }

  public String geteName() {
    return eName;
  }

  public void seteName(String eName) {
    this.eName = eName;
  }

  public long geteParentId() {
    return eParentId;
  }

  public void seteParentId(long eParentId) {
    this.eParentId = eParentId;
  }

  public String geteCreatename() {
    return eCreatename;
  }

  public void seteCreatename(String eCreatename) {
    this.eCreatename = eCreatename;
  }

  public Date geteCreateTime() {
    return eCreateTime;
  }

  public void seteCreateTime(Date eCreateTime) {
    this.eCreateTime = eCreateTime;
  }

  public String geteUpdatename() {
    return eUpdatename;
  }

  public void seteUpdatename(String eUpdatename) {
    this.eUpdatename = eUpdatename;
  }

  public Date geteUpadateTime() {
    return eUpadateTime;
  }

  public void seteUpadateTime(Date eUpadateTime) {
    this.eUpadateTime = eUpadateTime;
  }
}
