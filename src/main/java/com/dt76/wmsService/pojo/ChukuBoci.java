package com.dt76.wmsService.pojo;

import java.io.Serializable;

/*
 *Created by 王超 on 2019/5/12
 */
public class ChukuBoci implements Serializable {
    private int spId;//商品ID
    private String bociNo;//生成的波次号

    public int getSpId() { return spId; }

    public void setSpId(int spId) { this.spId = spId; }

    public String getBociNo() { return bociNo; }

    public void setBociNo(String bociNo) { this.bociNo = bociNo; }
}
