package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class Tuopan implements Serializable {
    private int tId;
    private String tuopanName;//托盘名
    private String pTuopan;//源托盘
    private String dTuopan;//到托盘储位；
    private String pChuwei;//源储位
    private String dChuwei;//到储位
    private String zhuantai;//状态
}
