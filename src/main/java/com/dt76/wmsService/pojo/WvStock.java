package com.dt76.wmsService.pojo;

import java.util.Date;

//库存视图 同mv_stock_cus
public class WvStock {
    private String id;
    private Date createDate;//创建日期
    private String createName;//创建人名称
    private String createBy;//创建人登录名称
    private String kucType;//库存类型
    private int goodsQua;//商品数量
    private String goodsUnit;//商品单位
    private int baseGoodscount;//基本单位数量
    private String baseUnit;//基本单位
    private String kuWeiBianMa;//库位编码
    private String binId;//托盘码
    private String cusCode;//货主
    private String zhongWenQch;//中文全称
    private String goodsId;//商品编码
    private String goodsCode;//商品统一码
    private String shpMingCheng;//商品名称
    private String shlDanWei;//单位
    private String chlShl;//拆零数量
    private String goodsProData;//生产日期
    private String bzhiQi;//保质期
    private String shpGuiGe;//商品规格
    private String dqr;
    private String kuWeiLeiXing;//库位类型
    private String quHuoCiXu;//取货次序
    private String shangJiaCiXu;//上架次序
    private String binStore;//仓库

    public WvStock() {
    }
    public WvStock(String id, Date createDate, String createName, String createBy, String kucType, int goodsQua, String goodsUnit, int baseGoodscount, String baseUnit, String kuWeiBianMa, String binId, String cusCode, String zhongWenQch, String goodsId, String goodsCode, String shpMingCheng, String shlDanWei, String chlShl, String goodsProData, String bzhiQi, String shpGuiGe, String dqr, String kuWeiLeiXing, String quHuoCiXu, String shangJiaCiXu, String binStore) {
        this.id = id;
        this.createDate = createDate;
        this.createName = createName;
        this.createBy = createBy;
        this.kucType = kucType;
        this.goodsQua = goodsQua;
        this.goodsUnit = goodsUnit;
        this.baseGoodscount = baseGoodscount;
        this.baseUnit = baseUnit;
        this.kuWeiBianMa = kuWeiBianMa;
        this.binId = binId;
        this.cusCode = cusCode;
        this.zhongWenQch = zhongWenQch;
        this.goodsId = goodsId;
        this.goodsCode = goodsCode;
        this.shpMingCheng = shpMingCheng;
        this.shlDanWei = shlDanWei;
        this.chlShl = chlShl;
        this.goodsProData = goodsProData;
        this.bzhiQi = bzhiQi;
        this.shpGuiGe = shpGuiGe;
        this.dqr = dqr;
        this.kuWeiLeiXing = kuWeiLeiXing;
        this.quHuoCiXu = quHuoCiXu;
        this.shangJiaCiXu = shangJiaCiXu;
        this.binStore = binStore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getKucType() {
        return kucType;
    }

    public void setKucType(String kucType) {
        this.kucType = kucType;
    }

    public int getGoodsQua() {
        return goodsQua;
    }

    public void setGoodsQua(int goodsQua) {
        this.goodsQua = goodsQua;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public int getBaseGoodscount() {
        return baseGoodscount;
    }

    public void setBaseGoodscount(int baseGoodscount) {
        this.baseGoodscount = baseGoodscount;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public String getKuWeiBianMa() {
        return kuWeiBianMa;
    }

    public void setKuWeiBianMa(String kuWeiBianMa) {
        this.kuWeiBianMa = kuWeiBianMa;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getZhongWenQch() {
        return zhongWenQch;
    }

    public void setZhongWenQch(String zhongWenQch) {
        this.zhongWenQch = zhongWenQch;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getShpMingCheng() {
        return shpMingCheng;
    }

    public void setShpMingCheng(String shpMingCheng) {
        this.shpMingCheng = shpMingCheng;
    }

    public String getShlDanWei() {
        return shlDanWei;
    }

    public void setShlDanWei(String shlDanWei) {
        this.shlDanWei = shlDanWei;
    }

    public String getChlShl() {
        return chlShl;
    }

    public void setChlShl(String chlShl) {
        this.chlShl = chlShl;
    }

    public String getGoodsProData() {
        return goodsProData;
    }

    public void setGoodsProData(String goodsProData) {
        this.goodsProData = goodsProData;
    }

    public String getBzhiQi() {
        return bzhiQi;
    }

    public void setBzhiQi(String bzhiQi) {
        this.bzhiQi = bzhiQi;
    }

    public String getShpGuiGe() {
        return shpGuiGe;
    }

    public void setShpGuiGe(String shpGuiGe) {
        this.shpGuiGe = shpGuiGe;
    }

    public String getDqr() {
        return dqr;
    }

    public void setDqr(String dqr) {
        this.dqr = dqr;
    }

    public String getKuWeiLeiXing() {
        return kuWeiLeiXing;
    }

    public void setKuWeiLeiXing(String kuWeiLeiXing) {
        this.kuWeiLeiXing = kuWeiLeiXing;
    }

    public String getQuHuoCiXu() {
        return quHuoCiXu;
    }

    public void setQuHuoCiXu(String quHuoCiXu) {
        this.quHuoCiXu = quHuoCiXu;
    }

    public String getShangJiaCiXu() {
        return shangJiaCiXu;
    }

    public void setShangJiaCiXu(String shangJiaCiXu) {
        this.shangJiaCiXu = shangJiaCiXu;
    }

    public String getBinStore() {
        return binStore;
    }

    public void setBinStore(String binStore) {
        this.binStore = binStore;
    }
}
