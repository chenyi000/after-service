package com.dt76.wmsService.pojo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysEmployeeType implements Serializable {

  private long tId;
  private String tCode;
  private String tName;
  private String tCreatename;
  private Date tCreateTime;
  private String tUpdatename;
  private Date tUpdateTime;
}
