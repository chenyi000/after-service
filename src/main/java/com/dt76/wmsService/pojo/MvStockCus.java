package com.dt76.wmsService.pojo;

import java.security.PrivateKey;

//客户库存 视图
public class MvStockCus {
    private String id;
    private String kucType;//库存类型
    private int goodsQua;//商品数量
    private String goodsUnit;//商品单位
    private int baseGoodscount;//基本商品类
    private String base_unit;//基本单位
    private String kuWeiBianMa;//库位编码
    private String binId;//托盘码
    private String cusCode;//货主
    private String zhongWenQch;//中文全称
    private String goodsProData;//生产日期
    private String bzhiQi;//保质期
    private String dqr;
    private String kuWeiLeiXing;//库位类型
    private String quHuoCiXu;//取货次序
    private String shangJiaCiXu;//上架次序
    private String goodsId;//商品编码
    private String shpMingCheng;//商品名称
    private String shlDanWei;//单位
    private String hiti;
    private String shpBianMakh;//商品客户编码

    public MvStockCus() {
    }
    public MvStockCus(String id, String kucType, int goodsQua, String goodsUnit, int baseGoodscount, String base_unit, String kuWeiBianMa, String binId, String cusCode, String zhongWenQch, String goodsProData, String bzhiQi, String dqr, String kuWeiLeiXing, String quHuoCiXu, String shangJiaCiXu, String goodsId, String shpMingCheng, String shlDanWei, String hiti, String shpBianMakh) {
        this.id = id;
        this.kucType = kucType;
        this.goodsQua = goodsQua;
        this.goodsUnit = goodsUnit;
        this.baseGoodscount = baseGoodscount;
        this.base_unit = base_unit;
        this.kuWeiBianMa = kuWeiBianMa;
        this.binId = binId;
        this.cusCode = cusCode;
        this.zhongWenQch = zhongWenQch;
        this.goodsProData = goodsProData;
        this.bzhiQi = bzhiQi;
        this.dqr = dqr;
        this.kuWeiLeiXing = kuWeiLeiXing;
        this.quHuoCiXu = quHuoCiXu;
        this.shangJiaCiXu = shangJiaCiXu;
        this.goodsId = goodsId;
        this.shpMingCheng = shpMingCheng;
        this.shlDanWei = shlDanWei;
        this.hiti = hiti;
        this.shpBianMakh = shpBianMakh;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKucType() {
        return kucType;
    }

    public void setKucType(String kucType) {
        this.kucType = kucType;
    }

    public int getGoodsQua() {
        return goodsQua;
    }

    public void setGoodsQua(int goodsQua) {
        this.goodsQua = goodsQua;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public int getBaseGoodscount() {
        return baseGoodscount;
    }

    public void setBaseGoodscount(int baseGoodscount) {
        this.baseGoodscount = baseGoodscount;
    }

    public String getBase_unit() {
        return base_unit;
    }

    public void setBase_unit(String base_unit) {
        this.base_unit = base_unit;
    }

    public String getKuWeiBianMa() {
        return kuWeiBianMa;
    }

    public void setKuWeiBianMa(String kuWeiBianMa) {
        this.kuWeiBianMa = kuWeiBianMa;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getZhongWenQch() {
        return zhongWenQch;
    }

    public void setZhongWenQch(String zhongWenQch) {
        this.zhongWenQch = zhongWenQch;
    }

    public String getGoodsProData() {
        return goodsProData;
    }

    public void setGoodsProData(String goodsProData) {
        this.goodsProData = goodsProData;
    }

    public String getBzhiQi() {
        return bzhiQi;
    }

    public void setBzhiQi(String bzhiQi) {
        this.bzhiQi = bzhiQi;
    }

    public String getDqr() {
        return dqr;
    }

    public void setDqr(String dqr) {
        this.dqr = dqr;
    }

    public String getKuWeiLeiXing() {
        return kuWeiLeiXing;
    }

    public void setKuWeiLeiXing(String kuWeiLeiXing) {
        this.kuWeiLeiXing = kuWeiLeiXing;
    }

    public String getQuHuoCiXu() {
        return quHuoCiXu;
    }

    public void setQuHuoCiXu(String quHuoCiXu) {
        this.quHuoCiXu = quHuoCiXu;
    }

    public String getShangJiaCiXu() {
        return shangJiaCiXu;
    }

    public void setShangJiaCiXu(String shangJiaCiXu) {
        this.shangJiaCiXu = shangJiaCiXu;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getShpMingCheng() {
        return shpMingCheng;
    }

    public void setShpMingCheng(String shpMingCheng) {
        this.shpMingCheng = shpMingCheng;
    }

    public String getShlDanWei() {
        return shlDanWei;
    }

    public void setShlDanWei(String shlDanWei) {
        this.shlDanWei = shlDanWei;
    }

    public String getHiti() {
        return hiti;
    }

    public void setHiti(String hiti) {
        this.hiti = hiti;
    }

    public String getShpBianMakh() {
        return shpBianMakh;
    }

    public void setShpBianMakh(String shpBianMakh) {
        this.shpBianMakh = shpBianMakh;
    }
}
