package com.dt76.wmsService.pojo;

import java.io.Serializable;

public class Plat implements Serializable {
    private long pId; //主键id
    private String carNo; //车牌号
    private String docId; //单据编号
    private String platId; //月台编号
    private String inDate; //计入时间
    private String outDate; //驶出时间
    private String platIndate; //计划进入时间
    private String platOutdate; //计划驶出时间
    private String sj; //司机
    private String sjPhone; //司机电话
    private String platSta;  //月台状态

    private String spId; //商品id
    public Plat() {
    }

    public Plat(long pId, String carNo, String docId, String platId, String inDate, String outDate, String platIndate, String platOutdate, String sj, String sjPhone, String platSta, String spId) {
        this.pId = pId;
        this.carNo = carNo;
        this.docId = docId;
        this.platId = platId;
        this.inDate = inDate;
        this.outDate = outDate;
        this.platIndate = platIndate;
        this.platOutdate = platOutdate;
        this.sj = sj;
        this.sjPhone = sjPhone;
        this.platSta = platSta;
        this.spId = spId;
    }

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getPlatId() {
        return platId;
    }

    public void setPlatId(String platId) {
        this.platId = platId;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getPlatIndate() {
        return platIndate;
    }

    public void setPlatIndate(String platIndate) {
        this.platIndate = platIndate;
    }

    public String getPlatOutdate() {
        return platOutdate;
    }

    public void setPlatOutdate(String platOutdate) {
        this.platOutdate = platOutdate;
    }

    public String getSj() {
        return sj;
    }

    public void setSj(String sj) {
        this.sj = sj;
    }

    public String getSjPhone() {
        return sjPhone;
    }

    public void setSjPhone(String sjPhone) {
        this.sjPhone = sjPhone;
    }

    public String getPlatSta() {
        return platSta;
    }

    public void setPlatSta(String platSta) {
        this.platSta = platSta;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }
}
