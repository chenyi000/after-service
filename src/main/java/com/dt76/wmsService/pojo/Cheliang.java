package com.dt76.wmsService.pojo;

public class Cheliang {

    private long cId;
    private String chePaiHao;
    private String cheXing;
    private String zaiZhong;
    private String userName;
    private String userPhone;
    private String xingBie;
    private String zhuangTai;
    private String beiZhu;

    public Cheliang() {
    }

    public Cheliang(String chePaiHao, String cheXing, String zaiZhong, String userName, String userPhone, String xingBie, String zhuangTai, String beiZhu) {
        this.chePaiHao = chePaiHao;
        this.cheXing = cheXing;
        this.zaiZhong = zaiZhong;
        this.userName = userName;
        this.userPhone = userPhone;
        this.xingBie = xingBie;
        this.zhuangTai = zhuangTai;
        this.beiZhu = beiZhu;
    }

    public long getcId() {
        return cId;
    }

    public void setcId(long cId) {
        this.cId = cId;
    }

    public String getChePaiHao() {
        return chePaiHao;
    }

    public void setChePaiHao(String chePaiHao) {
        this.chePaiHao = chePaiHao;
    }

    public String getCheXing() {
        return cheXing;
    }

    public void setCheXing(String cheXing) {
        this.cheXing = cheXing;
    }

    public String getZaiZhong() {
        return zaiZhong;
    }

    public void setZaiZhong(String zaiZhong) {
        this.zaiZhong = zaiZhong;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getXingBie() {
        return xingBie;
    }

    public void setXingBie(String xingBie) {
        this.xingBie = xingBie;
    }

    public String getZhuangTai() {
        return zhuangTai;
    }

    public void setZhuangTai(String zhuangTai) {
        this.zhuangTai = zhuangTai;
    }

    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }
}
