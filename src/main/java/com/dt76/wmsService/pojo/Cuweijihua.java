package com.dt76.wmsService.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class Cuweijihua implements Serializable {

    /*private  int cId;//主键
    private  String createName;//创建人名称
    private String  createDate;//创建日期
    private String updateName;//更新人名称
    private String updateDate;//更新日期
    private String goodsId;//商品编码
    private String goodsName;//商品名称
    private String goodsQua;//数量
    private String goodsCount;//基本单位数量
    private String goodsDate;//生产日期
    private String toGoodsDate;//到生产日期
    private String goodsunit;//单位
    private String cusCode;//客户编码
    private String cusName;//客户名称
    private String tinFrom;//源托盘
    private String tinId;//到托盘
    private String binFrom;//源储位
    private String binId;//到储位
    private String moveSta;//状态
    private String runSta;//执行状态
    private String toCusCode;//转移客户
    private String toCusName;//转移客户名称

    public Cuweijihua() {

    }

    public Cuweijihua(String goodsName) {
        this.goodsName = goodsName;
    }

    public Cuweijihua(int cId, String createName, String createDate, String updateName, String updateDate, String goodsId, String goodsName, String goodsQua, String goodsCount, String goodsDate, String toGoodsDate, String goodsunit, String cusCode, String cusName, String tinFrom, String tinId, String binFrom, String binId, String moveSta, String runSta, String toCusCode, String toCusName) {
        this.cId = cId;
        this.createName = createName;
        this.createDate = createDate;
        this.updateName = updateName;
        this.updateDate = updateDate;
        this.goodsId = goodsId;
        this.goodsName = goodsName;
        this.goodsQua = goodsQua;
        this.goodsCount = goodsCount;
        this.goodsDate = goodsDate;
        this.toGoodsDate = toGoodsDate;
        this.goodsunit = goodsunit;
        this.cusCode = cusCode;
        this.cusName = cusName;
        this.tinFrom = tinFrom;
        this.tinId = tinId;
        this.binFrom = binFrom;
        this.binId = binId;
        this.moveSta = moveSta;
        this.runSta = runSta;
        this.toCusCode = toCusCode;
        this.toCusName = toCusName;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsQua() {
        return goodsQua;
    }

    public void setGoodsQua(String goodsQua) {
        this.goodsQua = goodsQua;
    }

    public String getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(String goodsCount) {
        this.goodsCount = goodsCount;
    }

    public String getGoodsDate() {
        return goodsDate;
    }

    public void setGoodsDate(String goodsDate) {
        this.goodsDate = goodsDate;
    }

    public String getToGoodsDate() {
        return toGoodsDate;
    }

    public void setToGoodsDate(String toGoodsDate) {
        this.toGoodsDate = toGoodsDate;
    }

    public String getGoodsunit() {
        return goodsunit;
    }

    public void setGoodsunit(String goodsunit) {
        this.goodsunit = goodsunit;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getTinFrom() {
        return tinFrom;
    }

    public void setTinFrom(String tinFrom) {
        this.tinFrom = tinFrom;
    }

    public String getTinId() {
        return tinId;
    }

    public void setTinId(String tinId) {
        this.tinId = tinId;
    }

    public String getBinFrom() {
        return binFrom;
    }

    public void setBinFrom(String binFrom) {
        this.binFrom = binFrom;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getMoveSta() {
        return moveSta;
    }

    public void setMoveSta(String moveSta) {
        this.moveSta = moveSta;
    }

    public String getRunSta() {
        return runSta;
    }

    public void setRunSta(String runSta) {
        this.runSta = runSta;
    }

    public String getToCusCode() {
        return toCusCode;
    }

    public void setToCusCode(String toCusCode) {
        this.toCusCode = toCusCode;
    }

    public String getToCusName() {
        return toCusName;
    }

    public void setToCusName(String toCusName) {
        this.toCusName = toCusName;
    }*/
    private int cId;
    private String storeName;//仓库名称
    private String chuweiName;//储位名称
    private String tuopanName;//托盘名称
    private String goodsName;//商品名称
    private BShop bShop;
    private Tuopan tuopan;
    private Chuwei chuwei;
    private WmsShop wmsShop;
    private String tinFrom;//源托盘
    private String tinId;//到托盘
    private String binFrom;//源储位
    private String binId;//到储位
    private String moveSta;//状态
    private WmsOrder wmsOrder;



    private Plat plat;
    private Platform platform;
    private ChukuBoci chukuBoci;
    private WmsHuiDan wmsHuiDan;
    private SpZhuanTai spZhuanTai;

}
