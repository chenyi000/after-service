package com.dt76.wmsService.pojo;

import java.util.Date;

//商品信息
public class MvGoods {

    private String id;
    private String createName;//创建人名称
    private String createBy;//创建人登录名称
    private Date createDate;//创建日期
    private String updateName;//更新人名称
    private String updateBy;//更新人登录名称
    private Date updateDate;//更新日期
    private String sysOrgCode;//所属部门
    private String sysCompanyCode;//所属公司
    private String suoShuKeHu;//所属客户
    private String shpMingCheng;//商品名称
    private String shpJianCheng;//商品简称
    private String shpBianMa;//商品编码
    private String shpXingHao;//商品型号
    private String shpGuiGe;//商品规格
    private String shpYanSe;//商品颜色
    private String chpShuXing;//商品属性
    private String mpDanCeng;//码盘单层数量
    private String mpCengGao;//码盘层高
    private String jfShpLei;//计费类商品
    private String shpPinPai;//商品品牌
    private String shpTiaoMa;//商品条码
    private String bzhiQi;//保质期
    private String shlDanWei;//单位
    private String jshDanWei;//拆零单位
    private String tiJiCm;//体积
    private String zhlKg;//净重
    private String chlShl;//拆零数量
    private String chcDanWei;//尺寸单位
    private String shpMiaoShu;//商品描述
    private String zhuangTai;//状态
    private String zhlKgm;//毛重
    private String ShpBianMakh;//商品客户编码

    public MvGoods() {
    }
    public MvGoods(String id, String createName, String createBy, Date createDate, String updateName, String updateBy, Date updateDate, String sysOrgCode, String sysCompanyCode, String suoShuKeHu, String shpMingCheng, String shpJianCheng, String shpBianMa, String shpXingHao, String shpGuiGe, String shpYanSe, String chpShuXing, String mpDanCeng, String mpCengGao, String jfShpLei, String shpPinPai, String shpTiaoMa, String bzhiQi, String shlDanWei, String jshDanWei, String tiJiCm, String zhlKg, String chlShl, String chcDanWei, String shpMiaoShu, String zhuangTai, String zhlKgm, String shpBianMakh) {
        this.id = id;
        this.createName = createName;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateName = updateName;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.sysOrgCode = sysOrgCode;
        this.sysCompanyCode = sysCompanyCode;
        this.suoShuKeHu = suoShuKeHu;
        this.shpMingCheng = shpMingCheng;
        this.shpJianCheng = shpJianCheng;
        this.shpBianMa = shpBianMa;
        this.shpXingHao = shpXingHao;
        this.shpGuiGe = shpGuiGe;
        this.shpYanSe = shpYanSe;
        this.chpShuXing = chpShuXing;
        this.mpDanCeng = mpDanCeng;
        this.mpCengGao = mpCengGao;
        this.jfShpLei = jfShpLei;
        this.shpPinPai = shpPinPai;
        this.shpTiaoMa = shpTiaoMa;
        this.bzhiQi = bzhiQi;
        this.shlDanWei = shlDanWei;
        this.jshDanWei = jshDanWei;
        this.tiJiCm = tiJiCm;
        this.zhlKg = zhlKg;
        this.chlShl = chlShl;
        this.chcDanWei = chcDanWei;
        this.shpMiaoShu = shpMiaoShu;
        this.zhuangTai = zhuangTai;
        this.zhlKgm = zhlKgm;
        ShpBianMakh = shpBianMakh;
    }

        public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getSysOrgCode() {
        return sysOrgCode;
    }

    public void setSysOrgCode(String sysOrgCode) {
        this.sysOrgCode = sysOrgCode;
    }

    public String getSysCompanyCode() {
        return sysCompanyCode;
    }

    public void setSysCompanyCode(String sysCompanyCode) {
        this.sysCompanyCode = sysCompanyCode;
    }

    public String getSuoShuKeHu() {
        return suoShuKeHu;
    }

    public void setSuoShuKeHu(String suoShuKeHu) {
        this.suoShuKeHu = suoShuKeHu;
    }

    public String getShpMingCheng() {
        return shpMingCheng;
    }

    public void setShpMingCheng(String shpMingCheng) {
        this.shpMingCheng = shpMingCheng;
    }

    public String getShpJianCheng() {
        return shpJianCheng;
    }

    public void setShpJianCheng(String shpJianCheng) {
        this.shpJianCheng = shpJianCheng;
    }

    public String getShpBianMa() {
        return shpBianMa;
    }

    public void setShpBianMa(String shpBianMa) {
        this.shpBianMa = shpBianMa;
    }

    public String getShpXingHao() {
        return shpXingHao;
    }

    public void setShpXingHao(String shpXingHao) {
        this.shpXingHao = shpXingHao;
    }

    public String getShpGuiGe() {
        return shpGuiGe;
    }

    public void setShpGuiGe(String shpGuiGe) {
        this.shpGuiGe = shpGuiGe;
    }

    public String getShpYanSe() {
        return shpYanSe;
    }

    public void setShpYanSe(String shpYanSe) {
        this.shpYanSe = shpYanSe;
    }

    public String getChpShuXing() {
        return chpShuXing;
    }

    public void setChpShuXing(String chpShuXing) {
        this.chpShuXing = chpShuXing;
    }

    public String getMpDanCeng() {
        return mpDanCeng;
    }

    public void setMpDanCeng(String mpDanCeng) {
        this.mpDanCeng = mpDanCeng;
    }

    public String getMpCengGao() {
        return mpCengGao;
    }

    public void setMpCengGao(String mpCengGao) {
        this.mpCengGao = mpCengGao;
    }

    public String getJfShpLei() {
        return jfShpLei;
    }

    public void setJfShpLei(String jfShpLei) {
        this.jfShpLei = jfShpLei;
    }

    public String getShpPinPai() {
        return shpPinPai;
    }

    public void setShpPinPai(String shpPinPai) {
        this.shpPinPai = shpPinPai;
    }

    public String getShpTiaoMa() {
        return shpTiaoMa;
    }

    public void setShpTiaoMa(String shpTiaoMa) {
        this.shpTiaoMa = shpTiaoMa;
    }

    public String getBzhiQi() {
        return bzhiQi;
    }

    public void setBzhiQi(String bzhiQi) {
        this.bzhiQi = bzhiQi;
    }

    public String getShlDanWei() {
        return shlDanWei;
    }

    public void setShlDanWei(String shlDanWei) {
        this.shlDanWei = shlDanWei;
    }

    public String getJshDanWei() {
        return jshDanWei;
    }

    public void setJshDanWei(String jshDanWei) {
        this.jshDanWei = jshDanWei;
    }

    public String getTiJiCm() {
        return tiJiCm;
    }

    public void setTiJiCm(String tiJiCm) {
        this.tiJiCm = tiJiCm;
    }

    public String getZhlKg() {
        return zhlKg;
    }

    public void setZhlKg(String zhlKg) {
        this.zhlKg = zhlKg;
    }

    public String getChlShl() {
        return chlShl;
    }

    public void setChlShl(String chlShl) {
        this.chlShl = chlShl;
    }

    public String getChcDanWei() {
        return chcDanWei;
    }

    public void setChcDanWei(String chcDanWei) {
        this.chcDanWei = chcDanWei;
    }

    public String getShpMiaoShu() {
        return shpMiaoShu;
    }

    public void setShpMiaoShu(String shpMiaoShu) {
        this.shpMiaoShu = shpMiaoShu;
    }

    public String getZhuangTai() {
        return zhuangTai;
    }

    public void setZhuangTai(String zhuangTai) {
        this.zhuangTai = zhuangTai;
    }

    public String getZhlKgm() {
        return zhlKgm;
    }

    public void setZhlKgm(String zhlKgm) {
        this.zhlKgm = zhlKgm;
    }

    public String getShpBianMakh() {
        return ShpBianMakh;
    }

    public void setShpBianMakh(String shpBianMakh) {
        ShpBianMakh = shpBianMakh;
    }
}
