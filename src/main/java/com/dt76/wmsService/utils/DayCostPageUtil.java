package com.dt76.wmsService.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class DayCostPageUtil implements Serializable {

    private int id;
    private String cUsername;//客户名称
    private String dayCostYj;//每日费用
    private String shpName;
    private String costJs;//是否结算
    // private Date costData;
    private String shpWeight;//商品重量
    private String shpNum;//商品数量
    private String shpTiji;//商品体积
    private String unit;//单位
    private String yue;//余额
    private String costDate;//费用日期
    // private WmsShop wmsShop;
    //private SysCustomer sysCustomers;

    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
