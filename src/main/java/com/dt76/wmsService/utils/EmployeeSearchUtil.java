package com.dt76.wmsService.utils;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 17:30
 */

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 员工页搜索条件
 */

@Data
public class EmployeeSearchUtil implements Serializable {

    private String eRealName;
    private String departementName;
    private Date eCreateTime;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
