package com.dt76.wmsService.utils;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 17:29
 */

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 员工分页信息工具类
 */
@Data
public class EmployeePageUtil implements Serializable {

    private long eId;
    private String eCode;
    private String eLoadname;
    private String ePassword;
    private String eRealName;
    private String eTelphone;
    private String eEmial;
    private String eAddress;
    private Integer eStatus;
    private String eCreateName;
    private Date eCreateTime;
    private String eUpdateName;
    private Date eUpdateTime;

    private String deptName;
    private String jobName;

    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
