package com.dt76.wmsService.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-16 12:28
 */
@Data
@Component
public class OrderSh implements Serializable {
    private String cus;
    private String cusName;

    private String shpName;
    private String cusPhone;
    private String cusAdd;
    private String mas;
    private String masPhone;
    private String masAdd;


    private String shpType;//商品类型
    private String shpCode;//商品编码
    private String shpNum;//商品数量
    private String proDate;//生产日期
    private String baozhiqi;//保质期
    private String inType;//存放类型
    private String inDay;//存放时间


}
