package com.dt76.wmsService.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Data
@Component
public class ZhangDanSearchUtil implements Serializable {

    private int cid;//客户id
    private String name;
    private String startDate;
    private String endDate;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
