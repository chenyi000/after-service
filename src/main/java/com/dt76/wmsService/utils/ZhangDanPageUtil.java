package com.dt76.wmsService.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class ZhangDanPageUtil implements Serializable {

    private int id;
    private String cUsername;
    private String cCode;

    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
