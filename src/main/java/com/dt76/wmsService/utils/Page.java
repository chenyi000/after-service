package com.dt76.wmsService.utils;

import java.io.Serializable;
import java.util.List;

/**
 * 分页的工具类
 *
 * @author hjj05
 */
public class Page  implements Serializable {
    
    //每页显示的条数
    private int pageSize = 5;
    //数据的总条数
    private int totalCount;
    //总页数
    private int totalPageCount;
    //当前页,默认值为1
    private int pageIndex = 1;
    
    //封装结果
    private List rows;
    
    public List getRows() {
        return rows;
    }
    
    public void setRows(List rows) {
        this.rows = rows;
    }
    
    public int getPageSize() {
        return pageSize;
    }
    
    public int getTotalCount() {
        return totalCount;
    }
    
    public void setTotalCount(int totalCount) {
        if (totalCount < 0) {
            System.out.println("记录数不能为负数！");
            return;
        }
        this.totalCount = totalCount;
    }
    
    /*
     * ****最为核心的公式****
     */
    public int getTotalPageCount() {
        return this.totalCount % this.pageSize == 0
                   ? this.totalCount / this.pageSize
                   : this.totalCount / this.pageSize + 1;
    }
    
    public int getPageIndex() {
        return pageIndex;
    }
    
    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }
    
}
