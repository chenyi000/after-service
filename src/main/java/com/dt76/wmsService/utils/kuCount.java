package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-16 16:47
 */
@Data
public class kuCount implements Serializable {
    private int id;
    private int count;
}
