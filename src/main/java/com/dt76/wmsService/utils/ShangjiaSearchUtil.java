package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: u3
 * @description: 用于插入商品id，仓库id，储位id
 * @author: wx
 * @create: 2019-05-15 15:43
 */
@Data
public class ShangjiaSearchUtil implements Serializable {

    private int spId;
    private int cangku;
    private int chuwei;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
