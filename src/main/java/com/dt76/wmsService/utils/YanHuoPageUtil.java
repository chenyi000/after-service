package com.dt76.wmsService.utils;


import lombok.Data;

import java.io.Serializable;


@Data
public class YanHuoPageUtil implements Serializable {

    private String daoId;
    private String docId; //单据编号
    private String spId; //商品id
    private String platformName;
    private String siji;


    private int oId;
    private String createName;
    private String createDate;
    private String cusCode;//订单编号
    private String cusName;//客户姓名
    private String masName;//收件人姓名
    private String cusAddress;//客户地址
    private String masAddress;//收件人地址
    private String cusPhone;//客户电话
    private String masPhone;//收件人电话
    private int shopId;//商品id
    private String typeId;//订单类型

    private String shpName;//商品名称
    private String shpType;//商品类型
    private String shpCode;//商品编码
    private String shpNum;//商品数量
    private String shpWeight;//商品重量
    private String shpTiji;//商品体积
    private String inType;//存放类型
    private String proDate;//生产日期
    private String baozhiqi;//保质期
    private String unit;//商品单位
    private int inDay;//存放时间
    private int statusId;//业务状态
    private String status1;//验货
    private String status2;//上架

    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
