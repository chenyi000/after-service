package com.dt76.wmsService.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class DayCostSearchUtil implements Serializable {
    private String name;
    private String costJs;


    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;


}
