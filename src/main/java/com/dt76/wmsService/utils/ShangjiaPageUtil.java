package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: u3
 * @description:
 * @author: wx
 * @create: 2019-05-15 16:11
 */
@Data
public class ShangjiaPageUtil implements Serializable {

    private int cid;
    private String storeName;
    private String chuweiName;
    private String createDate;
    private String cusCode;//订单编号
    private String cusName;//客户姓名
    private int shopId;//商品id

    private String shpName;//商品名称
    private String shpType;//商品类型
    private String shpCode;//商品编码
    private String shpNum;//商品数量
    private String shpWeight;//商品重量
    private String shpTiji;//商品体积
    private String inType;//存放类型
    private String proDate;//生产日期
    private String baozhiqi;//保质期
    private String unit;//商品单位
    private int inDay;//存放时间
    private int statusId;//业务状态
    private String status;//库存


    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
