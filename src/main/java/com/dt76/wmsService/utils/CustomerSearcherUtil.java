package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 8:19
 */

@Data
public class CustomerSearcherUtil implements Serializable {

    private String cRealname;
    private Date cCreateTime;
    private String cCreateTimestr;
    private Integer cCustomerType;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
