package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;

@Data
public class BShopSearchUtil implements Serializable {

    private String storeCode;
    private String storeName;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
