package com.dt76.wmsService.utils;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 17:29
 */

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;



@Data
@Component
public class OrderPageUtil implements Serializable {

    private int oId;
    private String createName;
    private String createDate;
    private String cusCode;//订单编号
    private String cusName;//客户姓名
    private String masName;//收件人姓名
    private String cusAddress;//客户地址
    private String masAddress;//收件人地址
    private String cusPhone;//客户电话
    private String masPhone;//收件人电话
    private int shopId;//商品id
    private String typeId;//订单类型

    private String shpName;//商品名称
    private String shpType;//商品类型
    private String shpCode;//商品编码
    private String shpNum;//商品数量
    private String shpWeight;//商品重量
    private String shpTiji;//商品体积
    private String inType;//存放类型
    private String proDate;//生产日期
    private String baozhiqi;//保质期
    private String unit;//商品单位
    private int inDay;//存放时间
    private int statusId;//业务状态
    private String status;//业务状态

    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
