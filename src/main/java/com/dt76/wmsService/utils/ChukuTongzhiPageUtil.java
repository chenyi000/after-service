package com.dt76.wmsService.utils;

import com.dt76.wmsService.pojo.*;
import lombok.Data;

import java.io.Serializable;

/*
 *Created by 王超 on 2019/5/15
 */
@Data
public class ChukuTongzhiPageUtil implements Serializable {
//    private String masName;
//    private String masPhone;
//    private String masAddress;
//    private String cusName;
//    private String cusCode;
//    private int id;
//    private String chuhuoTime;
//    private String chuDocId;
//    private String carno;
//    private String siji;
//    private String sijiPhone;
//    private String platformName;

    private WmsOrder wmsOrder;
    private WmsShop wmsShop;
    private Plat plat;
    private Platform platform;
    private ChukuBoci chukuBoci;


    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
