package com.dt76.wmsService.utils;

import java.io.Serializable;

public class BaStorePageUtil implements Serializable {
    private int  storeId;
    private String storeCode;
    private String storeName;
    private String storeText;


    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
