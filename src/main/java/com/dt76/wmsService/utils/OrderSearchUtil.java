package com.dt76.wmsService.utils;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 17:30
 */

import lombok.Data;

import java.io.Serializable;



@Data
public class OrderSearchUtil implements Serializable {


    private String cusCode;
    private String shpName;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
