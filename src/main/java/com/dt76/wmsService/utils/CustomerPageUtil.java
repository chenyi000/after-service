package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 8:19
 */
@Data
public class CustomerPageUtil implements Serializable {

    //个人信息
    private long cId;
    private String cCode;
    private String cUsername;
    private String cPassword;
    private String cNickname;
    private String cRealname;
    private String cTelphone;
    private String cAddress;
    private Long cCustomerType;
    private Long status;

    private  String customerType;
    private String statusName;

    //公司信息
    private String pCode;
    private String pName;
    private String pPerson;
    private String pContacts;
    private String pTelphone;
    private String pEmail;
    private String pAddress;

    //顾客表创建信息
    private Date cCreateTime;
    private String cCreatName;
    private Date cUpdateTime;
    private String cUpdateName;

    //分页信息
    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
