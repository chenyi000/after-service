package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: u3
 * @description: 用于插入商品id，仓库id，储位id
 * @author: wx
 * @create: 2019-05-15 15:43
 */
@Data
public class ShangjiaSearchUtil2 implements Serializable {

    private String cusCode;
    private String storeName;
    private String chuweiName;

    private String cusName;


    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
