package com.dt76.wmsService.utils;



import lombok.Data;

import java.io.Serializable;


@Data
public class YanHuoSearchUtil implements Serializable {


    private String cusCode;
    private String docId;
    private String shpName;//商品名称
    private String daoId;
    private String siji;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;

}
