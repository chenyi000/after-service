package com.dt76.wmsService.utils;

import lombok.Data;

import java.io.Serializable;

@Data
public class BShopPageUtil implements Serializable {
    private int  storeId;
    private String storeCode;
    private String storeName;
    private String storeText;


    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
