package com.dt76.wmsService.utils;

import com.dt76.wmsService.pojo.WmsOrder;
import lombok.Data;

import java.io.Serializable;

/*
 *Created by 王超 on 2019/5/15
 */
public
@Data
class ChukuTongzhiSearchUtil implements Serializable {
    private WmsOrder wmsOrder;

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize;
    private Integer pageNum;
}
