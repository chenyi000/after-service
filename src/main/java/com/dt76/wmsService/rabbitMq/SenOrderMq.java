package com.dt76.wmsService.rabbitMq;

import com.dt76.wmsService.commen.SendOrderUtil;
import com.dt76.wmsService.pojo.WmsOrder;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.OrderService;
import com.dt76.wmsService.service.ShopService;
import com.dt76.wmsService.service.WebSocket;
import com.dt76.wmsService.utils.WmsUUID;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @auther jun
 * @create 2019-05-18 12:10
 */

@Component
@RabbitListener(queues = "orderSend")
public class SenOrderMq {

    @Autowired
    private ShopService shopService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private WebSocket webSocket;

    @RabbitHandler
    public void addOrder(String sendOrder) throws Exception{

        Gson gson=new Gson();
        SendOrderUtil orderShop = gson.fromJson(sendOrder, SendOrderUtil.class);

        System.out.println("成功调用远程服务:orderService>add>>>");

        String shopId = "SH"+ WmsUUID.getUUID();
        //2插入商品表
        WmsShop wmsShop = new WmsShop();
        wmsShop.setCreateName("wx");
        wmsShop.setShpName(orderShop.getShpName());
        wmsShop.setShpType(orderShop.getShpType());
        wmsShop.setShpCode(shopId);
        wmsShop.setShpNum(orderShop.getShpNum());
        wmsShop.setShpWeight(orderShop.getShpWeight());
        wmsShop.setShpTiji(orderShop.getShpTiji());
        wmsShop.setInType(orderShop.getInType());
        wmsShop.setProDate(orderShop.getProDate());
        wmsShop.setBaozhiqi(orderShop.getBaozhiqi());
        wmsShop.setUnit(orderShop.getUnit());
        wmsShop.setInDay(orderShop.getInDay());
        wmsShop.setOrderCode(orderShop.getOrderId());
        wmsShop.setStatusId(1);

        shopService.addShop(wmsShop);
        System.out.println("增加成功");


        //3插入订单表
        //获取商品主键
        int reciveId = wmsShop.getId();
        //插入订单表
        WmsOrder wmsOrder = new WmsOrder();
        wmsOrder.setCreateName("Wx");
        wmsOrder.setCusCode(orderShop.getOrderId());
        wmsOrder.setCusName(orderShop.getCusName());
        wmsOrder.setMasName(orderShop.getMasName());
        wmsOrder.setCusAddress(orderShop.getCusAddress());
        wmsOrder.setMasAddress(orderShop.getMasAddress());
        wmsOrder.setCusPhone(orderShop.getCusPhone());
        wmsOrder.setMasPhone(orderShop.getMasPhone());
        wmsOrder.setShopId(reciveId);
        wmsOrder.setTypeId(orderShop.getTypeId());

        orderService.addOrder(wmsOrder);
        System.out.println("增加成功2");

        //发送websocket消息
        //webSocket.sendMessage("您有新的订单啦!!!");
    }

}
