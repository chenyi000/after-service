package com.dt76.wmsService.rabbitMq;

import com.dt76.wmsService.commen.SendOrderUtil;
import com.dt76.wmsService.pojo.WmsOrder;
import com.dt76.wmsService.pojo.WmsShop;
import com.dt76.wmsService.service.OrderService;
import com.dt76.wmsService.service.ShopService;
import com.dt76.wmsService.utils.QuitOrderUtil;
import com.dt76.wmsService.utils.WmsUUID;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @auther jun
 * @create 2019-05-18 12:10
 */

@Component
@RabbitListener(queues = "orderQuit")
public class QuitOrderMq {


    @RabbitHandler
    public void addOrder(String quitOrder) throws Exception{
        Gson gson=new Gson();
        QuitOrderUtil orderShop = gson.fromJson(quitOrder, QuitOrderUtil.class);

        System.out.println("成功调用远程服务:orderService>add>>>");
        //1生成订单号

        //TODO 后台退订逻辑



    }

}
