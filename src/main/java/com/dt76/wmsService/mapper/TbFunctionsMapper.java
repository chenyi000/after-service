package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.TbFunctions;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-12 21:31
 */
public interface TbFunctionsMapper {

    int deleteByPrimaryKey(Integer funcId);

    int insert(TbFunctions record);

    TbFunctions selectByPrimaryKey(Integer funcId);

    List<TbFunctions> selectAll();

    int updateByPrimaryKey(TbFunctions record);

    List<TbFunctions> findByRoleIds(List<Integer> list);

    List<TbFunctions> findFunctionByUserId(Integer userId);
}
