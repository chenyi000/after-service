package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.KuCunA;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KuCunAMapper {
    List<KuCunA> findConditionPageAll(@Param("start") int start,
                                      @Param("size") int size,

                                      @Param("goodsName") String goodsName,
                                      @Param("chuweiName") String chuweiName

    );
    //动态查询+带分页chuweiName
    int getConditionCount(

            @Param("goodsName") String goodsName,
            @Param("chuweiName") String chuweiName
    );
    List<KuCunA> findAll();
}
