package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsHuiDan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *Created by 王超 on 2019/5/17
 */
public interface ChukuHuidanMapper {

//录入一笔回单
void add(WmsHuiDan wmsHuiDan);

//查询所有可以录入的出库单号
   List<Plat> huidan();


   //展示所有已回单的信息
   List<ChukuTongzhi> getAll(@Param("start") int start,
                             @Param("size") int size,
                             @Param("jjr") String jjr);


   //查询已回单的条数
   int qryCountHui(@Param("jjr") String jjr);



//录入回单同时修改当前的商品的状态(控制下次可回单出库号)
   void udp(@Param("chuDocId") String chuDocId);


   //根据出货单号删除回单
   void del(@Param("docId") int docId);
}
