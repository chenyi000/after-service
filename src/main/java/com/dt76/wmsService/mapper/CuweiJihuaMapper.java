package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import com.dt76.wmsService.pojo.Cuweijihua;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CuweiJihuaMapper {
    List<Cuweijihua> findConditionPageAll(@Param("start") int start,
                                          @Param("size") int size,

                                          @Param("goodsName") String goodsName

                                      );//动态查询+带分页
    int getConditionCount(
                           @Param("goodsName") String goodsName
                          );
    List<Cuweijihua> findAll();

    //根据商品Id查询一条商品的详细信息
    Cuweijihua findOne(@Param("odId") int odId);
}
