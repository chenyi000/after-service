package com.dt76.wmsService.mapper;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 8:33
 */

import com.dt76.wmsService.pojo.SysCompany;

import java.util.List;

/**
 * 公司增加
 */
public interface CompanyMapper {

    /**
     * 遍历所有的公司
     * @return
     */
    List<SysCompany> findCompany();

    /**
     * 插入后返回主键
     * @param sysCompany
     * @return
     */
    Long addCompany(SysCompany sysCompany);
}
