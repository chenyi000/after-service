package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Cheliang;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface CheliangMapper {


    List<Cheliang> qryPageAll(@Param("start") int start,
                              @Param("size") int size,
                              @Param("cheNo") String cheNo,
                              @Param("sj") String sj,
                              @Param("stu") String stu);

    int qryCount(@Param("cheNo") String cheNo,
                 @Param("sj") String sj,
                 @Param("stu") String stu);

    void insert(Cheliang cheliang);

    void update(Cheliang cheliang);

    void delete(int id);
}
