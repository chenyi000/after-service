package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.WmsOrder;
import com.dt76.wmsService.utils.OrderPageUtil;
import com.dt76.wmsService.utils.OrderSearchUtil;

import java.util.List;


public interface OrderMapper {
    //增加订单
    int insert(WmsOrder wmsOrder);
    //查询全部
    List<WmsOrder> qryAll();
    //查询订单详情
    List<OrderPageUtil> qryAll2(OrderSearchUtil orderSearchUtil);
    //查询单个订单
    OrderPageUtil qryById(int id);

    Integer getCount(OrderSearchUtil orderSearchUtil);

    //修改
    int upd(WmsOrder wmsOrder);
}
