package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.IntegratedInventory;

import java.util.List;

public interface IntegratedInventoryMapper {
    //展示
    List<IntegratedInventory> qryAll();
}
