package com.dt76.wmsService.mapper;

/**
 * @Description
 * @auther jun
 * @create 2019-05-10 11:05
 */

import com.dt76.wmsService.pojo.SysDepartment;
import com.dt76.wmsService.utils.DepartmentPageUtil;
import com.dt76.wmsService.utils.DepartmentSearchUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 部门信息的处理dao
 */
public interface DepartmentMapper {

    /**
     * 按条件查询部门
     * @param departmentSearchUtil
     * @return
     */
    List<DepartmentPageUtil> finDepartment(DepartmentSearchUtil departmentSearchUtil);

    /**
     * 增加
     * @param sysEpartment
     * @return
     */
    Integer addDepartment(SysDepartment sysEpartment);

    /**
     * 删除
     * @param id
     * @return
     */
    Integer delDepartment(@Param("id") Long id);

    /**
     * 修改
     * @param sysEpartment
     * @return
     */
    Integer updDepartment(SysDepartment sysEpartment);

    /**
     * 安条件统计数量
     * @param departmentSearchUtil
     * @return
     */
    Long getDepartmentCount(DepartmentSearchUtil departmentSearchUtil);
}
