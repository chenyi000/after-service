package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.KuCunA;
import com.dt76.wmsService.pojo.Lihuo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LihuoMapper {
    List<Lihuo> findConditionPageAll(@Param("start") int start,
                                     @Param("size") int size,

                                     @Param("goodsName") String goodsName,
                                     @Param("chuweiName") String chuweiName,
                                     @Param("tuopanName") String tuopanName

    );
    //动态查询+带分页chuweiName
    int getConditionCount(

            @Param("goodsName") String goodsName,
            @Param("chuweiName") String chuweiName,
            @Param("tuopanName") String tuopanName
    );
    List<Lihuo> findAll();
}
