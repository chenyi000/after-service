package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *Created by 王超 on 2019/5/14
 */
public interface ChuFuheMapper {
    //分页+查询所有待复核的商品
    List<ChukuTongzhi> getAllFuhe(@Param("start") int start,
                                  @Param("size") int size,
                                  @Param("ddh") String ddh);


    //查询所有待复核的商品订单数****订单号
    int qryCount(@Param("ddh") String ddh);

    //点击复核按钮修改当前商品的状态ID
    void udp(@Param("spId") int id);
}
