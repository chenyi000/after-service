package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Bin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BinMapper {
    List<Bin> findConditionPageAll(@Param("start") int start,
                                  @Param("size") int size,
                                  @Param("storeCode") String storeCode,
                                  @Param("storeId") int storeId,
                                   @Param("pId") int pId );//动态查询+带分页
    int getConditionCount(@Param("storeCode") String storeCode,
                          @Param("storeId") int storeId,
                          @Param("pId") int pId );
    List<Bin> findAll();
    List<Bin> findPageAll(@Param("start") int start,
                          @Param("size") int size,
                          @Param("storeName") String storeName,
                          @Param("chuweiName") String chuweiName,
                          @Param("tuopanName") String tuopanName);
    int getCount(@Param("storeName") String storeName,
                   @Param("chuweiName") String chuweiName,
                   @Param("tuopanName") String tuopanName);


}
