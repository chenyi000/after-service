package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.BaStore;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/*
   增加：add
   删除：del
   修改：upd
   统计个数：get
   查询：find
 */
public interface BaStoreMapper {
    List<BaStore> findConditionPageAll(@Param("start") int start,
                                       @Param("size") int size,
                                       @Param("storeCode") String storeCode,
                                       @Param("storeName") String storeName);//动态查询+带分页
    int getConditionCount(@Param("storeCode") String storeCode,
                          @Param("storeName") String storeName);
    List<BaStore> findAll();
    //查询单个
    BaStore findOne(@Param("storeId") int storeId);

    //新增
    void add(BaStore baStore);
    //修改
    void update(BaStore baStore);
    //删除
    void delete(@Param("storeId") int storeId);
   /* *//**
     * 仓库增加
     * @param
     * @return
     *//*
    void add(BaStore BaStore);*/

}
