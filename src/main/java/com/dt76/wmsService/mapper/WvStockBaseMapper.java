package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.WvStockBase;

import java.util.List;

public interface WvStockBaseMapper {

    //查询
    List<WvStockBase> qryAll();
}
