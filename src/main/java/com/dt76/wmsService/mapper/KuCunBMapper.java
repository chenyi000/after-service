package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.KuCunB;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KuCunBMapper {
    List<KuCunB> findConditionPageAll(@Param("start") int start,
                                      @Param("size") int size,

                                      @Param("goodsName") String goodsName,
                                      @Param("chuweiName") String chuweiName

    );
    //动态查询+带分页chuweiName
    int getConditionCount(

            @Param("goodsName") String goodsName,
            @Param("chuweiName") String chuweiName
    );
    List<KuCunB> findAll();
}
