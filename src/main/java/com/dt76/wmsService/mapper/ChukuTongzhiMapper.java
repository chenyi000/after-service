package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *Created by 王超 on 2019/5/10
 */
public interface ChukuTongzhiMapper {
//    //查询出库通知
    List<ChukuTongzhi> getAllChuKu(@Param("start") int start,
                                   @Param("size") int size,
                                   @Param("sjr") String sjr,
                                  @Param("ddh") String ddh);
//查询总条数
int qryCount(@Param("sjr") String sjr,
             @Param("ddh") String ddh);


    //点击出库按钮执行的操作1.0
    void udp(@Param("spId") int id);
    //点击出库按钮执行操作2.0
    void xiu(@Param("spId") int id);

    //根据商品Id查询一条商品的详细信息
    ChukuTongzhi findOne(@Param("spId") int spId);



}
