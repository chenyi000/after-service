package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.BShop;
import com.dt76.wmsService.utils.BShopPageUtil;
import com.dt76.wmsService.utils.BShopSearchUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BShopMapper {
    //新增仓库
    Integer add(BShop BShop);
    //修改仓库
    Integer update(BShop BShop);
    //分页查询+条件
    List<BShopPageUtil> findConditionPageAll(BShopSearchUtil bShopSearchUtil);
    //统计仓库数量
    Integer getConditionCount(BShopSearchUtil bShopSearchUtil);
     //删除
    Integer delete(@Param("storeId") int storeId);
   //查询单个
    BShop findOne(@Param("storeId") int storeId);
    //查询所有
    List<BShop> findAll();

}
