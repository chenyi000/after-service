package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Plat;
import com.dt76.wmsService.pojo.WmsShop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PlatAMapper {

    List<Plat> qryPageAll(@Param("start") int start,
                          @Param("size") int size,
                          @Param("doc") String doc,
                          @Param("plat") String plat);

    int qryCount(@Param("doc") String doc,
                 @Param("plat") String plat);

    void update(Plat plat);

    void delete(int id);


    void udpStuIdById(@Param("spId") int spId,
                      @Param("statusId") int statusId);

    void udpStuPlat(@Param("spId") int spId);

    Plat qryById(@Param("id") Long id);

    WmsShop qryShopById(@Param("id") int id);

    void updateInDate(@Param("pId") int pId);

    void updateOutDate(@Param("pId") int pId);

}
