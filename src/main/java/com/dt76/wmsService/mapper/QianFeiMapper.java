package com.dt76.wmsService.mapper;

import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QianFeiMapper {

    //分页+条件的动态查询所有费用明细
    List<DayCostPageUtil> findQianFei(DayCostSearchUtil dayCostSearchUtil);


    int getCount(DayCostSearchUtil dayCostSearchUtil);

    //根据id查询
    DayCostPageUtil findById(@Param("id") int id);
}
