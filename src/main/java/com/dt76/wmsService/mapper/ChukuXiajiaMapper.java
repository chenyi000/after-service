package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/*
 *Created by 王超 on 2019/5/14
 */
public interface ChukuXiajiaMapper {
    //分页+查询所有待下架任务
    List<ChukuTongzhi> getAllXiaJia(@Param("start") int start,
                                    @Param("size") int size,
                                    @Param("jjr") String jjr
                                   );


    //查询带下架商品的条数
    int qryCount(@Param("jjr") String jjr);


//    点击下架按钮修改当前商品状态ID
    void udp(@Param("spId") int id);
}
