package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.ChukuTongzhi;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *Created by 王超 on 2019/5/12
 */
public interface RenwuQuerenMapper {
    //测试查询所有待确认任务
    List<ChukuTongzhi> get(@Param("start") int start,
                           @Param("size") int size
                           ,@Param("sp") String sp
                       );


 //动态查询所有条数
    int qryCount(@Param("sp") String sp);


    //点击任务确认按钮后修改状态
    void udp(@Param("StatusID") int id);


  //点击波次生成按钮改变状态
    //为点击波次按钮的当前商品生成波次号
   void insert(@Param("spId") int id);
   void gai(@Param("spId") int id);
}
