package com.dt76.wmsService.mapper;
/*
    查询订单历史表
 */

import com.dt76.wmsService.pojo.Wms_his_stock_ku;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Wms_his_stock_kuMapper {
    //查所有
    List<Wms_his_stock_ku> findAllWms_his_stock_ku();


}
