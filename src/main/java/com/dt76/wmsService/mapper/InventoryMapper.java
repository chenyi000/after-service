package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Inventory;
import java.util.List;

public interface InventoryMapper {
    //展示
    List<Inventory> qryAll();
}
