package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.SysEmployee;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 12:37
 */

/**
 * 员工信息的处理dao
 */
public interface AccountMapper {

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    SysEmployee login(@Param("username") String username,
                      @Param("password") String password);

    SysEmployee loginShiro(@Param("username") String username);

    List<SysEmployee> findAll();

}
