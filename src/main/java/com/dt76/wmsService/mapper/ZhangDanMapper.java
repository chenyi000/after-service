package com.dt76.wmsService.mapper;

import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.ZhangDanPageUtil;
import com.dt76.wmsService.utils.ZhangDanSearchUtil;

import java.util.List;

public interface ZhangDanMapper {

    List<ZhangDanPageUtil> findZd(ZhangDanSearchUtil zhangDanSearchUtil);

    int getCount(ZhangDanSearchUtil zhangDanSearchUtil);

    List<DayCostPageUtil> exportBill(ZhangDanSearchUtil zhangDanSearchUtil);

}
