package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Kongchuwei;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KongMapper {
    List<Kongchuwei> findConditionPageAll(@Param("start") int start,
                                          @Param("size") int size,

                                          @Param("storeName") String storeName);//动态查询+带分页
    int getConditionCount(
                          @Param("storeName") String storeName);
}
