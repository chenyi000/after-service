package com.dt76.wmsService.mapper;




import com.dt76.wmsService.pojo.WmsShop;

import java.util.List;

public interface ShopMapper {
    //添加商品
    int insertShop(WmsShop wmsShop);
    //查询全部
    List<WmsShop> qryAll();
    //查询单个
    WmsShop getOne(int id);
    //修改业务状态
    int upd(WmsShop wmsShop);

    int updShop(WmsShop wmsShop);

    int qryCount1();
    int qryCount2();
    int qryCount3();
    int qryCount4();
}
