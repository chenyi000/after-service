package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.utils.CustomerPageUtil;
import com.dt76.wmsService.utils.CustomerSearcherUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-13 8:17
 */
public interface CustomerMapper {

    /**
     * 动态查询分页
     * @param searcherUtil
     * @return
     */
    List<CustomerPageUtil> findCustomer(CustomerSearcherUtil searcherUtil);

    /**
     * 增加用户
     * @param sysCustomer
     * @return
     */
    Integer addCustomer(SysCustomer sysCustomer);

    /**
     * 删除
     * @param id
     * @return
     */
    Integer delCustomerById(@Param("id") Integer id);


    /**
     * 修改
     * @param sysCustomer
     * @return
     */
    Integer updCustomer(SysCustomer sysCustomer);

    /**
     * 单个查找
     * @param id
     * @return
     */
    SysCustomer finCustomerById(@Param("id") Integer id);

    /**
     * 按条件统计数量
     * @param searcherUtil
     * @return
     */
    Integer getCustomerCount(CustomerSearcherUtil searcherUtil);

    /**
     * 修改合作状态
     * @param id
     * @return
     */
    Integer updCustomerStatusById(@Param("status") Integer status,
                                  @Param("id") Integer id);


}
