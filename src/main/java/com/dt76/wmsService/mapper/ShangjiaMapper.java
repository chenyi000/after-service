package com.dt76.wmsService.mapper;

import com.dt76.wmsService.utils.ShangjiaPageUtil;
import com.dt76.wmsService.utils.ShangjiaSearchUtil;
import com.dt76.wmsService.utils.ShangjiaSearchUtil2;
import com.dt76.wmsService.utils.kuCount;

import java.util.List;

/**
 * @program: u3
 * @description: 库存表
 * @author: wx
 * @create: 2019-05-15 15:45
 */

public interface ShangjiaMapper {

    //插入id
    int insert(ShangjiaSearchUtil shangjiaSearchUtil);

    List<ShangjiaPageUtil> qryAll(ShangjiaSearchUtil2 shangjiaSearchUtil2);
    Integer getCount(ShangjiaSearchUtil2 shangjiaSearchUtil2);

    List<kuCount> getCount2(int id);
}
