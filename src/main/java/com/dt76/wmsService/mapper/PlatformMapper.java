package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Platform;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PlatformMapper {
    List<Platform>  qryPlatformAll();

    List<Platform>qryPageAll(@Param("start") int start,
                             @Param("size") int size,
                             @Param("qryCode") String qryCode,
                             @Param("qryName") String qryName);

    int qryCount();

    void insert(Platform platform);

    void update(Platform platform);

    Platform getPlatformById(@Param("id") Long id);

    void delete(int id);
}
