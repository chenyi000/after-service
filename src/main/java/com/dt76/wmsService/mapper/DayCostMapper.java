package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.pojo.WmsDayCost;
import com.dt76.wmsService.utils.DayCostPageUtil;
import com.dt76.wmsService.utils.DayCostSearchUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DayCostMapper {

    //查询所有费用明细
    List<WmsDayCost> qryAllDayCost();

    //分页+条件的动态查询所有费用明细
    List<DayCostPageUtil> findDayCost(DayCostSearchUtil dayCostSearchUtil);

    int getConditionCount(DayCostSearchUtil dayCostSearchUtil);

    //修改费用明细
    Integer updataDayCost(WmsDayCost wmsDayCost);

    //根据id查询
    DayCostPageUtil findById(@Param("id") int id);

    //保存费用明细
    int addDayCost(WmsDayCost wmsDayCost);

    Integer delDayCost(@Param("id") int id);

    /*
   模糊查询客户名
    */
    List<SysCustomer> getName(@Param("cUsername") String cUsername);

    /*
    根据客户名查询订单信息
     */
    DayCostPageUtil findShop(@Param("cUsername") String cUsername);


}
