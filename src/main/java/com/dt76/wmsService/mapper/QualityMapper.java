package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Quality;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface QualityMapper {
     //查询所有
     List<Quality> qryAll();

     //增加品检
     int insert(Quality quality);

     //修改产品品检
     int update(Quality quality);

     //带条件查询
     Quality toqryAll(int id);

     //带条件删除
     int delet(int id);
}
