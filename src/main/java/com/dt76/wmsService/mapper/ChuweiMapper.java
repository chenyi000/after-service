package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Chuwei;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChuweiMapper {
    List<Chuwei> findConditionPageAll(@Param("start") int start ,
                                     @Param("size") int size,
                                      @Param("storeName") String storeName);
    int getConditionPageAll(@Param("storeName")String storeName);
   /* void add(Chuwei chuwei);
    void update(Chuwei chuwei);
    void delete(@Param("cId")int cId);*/
}
