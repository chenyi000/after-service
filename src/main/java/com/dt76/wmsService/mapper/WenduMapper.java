package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Wendu;

import java.util.List;

public interface WenduMapper {
    List<Wendu> qryAll();
}
