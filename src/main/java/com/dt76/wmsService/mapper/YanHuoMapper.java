package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.Yanhuo;
import com.dt76.wmsService.utils.OrderSearchUtil;
import com.dt76.wmsService.utils.YanHuoPageUtil;
import com.dt76.wmsService.utils.YanHuoSearchUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: u3
 * @description: 验货
 * @author: wx
 * @create: 2019-05-12 21:29
 */
public interface YanHuoMapper {

    //查询所有
    List<YanHuoPageUtil> qryAll(YanHuoSearchUtil yanHuoSearchUtil);
    List<YanHuoPageUtil> qryAll2(YanHuoSearchUtil yanHuoSearchUtil);
    //进货，查看是否分配司机
    List<YanHuoPageUtil> qryAll3(YanHuoSearchUtil yanHuoSearchUtil);

    Integer getCount(YanHuoSearchUtil yanHuoSearchUtil);
    Integer getCount2(YanHuoSearchUtil yanHuoSearchUtil);

    //查询单条
    YanHuoPageUtil getOne(int id);

    //插入进货表
    int insert(@Param("spId") int spId);

    int upd(@Param("spId") int spId);
}
